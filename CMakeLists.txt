cmake_minimum_required(VERSION 3.2)
project(embedded-concrete)

set_property(GLOBAL PROPERTY USE_FOLDERS ON)

if(NOT CMAKE_DEBUG_POSTFIX)
  set(CMAKE_DEBUG_POSTFIX -d)
endif()
if(NOT CMAKE_MINSIZEREL_POSTFIX)
  set(CMAKE_MINSIZEREL_POSTFIX -min)
endif()
if(NOT CMAKE_RELWITHDEBINFO_POSTFIX)
  set(CMAKE_RELWITHDEBINFO_POSTFIX -reldeb)
endif()

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/lib)

# Second, for multi-config builds (e.g. msvc)
foreach(OUTPUTCONFIG ${CMAKE_CONFIGURATION_TYPES})
    string(TOUPPER ${OUTPUTCONFIG} OUTPUTCONFIG)
    set(CMAKE_RUNTIME_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CMAKE_CURRENT_SOURCE_DIR}/bin)
    set(CMAKE_LIBRARY_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CMAKE_CURRENT_SOURCE_DIR}/lib)
    set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY_${OUTPUTCONFIG} ${CMAKE_CURRENT_SOURCE_DIR}/lib)
endforeach(OUTPUTCONFIG CMAKE_CONFIGURATION_TYPES)

if (WIN32)
    add_definitions(-DWINDOWS)
elseif (APPLE)
    add_definitions(-DAPPLE)
elseif (UNIX)
    add_definitions(-DLINUX)
else()
    message(Could not detect OS!)
endif ()

include(CheckCXXCompilerFlag)

CHECK_CXX_COMPILER_FLAG("-std=c++14" COMPILER_SUPPORTS_CXX14)
if(COMPILER_SUPPORTS_CXX14)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++14")
    #if (${CMAKE_CXX_COMPILER_ID} STREQUAL "Clang")
    #    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -stdlib=libc++")
    #    message("Clang detected - using libc++")
    #endif()
endif()

link_directories(${CMAKE_CURRENT_SOURCE_DIR}/lib)
link_directories(${CMAKE_CURRENT_SOURCE_DIR}/bin)
include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include")

option(EC_BUILD_TESTS "Build ec and dependencies' tests" OFF)
option(EC_MULTI_CORE "Enable parallel build" ON)

include(tools/CMakeUtils.cmake)
include(tmp/BuildOptions.cmake)

if(EC_MULTI_CORE)    
    if(MSVC)
        message("Enabling multi CPU compilation (CPUS count: ${EC_CPUS_COUNT})")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} /MP${EC_CPUS_COUNT}")
    endif()
endif()

add_subdirectory("src")

if (EC_BUILD_TESTS)
    message("Adding tests")
    add_subdirectory("tests")
endif ()

