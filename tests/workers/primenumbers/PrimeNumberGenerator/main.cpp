/* 
 * File:   main.cpp
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 2 listopada 2016, 09:16
 */

// Generator

#include <cstdlib>
#include <cstdio>
#include <ctime>
#include <iostream>
#include "io_setup.h"

using namespace std;

int main(int argc, char** argv)
{
    enableBinaryStdio();

    srand(static_cast<unsigned> (time(0)));
    int i = 0;
    while (i < 10000) {
        int nextNumber = rand();
        cout.write((char*) &i, sizeof (i));
        cout.write((char*) &nextNumber, sizeof (nextNumber));
        cerr << "Wygenerowana liczba (" << i << "): " << nextNumber << endl;
		i++;
    }
    return 0;
}
