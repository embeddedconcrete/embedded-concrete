/* 
 * File:   main.cpp
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 2 listopada 2016, 09:49
 */

#include <cstdlib>
#include <cstdio>
#include <iostream>

#include "io_setup.h"

// Reciever

using namespace std;

int main(int argc, char** argv)
{

    int n[3];
    while (cin.read((char*) &n, sizeof (n))) {
        cerr << "Odebrana liczba nr " << n[0] << " - " << n[1] << " - " << (n[2] ? "Tak" : "Nie") << endl;
    }
    return 0;
}

