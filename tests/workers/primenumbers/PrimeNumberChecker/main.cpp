/* 
 * File:   main.cpp
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 2 listopada 2016, 09:22
 */

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <iostream>
#include "io_setup.h"

// Checker

using namespace std;

int isPrime(int n);

int main(int argc, char** argv)
{
    int n[3] = {0};
    while (cin.read((char*) &n[0], 2 * sizeof (n[0]))) {
        n[2] = isPrime(n[1]);
        cerr << "Sprawdzanie " << n[0] << " liczby - " << n[1] << ": " << n[2] << endl;
        cout.write((char*) n, sizeof (n));
    }
    return 0;
}

int isPrime(int n)
{
    if ((n & 1) || n % 3 == 0 || n % 5 == 0) {
        return 1;
    }
    int sq = static_cast<int> (sqrt(n));
    for (int i = 7; i <= sq; i += 2) {
        if (n % i == 0) return 0;
    }
    return 1;

}
