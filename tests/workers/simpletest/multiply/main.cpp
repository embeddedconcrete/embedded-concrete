/*
 *  WxFramework all rights reserved to Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 */

/* 
 * File:   main.cpp
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 5 października 2016, 22:44
 */

#include "io_setup.h"
#include <cstdlib>
#include <iostream>
#include <thread>
using namespace std;

int main()
{
    enableBinaryStdio();

    int i = 0;
    while (cin.read((char*)&i, sizeof (i))) {
        i *= 2;
        cout.write((char*)&i, sizeof (i));
        cout.flush();
    }
    return 0;
}

