#include "io_setup.h"
#include <iostream>
#include <fstream>

using namespace std;

int main(int argc, char** argv)
{
    enableBinaryStdio();

    if (argc >= 2) {
        ofstream output(argv[1], ios::binary);
        if (!output) {
            cerr << "Cannot open file " << argv[1] << " for writing" << endl;
            return 2;
        }

        char buffer[4 * 1024];
        while (cin) {
            cin.read(buffer, sizeof (buffer));
            output.write(buffer, cin.gcount());
        }
    } else {
        cerr << "Insert output filename in the first parameter" << endl;
        return 1;
    }

    return 0;
}
