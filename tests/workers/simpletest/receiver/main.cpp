/*
 *  WxFramework all rights reserved to Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 */

/* 
 * File:   main.cpp
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 5 października 2016, 22:47
 */

#include "io_setup.h"
#include <cstdlib>
#include <iostream>

using namespace std;

/*
 * 
 */
int main(int argc, char** argv)
{
    enableBinaryStdio();

    int i = 0;
    while (cin.read((char*)&i, sizeof (i))) {
        cerr << i << endl;
    }
    return 0;
}

