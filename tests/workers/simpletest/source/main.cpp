#include "io_setup.h"
#include <iostream>
#include <thread>

using namespace std;

int main()
{
    enableBinaryStdio();

    for (int i = 0; i < 100000; i++) {
        cout.write((char*)&i, sizeof (i));
        cout.flush();
        //this_thread::sleep_for(chrono::microseconds(200));
    }

    return 0;

}