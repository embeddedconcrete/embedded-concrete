#pragma once

void enableBinaryStdio();

void setArgv0(char* argv0);

const char* getExecutableName();

unsigned getPidId();
