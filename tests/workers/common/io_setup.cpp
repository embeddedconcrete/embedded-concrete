#include "io_setup.h"
#include <cstdio>
#include <cstdlib>
#include <ios>
#include <iostream>
#include <string>
#include <cstring>


#ifdef WINDOWS
#ifdef _MSC_VER
#include <io.h>
#include <fcntl.h>
#include <stdio.h>
#include <Windows.h>
#endif
#else 
#include <unistd.h>
#endif

using namespace std;

static char* argv0 = nullptr;

void setArgv0(char* argv0)
{
    ::argv0 = argv0;
}

const char* getExecutableName()
{
    char* last = strrchr(argv0, '/');
    if (last != nullptr) {
        return last + 1;
    }
    return argv0;
}

static void printExit()
{
    if (argv0) {
        cout << "Exiting " << argv0 << endl;
    } else {
        cout << "Exiting process" << endl;
    }
}

void enableBinaryStdio()
{
#ifdef WINDOWS
    _setmode(_fileno(stdin), _O_BINARY);
    _setmode(_fileno(stdout), _O_BINARY);
#endif
    atexit(printExit);
    std::ios::sync_with_stdio(false);
}

unsigned getPidId()
{
#ifdef WINDOWS
    return GetCurrentProcessId();
#else
    return getpid();
#endif
}
