#include "io_setup.h"
#include "opencv_frame_common.h"
#include "FramePackage.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    if (argc == 2 && strcmp(argv[1], "--help") == 0) {
        cout << "This program simply rewrites input frame to the output and has been created just to test throughput of the connection channel." << endl;
        cout << "Run: " << endl;
        cout << "\t" << argv[0] << endl;
        cout << "Output format: " << "Standard" << endl;
        return 0;
    }

    enableBinaryStdio();
    setArgv0(argv[0]);


    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "--bare") == 0) {
            FramePackage::sendPacketSize = false;
        }
    }

    FramePackage package;
    while (cin) {
        package.read();
        if (package.frame.empty()) continue;
        package.write();
    }
    return 0;
}
