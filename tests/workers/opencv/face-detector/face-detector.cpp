/* 
 * File:   main.cpp
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 26 września 2016, 08:43
 */

#include "io_setup.h"
#include "opencv_frame_common.h"
#include "FramePackage.h"

#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>

#include "dir.h"
#include "strutils.h"
#include <iostream>

using namespace std;
using namespace cv;

std::vector<Rect> detectFaces(Mat const& frame, CascadeClassifier & faceCascade);

void drawFacesOnMat(Mat & mat, vector<Rect> const& faces);

int main(int argc, char** argv)
{
    if (argc == 2 && strcmp(argv[1], "--help") == 0) {
        cout << "Detects faces in the frame" << endl;
        cout << "Run: " << endl;
        cout << "\t" << argv[0] << endl;
        cout << "Output format: " << "Standard" << endl;
        return 0;
    }
    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "--bare") == 0) {
            FramePackage::sendPacketSize = false;
        }
    }

    try {
        enableBinaryStdio();
        setArgv0(argv[0]);

        String faceCascadeName = "haarcascade_frontalface_alt.xml";
        CascadeClassifier faceCascade;

        if (!faceCascade.load(faceCascadeName)) {
            cerr << "--(!)Error loading\n";
            return -1;
        };

        FramePackage package;
        while (cin) {
            package.read();
            if (package.frame.empty()) break;
            auto start = std::chrono::high_resolution_clock::now();
            std::vector<Rect> faces = detectFaces(package.frame, faceCascade);
            drawFacesOnMat(package.frame, faces);
            auto elapsed = std::chrono::high_resolution_clock::now() - start;
            long long milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
            package.processingTimes.push_back(milliseconds);
#ifndef NDEBUG
            cerr << "FD: " << faces.size() << " faces detected in " << milliseconds << "ms" << endl;
            cerr.flush();
#endif
            package.write();
            cout.flush();
        }
    } catch (exception& exp) {
        cerr << "!!FD: " << exp.what() << endl;
        return -1;
    }

    return 0;
}

std::vector<Rect> detectFaces(Mat const& frame, CascadeClassifier & faceCascade)
{
    std::vector<Rect> faces;
    Mat frame_gray;

    cvtColor(frame, frame_gray, CV_BGR2GRAY);
    equalizeHist(frame_gray, frame_gray);
    faceCascade.detectMultiScale(frame_gray, faces, 1.1, 2, 0 | CV_HAAR_SCALE_IMAGE, Size(30, 30));

    return move(faces);
}

void drawFacesOnMat(Mat & mat, vector<Rect> const& faces)
{
    for (auto face : faces) {
        rectangle(mat, face, Scalar(255, 0, 0), 3);
    }
}
