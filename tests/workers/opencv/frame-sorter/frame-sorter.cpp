#include "io_setup.h"
#include "opencv_frame_common.h"
#include "FramePackage.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>
#include <queue>
#include <functional>

#include <chrono>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    int bufferSize = 15;

    if (argc == 2 && strcmp(argv[1], "--help") == 0) {
        cout << "Executes frame sorting, sending frames until they are ordered and no frame is missing. When the buffer is full, sending is continued despite some frames are missing" << endl;
        cout << "Run: " << endl;
        cout << "\t" << argv[0] << " [-s bufferSize] [--bare]" << endl;
        cout << "Default values: " << endl;
        cout << "\tbufferSize: " << bufferSize << endl;
        cout << "Output format: " << "Standard" << endl;
        return 0;
    }

    enableBinaryStdio();
    setArgv0(argv[0]);

    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-s") == 0 && argc > i + 1) {
            bufferSize = atoi(argv[i + 1]);
            i++;
        } else if (strcmp(argv[i], "--bare") == 0) {
            FramePackage::sendPacketSize = false;
        }
    }



    std::priority_queue<FramePackage> queue;

    int lastSentPackageNr = 0;
    while (cin) {
        while (queue.size() >= bufferSize) {
            queue.top().write();
            queue.pop();
        }
        FramePackage package;
        package.read();
        if (package.frame.empty()) continue;
        queue.push(move(package));
    }
    while (!queue.empty()) {
        queue.top().write();
        queue.pop();
    }
    return 0;
}
