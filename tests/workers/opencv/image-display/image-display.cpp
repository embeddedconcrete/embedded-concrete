/* 
 * File:   main.cpp
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 2 września 2016, 11:31
 */

#include "io_setup.h"
#include "opencv_frame_common.h"
#include "FramePackage.h"
#include "strutils.h"

#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <iostream>
#include <fstream>
#include <numeric>
#include <iosfwd>

using namespace std;
using namespace cv;

void drawProcessingTimes(Mat&frame, vector<long long> const& times)
{
    int margin = 15;
    int lineHeght = 10, i = 0;
    for (auto time : times) {
        putText(frame, makestr(time, "ms"), cvPoint(10, margin + lineHeght * (i++)),
                FONT_HERSHEY_COMPLEX_SMALL, 0.5, cvScalar(245, 245, 250), 1, CV_AA);
    }
}

void drawEntireProcessingtime(Mat&frame, long long timeMs)
{
    putText(frame, makestr(timeMs, "ms"), cvPoint(10, frame.rows - 10 - 10),
            FONT_HERSHEY_COMPLEX_SMALL, 0.5, cvScalar(245, 245, 250), 1, CV_AA);
}

int main(int argc, char** argv)
{
    double fps = 10;
    char* output = nullptr;
    bool writeTimes = true;
    if (argc == 2 && strcmp(argv[1], "--help") == 0) {
        cout << "Displays frames or saves them into avi movie" << endl;
        cout << "Run: " << endl;
        cout << "\t" << argv[0] << " [-o outputFile:string [--fps framesPerSec:double]] [--no-time] [--bare]" << endl;
        cout << "Default values: " << endl;
        cout << "\toutputFile: " << "none" << endl
                << "\tframesPerSec: " << fps << endl
                << "\twriteTimes: " << writeTimes << endl;
        cout << "Output format: " << "Standard" << endl;
        return 0;
    }

    if (argc >= 4) {
        writeTimes = strcmp(argv[3], "1") == 0;
    }


    char* logfile = nullptr;
    std::ofstream* logFileStream = nullptr;


    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "--fps") == 0 && argc > i + 1) {
            fps = atof(argv[i + 1]);
            i++;
        } else if (strcmp(argv[i], "--bare") == 0) {
            FramePackage::sendPacketSize = false;
        } else if (strcmp(argv[i], "--no-time") == 0) {
            writeTimes = false;
        } else if (strcmp(argv[i], "-o") == 0 && argc > i + 1) {
            output = argv[i + 1];
            i++;
        } else if (strcmp(argv[i], "--log") == 0 && argc > i + 1) {
            logfile = argv[i + 1];
            logFileStream = new std::ofstream(logfile);
            i++;
        }
    }

    VideoWriter *w = nullptr;
    auto lastFrameTime = std::chrono::high_resolution_clock::now();
    std::list<long long> times;
    try {
        enableBinaryStdio();
        setArgv0(argv[0]);

        FramePackage package;

        if (logFileStream) {
            (*logFileStream)
                    << "Numer pakietu" << ";"
                    << "FPS" << ";" << "Czas od poprzedniej ramki [ms]" << ";"
                    << "��czny czas przetwarzania ramki [ms]" << ";" << endl;
        }
        for (; cin;) {
            package.read();
            if (package.frame.empty()) continue;
#ifndef NDEBUG
            cerr << "Czas przetwarzania ramki: " << std::accumulate(package.processingTimes.begin(), package.processingTimes.end(), 0ll) << "ms" << endl;
            cerr.flush();
#endif
            auto currentTime = std::chrono::high_resolution_clock::now();
            long long finishTime = std::chrono::duration_cast<std::chrono::milliseconds>(
                    currentTime.time_since_epoch()).count();
            if (writeTimes) {
                drawProcessingTimes(package.frame, package.processingTimes);
            }
            if (writeTimes) {
                drawEntireProcessingtime(package.frame, finishTime - package.startTime);
            }
            if (logFileStream) {
                auto elapsed = std::chrono::high_resolution_clock::now() - lastFrameTime;
                times.push_front(finishTime);
                int fps = 0;
                for (auto t : times) {
                    if (t > finishTime - 1000) {
                        fps++;
                    } else {
                        break;
                    }
                }
                (*logFileStream) << package.number << ";" << fps << ";" << std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count() << ";" << finishTime - package.startTime << endl;
                lastFrameTime = std::chrono::high_resolution_clock::now();
            }
            if (output) {
                if (w == nullptr) {
                    w = new VideoWriter(output, CV_FOURCC('M', 'J', 'P', 'G'), fps, Size(package.frame.cols, package.frame.rows));
                }
                if (!package.frame.empty())
                    w->write(package.frame);
                else {
                    break;
                }
            } else {
                imshow("Video", package.frame);
                waitKey(30);
            }
        }
        if (w) {
            delete w;
            w = nullptr;
        }
        if (logFileStream) {
            delete logFileStream;
        }
    } catch (exception& exp) {
        cerr << "!!IF: " << exp.what() << endl;
        if (w)
            delete w;
        return -1;
    }
    return 0;
}
