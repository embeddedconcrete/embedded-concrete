-- 10.0.0.101:8888
node root [10.0.0.101:8888]
config def_packet_size { prefix }

worker display [../tests/bin/workers/image-display -o output/fd3.avi --log output/times.csv] +def_packet_size @root
worker detector5 [../tests/bin/workers/face-detector] +def_packet_size -> display
worker detector4 [../tests/bin/workers/face-detector] +def_packet_size -> display
worker detector3 [../tests/bin/workers/face-detector] +def_packet_size -> display
worker detector2 [../tests/bin/workers/face-detector] +def_packet_size -> display
worker detector1 [../tests/bin/workers/face-detector] +def_packet_size -> display
worker source [../tests/bin/workers/frame-sender -i ../../avi/recording_320x240.avi] +def_packet_size +source -> { detector1 detector2 detector3 detector4 detector5 } @root
