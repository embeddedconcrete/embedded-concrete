#include "opencv_frame_common.h"
#include "io_setup.h"

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <algorithm>
#include <iostream>

using namespace std;

void writeFrame(Mat const& frame)
{
    int size = static_cast<int> (frame.total() * frame.elemSize());
    int portionSize = 10000;
    char* data = (char*) frame.data;
    int bytesSent = 0, bytesToSend = size;
    writeFrameHeader(frame);
    while (bytesToSend > 0) {
        int bytesWrote = std::min(portionSize, bytesToSend);
        cout.write((char*) data, bytesWrote);
        data += bytesWrote;
        bytesSent += bytesWrote;
        bytesToSend -= bytesWrote;
    }
}

void writeFramePackage(Mat const& frame, long milliseconds)
{
    writePacketSize(calcFrameSize(frame) + sizeof (FrameHeader) + sizeof (milliseconds));
    cout.write((char*) &milliseconds, sizeof (milliseconds));
    writeFrame(frame);
    cout.flush();
}

void readFrameFromInput(Mat & frame)
{
    FrameHeader fh = readFrameHeader();
    frame.create(fh.rows, fh.cols, fh.type);
    FramePixel fpx = {0, 0, 0};
    for (int i = 0; i < frame.rows; i++) {
        for (int j = 0; j < frame.cols; j++) {
            cin.read((char*) &fpx, 3);
            if (!cin)
                throw std::runtime_error("Could not read");

            frame.at<cv::Vec3b>(i, j) = cv::Vec3b(fpx.r, fpx.g, fpx.b);
        }
    }
}

long long readFramePackageFromInput(Mat & frame)
{
    long long milliseconds;
    cin.read((char*) &milliseconds, sizeof (milliseconds));
    readFrameFromInput(frame);
    return milliseconds;
}

FrameHeader readFrameHeader()
{
    if (!cin) {
        cerr << "(" << getExecutableName() << ": " << getPidId() << ") FEOFFFFFFFFFF!!!!" << endl;
    }
    FrameHeader fhead = {0, 0, 0};
    cin.read((char*) &fhead, sizeof (fhead));
    return fhead;
}

std::vector<Rect> readFaces()
{
    std::vector<Rect> result;
    // read uint32
    unsigned length = 0;
    _Rect r;
    cin.read((char*) &length, sizeof (length));
    if (!cin) throw std::runtime_error("Could not read fejsyz");
    for (unsigned i = 0; i < length; i++) {
        cin.read((char*) &r, sizeof (r));
        if (!cin) throw std::runtime_error("Could not read fejsyz");
        result.push_back(Rect(r.x, r.y, r.w, r.h));
    }
    return result;
}

void writeFrameHeader(Mat const& mat)
{
    int frameType = mat.type();
    cout.write((char*) &mat.rows, sizeof (mat.rows));
    if (!cout) throw std::runtime_error("Could not write");

    cout.write((char*) &mat.cols, sizeof (mat.cols));
    if (!cout) throw std::runtime_error("Could not write");

    cout.write((char*) &frameType, sizeof (frameType));
    if (!cout) throw std::runtime_error("Could not write");
}

void printArgv(int argc, char** argv, const char* prefix)
{
    cerr << prefix << " Print argv: (" << argc << endl;
    for (int i = 0; i < argc; i++) {
        cerr << prefix << " argv[" << i << "] = \"" << argv[i] << "\";" << endl;
    }
    cerr << prefix << " argv end" << endl;
    cerr.flush();
}
