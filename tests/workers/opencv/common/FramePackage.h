/* 
 * File:   FramePackage.h
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 30 listopada 2016, 18:48
 */

#pragma once

#include "opencv_frame_common.h"

using namespace std;
using namespace cv;

struct FramePackage
{
	int number = 0;
    /** ms */
    long long startTime;
    vector<long long> processingTimes;
    Mat frame;
    FramePackage();

    void read();
    void write() const;
    void write(Mat const& mat) const;
    pid_t pid;
    const char* filename;
    bool operator>(FramePackage const&) const;
    bool operator<(FramePackage const&) const;
	static bool sendPacketSize;
};

inline bool FramePackage::operator>(FramePackage const& f) const
{
    return this->startTime > f.startTime;
}

inline bool FramePackage::operator<(FramePackage const& f) const
{
    return this->startTime > f.startTime;
}

template<typename T>
vector<T> readVector()
{
    vector<T> v;
    unsigned size = 0;
    cin.read((char*) &size, sizeof (size));
    v.reserve(size);
    T temp;
    for (unsigned i = 0; i < size; i++) {
        cin.read((char*) &temp, sizeof (temp));
        v.push_back(temp);
    }
    return v;
}

template<typename T>
void writeVector(vector<T> const& v)
{
    unsigned size = static_cast<unsigned> (v.size());
    cout.write((char*) &size, sizeof (size));
    for (auto const& e : v) {
        cout.write((char*) &e, sizeof (e));
    }
}

template<typename T>
unsigned getVectorSize(vector<T> const& v)
{
    return static_cast<unsigned> (sizeof (unsigned) + sizeof (T) * v.size());
}
