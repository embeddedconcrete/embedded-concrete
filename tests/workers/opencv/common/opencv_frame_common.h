#pragma once

#include <opencv2/core/core.hpp>
#include <vector>
#include <iostream>
#include <string>

#ifdef WINDOWS
typedef unsigned pid_t;
#endif


using namespace cv;

struct FramePixel
{
    uchar r, g, b;
};

struct FrameHeader
{
    int rows, cols, type;
};

struct _Rect
{
    int x, y, w, h;
};



void readFrameFromInput(Mat & frame);

/**
 * 
 * @param frame
 * @return milliseconds as time of processing the frame
 */
long long readFramePackageFromInput(Mat & frame);

void writeFrame(Mat const& frame);
void writeFramePackage(Mat const& frame, long time);

FrameHeader readFrameHeader();
void writeFrameHeader(Mat const& mat);

inline int calcFrameSize(Mat const& frame)
{
    return static_cast<int> (frame.total() * frame.elemSize());
}

inline void writePacketSize(unsigned size)
{
    std::cout.write((char*) &size, sizeof (size));
}

std::vector<Rect> readFaces();

void printArgv(int argc, char** argv, const char* prefix);
