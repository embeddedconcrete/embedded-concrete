#include "FramePackage.h"
#include "io_setup.h"

bool FramePackage::sendPacketSize = true;

FramePackage::FramePackage()
{
    pid = getPidId();
    filename = getExecutableName();
}

void FramePackage::read()
{
#ifndef NDEBUG
    cerr << "(" << filename << ": " << pid << ") Reading frame..." << endl;
#endif
    cin.read((char*) &startTime, sizeof (startTime));
    cin.read((char*) &number, sizeof (number));
    processingTimes = readVector<long long>();
    readFrameFromInput(frame);
#ifndef NDEBUG
    cerr << "(" << filename << ": " << pid << ") Read frame: " << frame.cols << "x" << frame.rows << endl;
#endif
}

void FramePackage::write() const
{
    write(frame);
}

void FramePackage::write(Mat const& f) const
{
#ifndef NDEBUG
    cerr << "(" << filename << ": " << pid << ") Writing frame..." << endl;
#endif
    if (sendPacketSize) {
        unsigned size = sizeof (startTime) + sizeof(number) + getVectorSize(processingTimes) + sizeof (FrameHeader) + calcFrameSize(f);
        writePacketSize(size);
    }
    cout.write((char*) &startTime, sizeof (startTime));
    cout.write((char*) &number, sizeof (number));
    writeVector(processingTimes);
    writeFrame(f);
    cout.flush();
#ifndef NDEBUG
    cerr << "(" << filename << ": " << pid << ") Wrote frame: " << f.cols << "x" << f.rows << endl;
#endif
}

