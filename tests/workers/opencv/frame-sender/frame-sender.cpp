/* 
 * File:   main.cpp
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 2 września 2016, 12:20
 */

#include "io_setup.h"
#include "opencv_frame_common.h"
#include "FramePackage.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#include <chrono>

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    VideoCapture*cap = nullptr;
    try {
        enableBinaryStdio();
        setArgv0(argv[0]);

        bool camera = true;


        Size destSize(320, 240);
        int nthFrame = 0, frameCount = -1, waitms = 15;
        for (int i = 0; i < argc; i++) {
            if (strcmp(argv[i], "-s") == 0 && argc > i + 2) {
                destSize.width = atoi(argv[i + 1]);
                destSize.height = atoi(argv[i + 2]);
                i += 2;
            } else if (strcmp(argv[i], "-i") == 0 && argc > i + 1) {
                cap = new VideoCapture(argv[i + 1]);
                camera = false;
                i++;
            } else if (strcmp(argv[i], "--nth-frame") == 0 && argc > i + 1) {
                nthFrame = atoi(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--bare") == 0) {
                FramePackage::sendPacketSize = false;
            } else if (strcmp(argv[i], "--frame-count") == 0 && argc > i + 1) {
                frameCount = atoi(argv[i + 1]);
                i++;
            } else if (strcmp(argv[i], "--wait-ms") == 0 && argc > i + 1) {
                waitms = atoi(argv[i + 1]);
                i++;
            }
        }

        if (cap == nullptr) {
            cap = new VideoCapture(0);
        }
        if (!cap->isOpened()) {
            return -1;
        }

        Mat frame;
        FramePackage package;

        cap->read(frame);
        resize(frame, package.frame, destSize);

        if (camera) {
            for (int i = 0, j = 0; cap->grab() && (frameCount == -1 || j < frameCount); i++, j++) {
                if (i > nthFrame) {
                    cap->retrieve(frame);
                    resize(frame, package.frame, destSize);
                    auto currentTime = std::chrono::high_resolution_clock::now();
                    package.startTime = std::chrono::duration_cast<std::chrono::milliseconds>(
                            currentTime.time_since_epoch()).count();
					package.number = j;
                    package.write();
                    cout.flush();

#ifndef NDEBUG
                    cerr << "FS: ramka \n";
#endif
                    imshow("Video", package.frame);
                    if (waitKey(waitms) != -1) {
                        break;
                    }
#ifndef NDEBUG
                    cerr << "FS: ramka wys�ana \n";
#endif
                    i = 0;
                    continue;
                }
            }
        } else {
			int i = 0;
            while (1) {
                cap->read(frame);
                if (frame.empty()) break;
                resize(frame, package.frame, destSize);
                auto currentTime = std::chrono::high_resolution_clock::now();
                package.startTime = std::chrono::duration_cast<std::chrono::milliseconds>(
                        currentTime.time_since_epoch()).count();
				package.number = i++;
                package.write();
                cout.flush();
            }
        }
        delete cap;
        cap = nullptr;
    } catch (exception& exp) {
        cerr << "!!FS: " << exp.what() << endl;
        if (cap) delete cap;
        return -1;
    }

    return 0;
}
