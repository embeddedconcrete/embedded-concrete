#include "io_setup.h"
#include "opencv_frame_common.h"
#include "FramePackage.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#include <chrono>

Mat& execFilter(Mat &src, Mat&src_gray, Mat&detected_edges, double lowThreshold, double highThreshold, int kernelSize, Mat& dst);

using namespace std;
using namespace cv;

int main(int argc, char** argv)
{
    double lowThreshold = 20;
    double highThreshold = 50;
    int kernelSize = 3;

    if (argc == 2 && strcmp(argv[1], "--help") == 0) {
        cout << "Executes canny filter" << endl;
        cout << "Run: " << endl;
        cout << "\t" << argv[0] << " [minThreshold [maxThreshold [kernelSize]]]" << endl;
        cout << "Default values: " << endl;
        cout << "\tminThreshold: " << lowThreshold << endl
                << "\tmaxThreshold: " << highThreshold << endl
                << "\tkernelSize: " << kernelSize << endl;
        cout << "Output format: " << "Standard" << endl;
        return 0;
    }

    enableBinaryStdio();
    setArgv0(argv[0]);


    if (argc >= 2) {
        lowThreshold = atof(argv[1]);
    }
    if (argc >= 3) {
        highThreshold = atof(argv[2]);
    }
    if (argc >= 4) {
        kernelSize = atoi(argv[3]);
    }

    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "--bare") == 0) {
            FramePackage::sendPacketSize = false;
        }
    }

    Mat dst, src_gray, detectedEdges;
    FramePackage package;
    while (cin) {
        package.read();
        if (package.frame.empty()) continue;

        dst.create(package.frame.size(), package.frame.type());

        auto start = std::chrono::high_resolution_clock::now();

        execFilter(package.frame, src_gray, detectedEdges, lowThreshold, highThreshold, kernelSize, dst);

        auto elapsed = std::chrono::high_resolution_clock::now() - start;
        long long milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
        package.processingTimes.push_back(milliseconds);

#ifndef NDEBUG
        cerr << "CF: frame processed in " << milliseconds << "ms" << endl;
        cerr.flush();
#endif
        package.write(dst);
    }
    return 0;
}

Mat& execFilter(Mat &src, Mat&src_gray, Mat&detected_edges, double lowThreshold, double highThreshold, int kernelSize, Mat& dst)
{
    cvtColor(src, src_gray, CV_BGR2GRAY);

    blur(src_gray, detected_edges, Size(3, 3));

    Canny(detected_edges, detected_edges, lowThreshold, highThreshold, kernelSize);

    dst = Scalar::all(0);

    src.copyTo(dst, detected_edges);
    return dst;
}
