#include "io_setup.h"
#include "opencv_frame_common.h"
#include "FramePackage.h"

#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <iostream>

#include <chrono>

Mat& execFilter(Mat &src, Mat& dst, int size);

using namespace std;
using namespace cv;

int radius = 5;

int main(int argc, char** argv)
{

    if (argc == 2 && strcmp(argv[1], "--help") == 0) {
        cout << "Executes gaussian blur" << endl;
        cout << "Run: " << endl;
        cout << "\t" << argv[0] << " [-r radius] [--bare]" << endl;
        cout << "Default values: " << endl;
        cout << "\tradius: " << radius << endl;
        cout << "Output format: " << "Standard" << endl;
        return 0;
    }

    enableBinaryStdio();
    setArgv0(argv[0]);

    for (int i = 0; i < argc; i++) {
        if (strcmp(argv[i], "-r") == 0 && argc > i + 1) {
            radius = atoi(argv[i + 1]);
            if (radius % 2 == 0) {
                radius++;
            }
            i++;
        } else if (strcmp(argv[i], "--bare") == 0) {
            FramePackage::sendPacketSize = false;
        }
    }

    Mat dst;
    FramePackage package;
    while (cin) {
        package.read();
        if (package.frame.empty()) continue;

        dst.create(package.frame.size(), package.frame.type());

        auto start = std::chrono::high_resolution_clock::now();

        execFilter(package.frame, dst, radius);

        auto elapsed = std::chrono::high_resolution_clock::now() - start;
        long long milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(elapsed).count();
        package.processingTimes.push_back(milliseconds);

#ifndef NDEBUG
        cerr << "CF: frame processed in " << milliseconds << "ms" << endl;
        cerr.flush();
#endif
        package.write(dst);
    }
    return 0;
}

Mat& execFilter(Mat &src, Mat& dst, int size)
{
    GaussianBlur(src, dst, Size(size, size), 0, 0);
    return dst;
}
