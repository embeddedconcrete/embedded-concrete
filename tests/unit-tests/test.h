#pragma once

#ifdef EC_TEST_SUITE

namespace ec {
	namespace tests {

        void initSubprocessEngine(const char* argv);
        const char* getSubprocessPath(const char* file, unsigned int line);
        int runSubprocess(int argc, char** argv);

		class TestCase
		{
		public:
			TestCase(const char* file, unsigned int line);

			int run();

            TestCase* next() const { return _next; }
            const char* file() const { return _file; }
            unsigned int line() const { return _line; }

		private:
            TestCase* _next = nullptr;

            const char* _file;
            unsigned int _line;

            virtual int main() { return -1; }
            virtual int main(int argc, char** argv) { return main(); }

            virtual void setup() {}
            virtual bool validate() { return true; }

            void redirect();
            void restore();

#ifdef _WIN32
            void* inhnd_out = nullptr, *inhnd_in = nullptr;
            void* outhnd_out = nullptr, *outhnd_in = nullptr;

            void* orig_in = nullptr, *orig_out = nullptr;
            int old_out = -1, old_in = -1;
#else
            int out_fd[2];
            int in_fd[2];
            int old_out, old_in;
#endif

        protected:
            int result = -1;
            char* in; 
            unsigned long inlen;

            char* out;
            unsigned long outlen;

            char** cmd;
            unsigned long cmdlen;
        };

        extern TestCase* tests;
        extern const unsigned* tests_count;

        class Subprocess
        {
            friend int ec::tests::runSubprocess(int argc, char** argv);
        public:
            Subprocess(const char* file, unsigned int line);

            Subprocess* next() const { return _next; }

            const char* file() const { return _file; }
            unsigned int line() const { return _line; }

        private:
            Subprocess* _next = nullptr;
            const char* _file;
            unsigned int _line;

            virtual int main() { return -1; }
            virtual int main(int argc, char** argv) { return main(); }
        };
	}
}

#define BEGIN_TEST	namespace ec { namespace tests { \
						namespace { \
							class TestCaseInstance : public TestCase { \
							public: \
								TestCaseInstance(const char* u, uint32 t) : TestCase(u,t) {} \
							private:

                            //USER SUBPROCESSES GO HERE - LIKE THIS
#define SUBPROCESS(Name)    private: \
                                class _subprocess_##Name : public Subprocess \
                                { \
                                public: \
                                    using Subprocess::Subprocess; \
                                private:

                                    //SUBPROCESS CODE GOES HERE

#define END_SUBPROCESS(Name)    }; \
                            protected: \
                                const _subprocess_##Name Name = _subprocess_##Name{ __FILE__, __LINE__ }; \
                                const char* getSubprocessPath(_subprocess_##Name const&) const { return ec::tests::getSubprocessPath(__FILE__, __LINE__); } \
                            private:

                                //TEST CODE GOES HERE

#define END_TEST			} __test_case(__FILE__, __LINE__); \
						} \
					} }

#define TEST_FUNC override

#else

#define TEST_FUNC

#endif
