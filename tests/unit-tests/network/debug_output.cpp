#include "test.h"
#include "socket.h"

#include <regex>
#include <string>
using namespace std;

#ifdef WINDOWS
#include <winsock2.h>
#include <WS2tcpip.h>

class RawUdpSocket
{
public:
    RawUdpSocket()
    {
        ZeroMemory(&hints, sizeof(hints));
        hints.ai_family = AF_UNSPEC;
        hints.ai_socktype = SOCK_DGRAM;
        hints.ai_protocol = IPPROTO_UDP;

        auto iResult = getaddrinfo(nullptr, nullptr, &hints, &result);
        if (iResult != 0)
            throw std::runtime_error(makestr("getaddrinfo failed: ", iResult));

        sock = socket(result->ai_family, result->ai_socktype, result->ai_protocol);
        if (sock == INVALID_SOCKET)
            throw std::runtime_error(makestr("socket failed: ", WSAGetLastError()));
    }

    ~RawUdpSocket()
    {
        closesocket(sock);
    }

    void send(std::string const& to, ec::port_t port, memblock const& data)
    {
        struct sockaddr_in addr;
        ZeroMemory(&addr, sizeof(addr));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(port);
        addr.sin_addr.S_un.S_addr = inet_addr(to.c_str());

        auto result = sendto(sock, data.data(), static_cast<int>(data.size()), 0, (struct sockaddr*)&addr, sizeof(addr));
        if (result == SOCKET_ERROR)
            throw std::runtime_error(makestr("sendto failed: ", WSAGetLastError()));
        if (result != data.size())
            throw std::runtime_error(makestr("sendto: Could not send all data"));
    }

    void recv(std::string& from, ec::port_t& port, storage& data)
    {
        struct sockaddr_in addr;
        ZeroMemory(&addr, sizeof(addr));
        int len = 0;
        auto result = recvfrom(sock, data.end(), static_cast<int>(data.available()), 0, (struct sockaddr*)&addr, &len);
        if (result == SOCKET_ERROR)
            throw std::runtime_error(makestr("recvfrom failed: ", WSAGetLastError()));

        data.update_size(result);
        char buff[INET_ADDRSTRLEN];
        inet_ntop(addr.sin_family, &addr.sin_addr, buff, INET_ADDRSTRLEN);
        from = buff;
        port = ntohs(addr.sin_port);
    }

private:
    struct addrinfo* result = NULL, *ptr = NULL, hints;
    SOCKET sock = INVALID_SOCKET;
};

#endif

BEGIN_TEST

string parseUdpAddr(string const& addr, port_t& outport)
{
    return "";
}

int main() TEST_FUNC
{
    ec::DebugOutput dbg{};
    {
        auto result = dbg.bind("udp://127.0.0.1:0");
        if (!result)
            throw runtime_error(result.descr());
    }
    if (dbg.getBoundAddrs().size() != 1)
        return 1;


    auto addr = dbg.getBoundAddrs().front();
    port_t port;
    auto ip = parseUdpAddr(addr, port);

    char data[] = { 1, 2, 3, 4, 5, 6 };

//    RawUdpSocket raw;
//    raw.send(ip, port, memblock::readonly(data));

    //TODO: end test

    return 0;
}

END_TEST
