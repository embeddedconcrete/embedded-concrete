
#include "test.h"

#include "socket.h"
#include <iostream>

BEGIN_TEST

int main()
{
    ec::DataInput in;
    ec::DataOutput out;

    in.connect("tcp://127.0.0.1:5555");
    out.bind("tcp://127.0.0.1:5555");

    char tmp[500];
    for (int i = 0; i < sizeof(tmp); ++i)
        tmp[i] = i % 100;

	memblock to_send = memblock::readonly(tmp);
	storage recv;

    out.send(memblock::readonly(tmp));
    auto data = in.recv();
    while (data) {
		recv.append(data.get(), true);
        data = data.more();
    }

	if (recv.size() != to_send.size())
		return 1;

	return memcmp(recv.data(), to_send.data(), recv.size());
}

END_TEST
