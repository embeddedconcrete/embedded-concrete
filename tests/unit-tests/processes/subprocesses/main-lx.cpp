#include <iostream>

#ifdef _WIN32
#include <windows.h>
#else 
#include <unistd.h>
#endif


using namespace std;

void sleep_s(unsigned sec) {

#ifdef _WIN32
    Sleep(sec*1000);
#else
    sleep(sec);
#endif

}


int main(int argc, char**argv) {
	cout<<"Jestem w procesie dziecka!\n";
	cout<<"\tParametry:\n";
	for(int i=0;i<argc;i++) {
		cout<<argv[i]<<"\n";
	}
	for(int i=0;i<10;i++) {
		sleep_s(1);
		int number = 0;
		cout<<i<<": ";
		if(cin>>number) {
			cout<<"odczytano "<<number<<" " ;
		}
		for(int j=0;j<30;j++) {
			cout<<"#";
		}
		cout<<"\n";
	}
	cout<<"Koniec procesu\n\n";
	return -5;
}
