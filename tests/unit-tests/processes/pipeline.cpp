#include "test.h"

#include <cstdlib>
#include <iostream>
#include <cstring>
#include <fstream>

#include "process_wrapper.h"
using namespace std;
using namespace ec;

BEGIN_TEST


int main(int argc, char** argv) TEST_FUNC
{
    try {
        process_wrapper frameSender{ "/home/vortex41/EmbededConcrete/repo/embedded-concrete/tests/bin/frame-sender" };
        process_wrapper faceDetector{ "/home/vortex41/EmbededConcrete/repo/embedded-concrete/tests/bin/face-detector" };
        process_wrapper imageFilter{ "/home/vortex41/EmbededConcrete/repo/embedded-concrete/tests/bin/image-filter" };
//        process_wrapper frameSender{ "/home/vortex41/EmbededConcrete/repo/embedded-concrete/tests/bin/simple-source" };
//        process_wrapper faceDetector{ "/home/vortex41/EmbededConcrete/repo/embedded-concrete/tests/bin/simple-multiply" };
//        process_wrapper imageFilter{ "/home/vortex41/EmbededConcrete/repo/embedded-concrete/tests/bin/simple-reciever" };

        frameSender.setFetchMode(process_wrapper::fixed_packets);
        frameSender.setPacketSize(1024);
        faceDetector.setFetchMode(process_wrapper::fixed_packets);
        faceDetector.setPacketSize(1024);
        imageFilter.setFetchMode(process_wrapper::fixed_packets);
        imageFilter.setPacketSize(1024);


        frameSender.start(StringList{"argument"});
        faceDetector.start(StringList{"argument"});
        imageFilter.start(StringList{"argument"});
		
		while(1) {
			auto mem = frameSender.try_read();
			if(mem) {
				cerr<<"Piszę do FD "<<mem.size()<<"B\n";
				faceDetector.write(mem);
			}
			auto mem2 = faceDetector.try_read();
			if(mem2) {
				cerr<<"Piszę do IF "<<mem.size()<<"B\n";
				imageFilter.write(mem2);
			}
		}

    } catch (std::exception const& e) {
        cout << "Unhandled std::exception: " << e.what() << endl;
        return -1;
    } catch (...) {
        cout << "Unhandled exception!" << endl;
        return -1;
    }

    return 0;
}

END_TEST
