#include "test.h"

#include <cstdlib>
#include <iostream>
#include <cstring>
#include <fstream>

#include "process_wrapper.h"
using namespace std;
using namespace ec;

BEGIN_TEST

SUBPROCESS(Test1)

int main(int argc, char** argv) TEST_FUNC
{
    cout << "Jestem w procesie dziecka!\n";
    cout << "\tParametry:\n";
    for (int i = 1; i<argc; i++) {
        cout << argv[i] << "\n";
    }
    for (int i = 0; i<10; i++) {
        int number = 0;
        cout << i << ": ";
        if (cin >> number) {
            cout << "odczytano " << number << " ";
        }
        for (int j = 0; j<5; j++) {
            cout << "#";
        }
        cout << "\n";
    }
    cout << "Koniec procesu\n\n";
    return -5;
}

END_SUBPROCESS(Test1)

int main(int argc, char** argv) TEST_FUNC
{
    try {
        process_wrapper pw{ getSubprocessPath(Test1) };
        storage m = storage::allocate<char>(20);

        pw.setFetchMode(process_wrapper::one_shot);

        pw.start(StringList{"argument"});

        char testData[] = "1 2 3 4 5 6 7 8\n";
        pw.write(memblock::readonly(testData));
        pw.close_input();

        while (auto mem = pw.read()) {
            cout << "size: " << mem.size() << "\n";
            cout << "data: " << string{ mem.data(), mem.size() } << endl;
        }


    } catch (std::exception const& e) {
        cout << "Unhandled std::exception: " << e.what() << endl;
        return -1;
    } catch (...) {
        cout << "Unhandled exception!" << endl;
        return -1;
    }

    return 0;
}

bool validate() TEST_FUNC
{
#ifdef WINDOWS
    string expected_output = "size: 265\n"
        "data: "
        "Jestem w procesie dziecka!\r\n" //child output begin (note child output has \r\n instead of \n on Windows, while parent process has unchanged \n)
        "\tParametry:\r\n"
        "argument\r\n"
        "0: odczytano 1 #####\r\n"
        "1: odczytano 2 #####\r\n"
        "2: odczytano 3 #####\r\n"
        "3: odczytano 4 #####\r\n"
        "4: odczytano 5 #####\r\n"
        "5: odczytano 6 #####\r\n"
        "6: odczytano 7 #####\r\n"
        "7: odczytano 8 #####\r\n"
        "8: #####\r\n"
        "9: #####\r\n"
        "Koniec procesu\r\n\r\n"
        "\n"; //child output end
#else
    string expected_output = "size: 250\n"
        "data: "
        "Jestem w procesie dziecka!\n" //child output begin (note child output has \r\n instead of \n on Windows, while parent process has unchanged \n)
        "\tParametry:\n"
        "argument\n"
        "0: odczytano 1 #####\n"
        "1: odczytano 2 #####\n"
        "2: odczytano 3 #####\n"
        "3: odczytano 4 #####\n"
        "4: odczytano 5 #####\n"
        "5: odczytano 6 #####\n"
        "6: odczytano 7 #####\n"
        "7: odczytano 8 #####\n"
        "8: #####\n"
        "9: #####\n"
        "Koniec procesu\n\n"
        "\n"; //child output end
#endif

    string outstr{ out, outlen };
    return (outstr == expected_output);
}

END_TEST
