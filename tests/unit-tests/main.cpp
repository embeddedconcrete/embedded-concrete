#include "test.h"
#include <iostream>
using namespace std;

#ifdef WINDOWS
#include <winsock2.h>
#pragma comment(lib, "Ws2_32.lib")
#endif

int main(int argc, char** argv)
{
    if (argc > 1)
        return ec::tests::runSubprocess(argc, argv);

#ifdef WINDOWS
    WSADATA wsa;
    if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
    {
        cout << "Failed to initialize WinSock. Error Code : " <<  WSAGetLastError();
        return -1;
    }
#endif

    ec::tests::initSubprocessEngine(argv[0]);

    ec::tests::TestCase* curr = ec::tests::tests;

    unsigned idx = 1;
    unsigned failed = 0;
    while (curr)
    {
        cout << "Running test " << curr->file() << ":" << curr->line() << " (" << idx++ << "/" << *ec::tests::tests_count << "): ";
        auto result = curr->run();
        cout << result << endl;
        if (result != 0)
            ++failed;
        curr = curr->next();
    }

    if (failed)
        cout << failed << " tests failed" << endl;

#ifdef WINDOWS
    WSACleanup();
#endif

    return failed;
}
