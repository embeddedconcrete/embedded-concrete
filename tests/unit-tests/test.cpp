#include "test.h"
using namespace ec::tests;

#include <iostream>
#include <sstream>
#include <vector>
#include <cstring>

using namespace std;

#ifdef _WIN32
#define NOMINMAX
#include <Windows.h>
#include <io.h>
#else
#include <unistd.h>
#include <sys/ioctl.h>
#endif

TestCase* ec::tests::tests = nullptr;

static unsigned _tests_count = 0;
const unsigned* ec::tests::tests_count = &_tests_count;

static string execName;
static string tmp;

static ec::tests::Subprocess* subprocesses = nullptr;

void ec::tests::initSubprocessEngine(const char * argv)
{
    execName = argv;
}

const char * ec::tests::getSubprocessPath(const char * file, unsigned int line)
{
    stringstream ss;
    ss << execName << " " << file << ":" << line;
    tmp = ss.str();
    return tmp.c_str();
}

int ec::tests::runSubprocess(int argc, char ** argv)
{
    tmp = argv[1];
    argv[1] = argv[0];
    --argc;

    auto pos = tmp.find_last_of(':');
    if (pos == tmp.npos)
        return -1;

    string file = tmp.substr(0, pos);
    unsigned int line;
    stringstream ss{ tmp.substr(pos+1) };
    ss >> line;

    auto subp = subprocesses;
    while (subp)
    {
        if (subp->file() == file && subp->line() == line)
            return subp->main(argc, argv + 1);

        subp = subp->next();
    }

    return -1;
}


TestCase::TestCase(const char* file, unsigned int line) : _file(file), _line(line)
{
    _next = tests;
    tests = this;
    ++_tests_count;
}

int TestCase::run()
{
    setup();

    vector<char*> opts;
    char* buff = new char[execName.size() + 1];
    memcpy(buff, execName.c_str(), execName.size());
    buff[execName.size()] = 0;

    opts.push_back(buff);
    for (unsigned long i = 0; i < cmdlen; ++i)
        opts.push_back(cmd[i]);

    string msg;

    redirect();
    try {
        result = main(static_cast<int>(opts.size()), opts.data());
    }
    catch (exception& exp) {
        msg = exp.what();
    }
    catch (...) {
        msg = "Unhandled unknown exception";
    }

    restore();
    if (!msg.empty()) {
        cout << "Error occured while running test: " << msg << endl;
        result = -1;
    }
    else if (!validate())
        result = -2;

    delete[] out;
    out = nullptr;
    outlen = 0;

    delete[] buff;
    buff = nullptr;
    opts.clear();

    return result;
}

void TestCase::redirect()
{
    cout.flush();
    fflush(stdout);

#ifdef _WIN32
    orig_in = GetStdHandle(STD_INPUT_HANDLE);
    orig_out = GetStdHandle(STD_OUTPUT_HANDLE);

    if (FALSE == CreatePipe(&inhnd_out, &inhnd_in, nullptr, 0))
        throw runtime_error("Could not create pipe");

    if (FALSE == CreatePipe(&outhnd_out, &outhnd_in, nullptr, 0))
        throw runtime_error("Could not create pipe");

    if (FALSE == SetStdHandle(STD_OUTPUT_HANDLE, outhnd_in))
        throw runtime_error("Could not redirect stdout");

    if (FALSE == SetStdHandle(STD_INPUT_HANDLE, inhnd_out))
        throw runtime_error("Could not redirect stdin");

    /* HACK BEGIN */

    old_out = _dup(_fileno(stdout));
    old_in = _dup(_fileno(stdin));

    auto new_out_fd = _open_osfhandle((intptr_t)outhnd_in, 0);
    auto new_in_fd = _open_osfhandle((intptr_t)inhnd_out, 0);

    auto new_out = _fdopen(new_out_fd, "w");
    auto new_in = _fdopen(new_in_fd, "r");

    _dup2(_fileno(new_out), _fileno(stdout));
    _dup2(_fileno(new_in), _fileno(stdin));

    /* HACK END */

    if (in && inlen > 0)
        if (FALSE == WriteFile(inhnd_in, in, inlen, nullptr, nullptr))
            throw runtime_error("Could not populated redirected stdin");

    delete[] in;
    in = nullptr;
    inlen = 0;

    CloseHandle(inhnd_in);
#else
    if (pipe(out_fd))
        throw runtime_error("Could not create pipe for unit-test");
    if (pipe(in_fd))
        throw runtime_error("Could not create pipe for unit-test");
    
    old_out = dup(fileno(stdout));
    old_in = dup(fileno(stdin));

    dup2(out_fd[1], fileno(stdout));
    dup2(in_fd[0], fileno(stdin));

    if (in && inlen > 0)
    {
        auto res = write(in_fd[1], in, inlen);
        if (res != inlen)
            throw runtime_error("Could not write unit-test's input");
    }

    delete[] in;
    in = nullptr;
    inlen = 0;

    close(in_fd[1]);
#endif
}

void TestCase::restore()
{
    cout.flush();
    fflush(stdout);

#ifdef _WIN32
    /* HACK BEGIN */

    //AFAIK: _dup2 closes 'new_out' (fact from MSDN) which is stream for 'new_out_fd' (so it's also closed?)
    // which is fd for os handle so there's implicit CloseHandle on pipe end (which is also a fact if 'new_out_fd' is in fact closed)
    //because of this _dup2 is the only thing we do here and below we're closing only 2 pipe ends - this is the only combination which didn't
    //get me an error during application termination
    _dup2(old_out, _fileno(stdout));
    _dup2(old_in, _fileno(stdin));

    /* HACK END */

    SetStdHandle(STD_OUTPUT_HANDLE, orig_out);
    SetStdHandle(STD_INPUT_HANDLE, orig_in);

    if (FALSE == PeekNamedPipe(outhnd_out, nullptr, 0, nullptr, &outlen, nullptr))
        throw runtime_error("Could not peek stdout pipe");

    if (outlen != 0)
    {
        out = new char[outlen];
        ReadFile(outhnd_out, out, outlen, nullptr, nullptr);
    }

    CloseHandle(outhnd_out);
#else

    dup2(old_out, fileno(stdout));
    dup2(old_in, fileno(stdin));

    int err = ioctl(out_fd[0], FIONREAD, &outlen);
    if (err)
        throw runtime_error("Could not peek stdout pipe");

    if (outlen != 0)
    {
        out = new char[outlen];
        auto res = read(out_fd[0], out, outlen);
        if (res != outlen)
            throw runtime_error("Could not read from unit-test's output");
    }

    close(out_fd[0]);
#endif
}

Subprocess::Subprocess(const char * file, unsigned int line) : _file(file), _line(line)
{
    _next = subprocesses;
    subprocesses = this;
}
