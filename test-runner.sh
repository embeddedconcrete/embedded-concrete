#!/bin/bash

if [[ $# -lt 1 ]]; then
	echo "EC test runner.";
	echo "Usage: $0 testFile";
	exit;
fi;

GREEN="$(tput setaf 2)"
BOLD="$(tput bold)"
RESET="$(tput sgr0)"

cd `dirname "$0"`;
archiveDirectory="`pwd`/testOut";
echo "ArchiveDirectory: $archiveDirectory";

tempDirName="output";

testName=$(basename $1);
bareFileName="${testName%.*}";

testOutDir="$archiveDirectory/$bareFileName-`date +%y-%m-%d_%H.%M.%S`";
mkdir -p "$testOutDir";

testFile=$1;

testBinsDir="tests/bin/workers";

netfile="network.desc";
logfileName="ec.log";

options="-netfile $netfile ";

if [[ $2 == "local" ]]; then
	options="";
fi;

while read line; do
	tempDir="$testBinsDir/$tempDirName";
	mkdir -p "$tempDir";

	tokens=( $line );
	testName=${tokens[0]};
	pcWorkers=${tokens[1]};
	clusterFile=${tokens[2]};
	testDir="$testOutDir/$testName";
	logfile="$testDir/$logfileName";

	mkdir -p $testDir;
	echo "${GREEN}${BOLD}Running test $testName...${RESET}";
	__pwd=`pwd`;
	cd bin;
	time bash -c "time ./ec-daemon $options -maxWorkers $pcWorkers -log $logfile -run_cluster \"../$testBinsDir/$clusterFile\" 1>&2" 2>&1 | tee "$testDir/ec-output.log";
	cd $__pwd;
	echo "${GREEN}moving output dir... $tempDir to $testDir/$tempDirName ${RESET}";
	mv "$tempDir" "$testDir/$tempDirName";
	echo "${GREEN}moving done${RESET}";
	echo "${GREEN}${BOLD}test $testName finished ${RESET}";
	
done < $testFile;

