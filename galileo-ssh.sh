#!/bin/bash

setupGalileo()
{
	tmux send-keys "export TERM=xterm" C-m
	tmux send-keys "ssh root@$1" C-m
	tmux send-keys "cd ec/bin/" C-m
	tmux send-keys "./ec-daemon -maxWorkers 1" C-m
}

SESSION="galileo-ssh";

tmux -2 new-session -d -s $SESSION;

tmux new-window -t $SESSION:1 -n "Galileo ssh 1";

# ssh window
tmux split-window -h;
#setup ssh to galileos
tmux select-pane -t 0;
setupGalileo "192.168.0.2";

tmux split-window -v
setupGalileo "192.168.0.3";

tmux select-pane -t 2;
setupGalileo "192.168.0.4";

tmux split-window -v
setupGalileo "192.168.0.5";


tmux new-window -t $SESSION:2 -n "Galileo ssh 2";

# ssh window
tmux split-window -h;
#setup ssh to galileos
tmux select-pane -t 0;
setupGalileo "192.168.0.6";

tmux split-window -v
setupGalileo "192.168.0.7";

tmux select-pane -t 2;
setupGalileo "192.168.0.8";


tmux -2 attach-session -t $SESSION
