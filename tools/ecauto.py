import sys
import ecauto.target as target
import ecauto.jobs as jobs
from ecauto.os import chooseByOs
from ecauto.config import Step, Config
from ecauto.engine import Engine, TargetTemplate

def makePurge(purge, config):
    purge.addBuildStep(jobs.RmDir(path='///project'))
    purge.addBuildStep(jobs.RmDir(path='///tmp'))
    purge.addBuildStep(jobs.RmDir(path='///dep'))
    purge.addBuildStep(jobs.RmDir(path='///tests/zeromq'))
    purge.addBuildStep(jobs.RmDir(path='///tests/bin'))
    purge.addBuildStep(jobs.RmDir(path='///bin'))

def makeClean(clean, config):
    clean.addBuildStep(jobs.RmDir(path='///project'))
    clean.addBuildStep(jobs.RmDir(path='///tests/zeromq'))
    clean.addBuildStep(jobs.RmDir(path='///tests/bin'))
    clean.addBuildStep(jobs.RmDir(path='///bin'))
    
def makeDeps(zeromq, config):
    if Step.make_tests in config.steps:
        config.addFlag('ZMQ_BUILD_TESTS')

    if Step.get not in config.steps:
        zeromq.suppressDownload()
    else:
        zeromq.addConfigureStep(jobs.Cd('libzmq-master'))
        zeromq.addConfigureStep(jobs.FileRewrite('src/fd.hpp', '<windows.hpp>', '"windows.hpp"'))
        zeromq.addConfigureStep(jobs.FileRewrite('src/udp_address.cpp', 'if (port == 0) {\n        errno = EINVAL;\n        return -1;\n    }', '/*EC: allow autochoose port (0), because why not?\n    if (port == 0) {\n        errno = EINVAL;\n        return -1;\n    }*/'))
    
    if Step.get not in config.steps:
        zeromq.addConfigureStep(jobs.Cd('libzmq-master'))
        
    if Step.configure in config.steps:
        zeromq.addConfigureStep(jobs.MkDir(path='project', clean=True))
        zeromq.addConfigureStep(jobs.CMakeRun(args=config.getCmakeConfArgs(), wdir='project'))
        
    if Step.make in config.steps or Step.clean in config.steps:
        zeromq.addBuildStep(jobs.CMakeRun(args=config.getCmakeBuildArgs(), wdir='project'))
        if Step.make in config.steps:
            zeromq.addInstallStep(jobs.Copy(
                chooseByOs({
                    'windows': 'project/bin/Release/libzmq{{.*}}.dll',
                    'linux': 'project/lib/{{.*}}.so{{.*}}'
                }), '///bin'))
            zeromq.addInstallStep(jobs.Copy(
                chooseByOs({
                    'windows': 'project/lib/Release/{{.*}}.lib',
                    'linux': 'project/lib/{{.*}}.a'
                }), '///lib'))
            zeromq.addInstallStep(jobs.Copy('include/{{.*}}', '///include/{name}/'))

    if Step.test in config.steps:
        zeromq.addInstallStep(jobs.Copy(
            chooseByOs({
                'windows': 'project/bin/Release/test{{.*}}.exe',
                'linux': 'project/bin/test{{.*}}'
            }), '///tests/{name}'))
        zeromq.addInstallStep(jobs.Copy(chooseByOs({
                'windows': '///bin/libzmq{{.*}}.dll',
                'linux': '///bin/{{.*}}.so{{.*}}'
            }), '///tests/{name}'))
        zeromq.addTestStep(jobs.RunAll('///tests/{name}/test{{.*}}', expected=0))
    
def makeEc(ec, config):
    if Step.make_tests in config.steps:
        config.addFlag('EC_BUILD_TESTS')

    if Step.configure in config.steps:
        ec.addConfigureStep(jobs.BuildOptionsGenerator())
        ec.addConfigureStep(jobs.MkDir(path='project', clean=True))
        ec.addConfigureStep(jobs.CMakeRun(args=config.getCmakeConfArgs(), wdir='project'))
        
    if Step.make in config.steps or Step.clean in config.steps:
        ec.addBuildStep(jobs.CMakeRun(args=config.getCmakeBuildArgs(), wdir='project'))
        
    if Step.test in config.steps:
        ec.addInstallStep(jobs.Copy(chooseByOs({
                'windows': '///bin/libzmq{{.*}}.dll',
                'linux': '///bin/{{.*}}.so{{.*}}'
            }), '///tests/bin'))
        ec.addTestStep(jobs.RunAll(chooseByOs({
                'windows': '///tests/bin/ec-test{{.*}}.exe',
                'linux': '///tests/bin/ec-tests{{.*}}'
            }), expected=0, wdir='///tests/bin/'))
    
targets = [
    TargetTemplate('purge', target.RootAction(), makePurge, is_default=False),
    TargetTemplate('clean', target.RootAction(), makeClean, is_default=False),
    TargetTemplate('cmake', target.CMake(chooseByOs({'windows': 'https://cmake.org/files/v3.5/cmake-3.5.2-win32-x86.zip', 'linux': 'https://cmake.org/files/v3.5/cmake-3.5.2-Linux-i386.tar.gz'})), prep_fun=None, is_default=True, default_steps=[Step.get]),
    TargetTemplate('deps', target.Dependency('zeromq', 'https://github.com/zeromq/libzmq/archive/master.zip'), makeDeps, is_default=True, default_steps=[Step.get, Step.configure, Step.make], fixed_flags=['ENABLE_DRAFTS']),
    TargetTemplate('ec', target.RootAction(), makeEc, is_default=True, default_steps=[Step.configure, Step.make])
]

en = Engine(targets)
en.configure(sys.argv[1:])
en.prepare()
if en.run() == False:
    sys.exit(1)


