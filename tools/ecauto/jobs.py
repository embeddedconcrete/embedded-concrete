import subprocess
import shutil
from multiprocessing import cpu_count

from ecauto.os import chooseByOs
from ecauto.explore import getfiles
import os

class Job:
    def __init__(self, tool, args, shell, wdir=None):
        self.tool = tool
        self.args = args
        self.wdir = wdir
        self.exitcode = False
        self.success = False
        
    def run(self, module):
        self.success = False
        wdir = module.getpath(self.wdir)
        oldcwd = None
        if self.wdir != None:
            oldcwd = os.getcwd()
            os.chdir(wdir)

        print("      Run process: " + self.tool + " " + " ".join(self.args))
        proc = subprocess.Popen([self.tool] + self.args)
        proc.wait()
        if oldcwd != None:
            os.chdir(oldcwd)

        self.exitcode = proc.returncode
        self.success = (proc.returncode == 0)


class CMakeRun(Job):
    def __init__(self, args = [], wdir=None):
        super(CMakeRun, self).__init__('', args, False, wdir)

    def run(self, module):
        self.success = False
        match_patern = chooseByOs({
            'windows': module.getpath('///tools/cmake/cmake-{{[0-9.]+}}-win32{{.*}}/bin/cmake.exe'),
            'linux': module.getpath('///tools/cmake/cmake-{{[0-9.]+}}-Linux{{.*}}/bin/cmake')
        })
        matches = getfiles(match_patern)
        if len(matches) != 1:
            print('Could not determine cmake binaries! Found matches: ' + repr(matches))
            self.cmake = ''
        else:
            self.cmake = os.path.abspath(matches[0])

        self.tool = self.cmake
        super(CMakeRun, self).run(module)
        
class RunAll(Job):
    def __init__(self, pattern, expected, args=[], wdir=None):
        super(RunAll, self).__init__('', args, False, wdir)
        self.pattern = pattern
        self.expected = expected
        
    def run(self, module):
        self.success = False
        success = True
        tests = 0
        pattern = module.getpath(self.pattern)
        files = getfiles(pattern)
        good = 0
        if len(files) > 0:
            print("      Got " + str(len(files)) + " tests by pattern: " + pattern)
        for file in files:
            print("      Test " + str(tests) + ": " + file)
            self.tool = file
            super(RunAll, self).run(module)
            if self.exitcode != self.expected or not self.success:
                print("      Test failed")
                success = False
            else:
                print("      Test OK")
                good += 1
                
        self.success = success
        if len(files) == 0:
            print("      Warning: non tests were run!")
        else:
            print("      " + str(good) + "/" + str(len(files)) + " test passed")

class MkDir:
    def __init__(self, path, clean):
        self.dir = path
        self.clean = clean
        self.success = False
        
    def run(self, module):
        self.success = False
        dir = module.getpath(self.dir)
        exists = os.path.exists(dir)
        if self.clean and exists:
            shutil.rmtree(dir)
        elif exists:
            self.success = True
            return
            
        os.makedirs(dir)
        self.success = True
        
class RmDir:
    def __init__(self, path):
        self.dir = path
        self.success = False
        
    def run(self, module):
        self.success = False
        dir = module.getpath(self.dir)
        exists = os.path.exists(dir)
        if exists and os.path.isdir(dir):
            shutil.rmtree(dir)
            self.success = True
        elif not exists:
            self.success = True
        
class Cd:
    def __init__(self, cd):
        self.cd = cd
        self.success = False
        
    def run(self, module):
        self.success = False
        matches = getfiles(module.getpath(self.cd))
        if len(matches) != 1:
            print(os.getcwd())
            print(self.cd)
            raise Exception('cd fail, matches: ' + repr(matches))
        
        os.chdir(matches[0])
        self.success = True
        
class Copy:
    def __init__(self, src, dst, clean=False):
        self.src = src
        self.dst = dst
        self.clean = clean
        self.success = False
        
    def run(self, module):
        self.success = False
        src = module.getpath(self.src)
        dst = module.getpath(self.dst)
        
        matches = getfiles(src)
        if len(matches) > 0:
            if not os.path.isdir(dst):
                os.makedirs(dst)
            elif self.clean:
                shutil.rmtree(dst)
                os.makedirs(dst)
                
            for f in matches:
                name = os.path.basename(f)
                fdst = os.path.join(dst, name)
                
                if os.path.isdir(f):
                    if os.path.isdir(fdst):
                        shutil.rmtree(fdst)
                        
                    shutil.copytree(f, fdst)
                else:
                    shutil.copy2(f, fdst)
                    
        self.success = True
            
class FileRewrite:
    def __init__(self, file, oldval, newval):
        self.f = file
        self.old = oldval
        self.new = newval
        self.success = False
        
    def run(self, module):
        self.success = False
        bytes = ""
        print("      Replacing %s with %s in file %s" % (self.old, self.new, module.getpath(self.f)))
        with open(module.getpath(self.f), 'r') as file:
            bytes = file.read().replace(self.old, self.new)
            self.success = True
            
        if (self.success):
            self.success = False
            with open(module.getpath(self.f), 'w') as file:
                file.write(bytes)
                self.success = True
            
class BuildOptionsGenerator:
    def __init__(self):
        self.success = False
        
    def run(self, module):
        self.success = False
        with open(module.getpath('///tmp/BuildOptions.cmake'), 'w') as file:
            file.write('set(EC_CPUS_COUNT "{}")'.format(chooseByOs({ 'windows': cpu_count(), 'linux': cpu_count()+1 })))
            self.success = True
