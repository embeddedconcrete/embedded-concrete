import os
from zipfile import ZipFile, ZipInfo

from ecauto.utils import copyfileobj
from ecauto.printing import update_print

# ZipFile _write_member function modified to use reporting copyfileobj
def _extract_member(self, member, targetpath, pwd, callback):
    """Extract the ZipInfo object 'member' to a physical
       file on the path targetpath.
    """
    # build the destination pathname, replacing
    # forward slashes to platform specific separators.
    arcname = member.filename.replace('/', os.path.sep)
    
    if os.path.altsep:
        arcname = arcname.replace(os.path.altsep, os.path.sep)
    # interpret absolute pathname as relative, remove drive letter or
    # UNC path, redundant separators, "." and ".." components.
    arcname = os.path.splitdrive(arcname)[1]
    invalid_path_parts = ('', os.path.curdir, os.path.pardir)
    arcname = os.path.sep.join(x for x in arcname.split(os.path.sep)
                               if x not in invalid_path_parts)
    if os.path.sep == '\\':
        # filter illegal characters on Windows
        arcname = self._sanitize_windows_name(arcname, os.path.sep)

    targetpath = os.path.join(targetpath, arcname)
    targetpath = os.path.normpath(targetpath)

    # Create all upper directories if necessary.
    upperdirs = os.path.dirname(targetpath)
    if upperdirs and not os.path.exists(upperdirs):
        os.makedirs(upperdirs)

    if member.filename[-1] == '/':
        if not os.path.isdir(targetpath):
            os.mkdir(targetpath)
        return targetpath

    with self.open(member, pwd=pwd) as source, \
         open(targetpath, "wb") as target:
        copyfileobj(source, target, callback)

    return targetpath
        
def extract(self, member, path=None, pwd=None, callback=None):
    """Extract a member from the archive to the current working directory,
       using its full name. Its file information is extracted as accurately
       as possible. `member' may be a filename or a ZipInfo object. You can
       specify a different directory using `path'.
    """
    if not isinstance(member, ZipInfo):
        member = self.getinfo(member)

    if path is None:
        path = os.getcwd()

    return _extract_member(self, member, path, pwd, callback)

def extractall(self, path=None, members=None, pwd=None, callback=None):
    """Extract all members from the archive to the current working
       directory. `path' specifies a different directory to extract to.
       `members' is optional and must be a subset of the list returned
       by namelist().
    """
    if members is None:
        members = self.namelist()

    copied = 0 #copied from already extracted members
    if callback is not None:
        single_file_callback = (lambda done: callback(done+copied))
    else:
        single_file_callback = None
        
    for zipinfo in members:
        extract(self, zipinfo, path, pwd, single_file_callback)
        copied += self.getinfo(zipinfo).file_size

def unpack(archive_file, dst):
    archive = ZipFile(archive_file)
    total = 0
    for info in archive.infolist():
        total += info.file_size

    extractall(archive, dst, None, None, lambda done: update_print(done, total))
    print()
