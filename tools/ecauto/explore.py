import os
import re

def _check_single_dir(dir, rem):
    curr = rem[0]
    rem = rem[1:]
    
    result = []
    for f in os.listdir(dir):
        if not re.fullmatch(curr, f):
            continue

        if (len(rem) > 0 and os.path.isdir(os.path.join(dir, f))) or len(rem) == 0:
            result.append(os.path.join(dir, f))
            
    if len(rem) > 0:
        ret = []
        for next in result:
            ret += _check_single_dir(next, rem)
        result = ret
    
    return result

def getfiles(patern):
    res = []
    find_subre = re.compile('{{(.*?)}}')
    offset = 0
    while True:
        subre = find_subre.search(patern[offset:])
        if subre == None:
            break
            
        b,e = subre.span()
        b += offset
        e += offset
        res.append(subre.group(1))
        patern = patern[:b] + '{' + str(len(res)) +'}' + patern[e:]
        offset = b+len(str(len(res)))+2

    patern = os.path.normpath(patern)
    dirs = []
    tmp_dirs = patern.split(os.sep)
    res_idx = 1

    replace_re = re.compile('{([0-9]+?)}')
    for d in tmp_dirs:
        doff = 0
        while True:
            repl = replace_re.search(d[doff:])
            if repl == None:
                break

            b,e = repl.span()
            b += doff
            e += doff
            d = d[:b] + '"' + res[int(repl.group(1))-1] + '"' + d[e:]
            doff = b+len(res[int(repl.group(1))-1])+2
            
        dirs.append(('"'+d+'"').replace('""', ''))

    tmp_dirs = dirs
    dirs = []
    espace_re = re.compile('"(.*?)"')
    for d in tmp_dirs:
        doff = 0
        while True:
            esc = espace_re.search(d[doff:])
            if esc == None:
                break

            b,e = esc.span()
            b += doff
            e += doff
            newtxt = re.escape(esc.group(1))
            d = d[:b] + newtxt + d[e:]
            doff = b+len(newtxt)

        dirs.append(d)

    if dirs[0] == '':
        return _check_single_dir('/', dirs[1:])
    elif re.fullmatch('[A-Z]\\\:', dirs[0]):
        # if dirs[0] is Windows drive, ':' in its name has been escaped by re.escape so we need to remove backslash
        return _check_single_dir(dirs[0][0]+':\\', dirs[1:])
    else:
        return _check_single_dir('.', dirs)
