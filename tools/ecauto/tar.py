import os
import copy
from tarfile import TarFile, TarInfo, ExtractError, ReadError, open as opentar

from ecauto.utils import copyfileobj as copyfileobj_ext
from ecauto.printing import update_print

def copyfileobj(src, dst, length=None, exception=OSError, callback=None):
    """Copy length bytes from fileobj src to fileobj dst.
       If length is None, copy the entire content.
    """
    if length == 0:
        return
    if length is None:
        copyfileobj_ext(src, dst, callback)
        return

    total = 0
    BUFSIZE = 16 * 1024
    blocks, remainder = divmod(length, BUFSIZE)
    for b in range(blocks):
        buf = src.read(BUFSIZE)
        if len(buf) < BUFSIZE:
            raise exception("unexpected end of data")
        dst.write(buf)
        total += BUFSIZE
        if callback != None:
            callback(total)

    if remainder != 0:
        buf = src.read(remainder)
        if len(buf) < remainder:
            raise exception("unexpected end of data")
        dst.write(buf)
        total += remainder
        if callback != None:
            callback(total)
    return

def makefile(self, tarinfo, targetpath, callback):
    """Make a file called targetpath.
    """
    source = self.fileobj
    source.seek(tarinfo.offset_data)
    with open(targetpath, "wb") as target:
        if tarinfo.sparse is not None:
            for offset, size in tarinfo.sparse:
                target.seek(offset)
                copyfileobj(source, target, size, ReadError, callback)
        else:
            copyfileobj(source, target, tarinfo.size, ReadError, callback)
        target.seek(tarinfo.size)
        target.truncate()

# ZipFile _write_member function modified to use reporting copyfileobj
def _extract_member(self, tarinfo, targetpath, set_attrs=True, numeric_owner=False, callback=None):
    """Extract the TarInfo object tarinfo to a physical
       file called targetpath.
    """
    # Fetch the TarInfo object for the given name
    # and build the destination pathname, replacing
    # forward slashes to platform specific separators.
    targetpath = targetpath.rstrip("/")
    targetpath = targetpath.replace("/", os.sep)

    # Create all upper directories.
    upperdirs = os.path.dirname(targetpath)
    if upperdirs and not os.path.exists(upperdirs):
        # Create directories that are not part of the archive with
        # default permissions.
        os.makedirs(upperdirs)

    if tarinfo.islnk() or tarinfo.issym():
        self._dbg(1, "%s -> %s" % (tarinfo.name, tarinfo.linkname))
    else:
        self._dbg(1, tarinfo.name)

    if tarinfo.isreg():
        makefile(self, tarinfo, targetpath, callback)
    elif tarinfo.isdir():
        self.makedir(tarinfo, targetpath)
    elif tarinfo.isfifo():
        self.makefifo(tarinfo, targetpath)
    elif tarinfo.ischr() or tarinfo.isblk():
        self.makedev(tarinfo, targetpath)
    elif tarinfo.islnk() or tarinfo.issym():
        self.makelink(tarinfo, targetpath)
    elif tarinfo.type not in SUPPORTED_TYPES:
        self.makeunknown(tarinfo, targetpath)
    else:
        makefile(self, tarinfo, targetpath, callback)

    if set_attrs:
        self.chown(tarinfo, targetpath)
        if not tarinfo.issym():
            self.chmod(tarinfo, targetpath)
            self.utime(tarinfo, targetpath)

def extract(self, member, path="", set_attrs=True, *, numeric_owner=False, callback=None):
    """Extract a member from the archive to the current working directory,
       using its full name. Its file information is extracted as accurately
       as possible. `member' may be a filename or a TarInfo object. You can
       specify a different directory using `path'. File attributes (owner,
       mtime, mode) are set unless `set_attrs' is False. If `numeric_owner`
       is True, only the numbers for user/group names are used and not
       the names.
    """
    self._check("r")

    if isinstance(member, str):
        tarinfo = self.getmember(member)
    else:
        tarinfo = member

    # Prepare the link target for makelink().
    if tarinfo.islnk():
        tarinfo._link_target = os.path.join(path, tarinfo.linkname)

    try:
        _extract_member(self, tarinfo, os.path.join(path, tarinfo.name), set_attrs=set_attrs, numeric_owner=numeric_owner, callback=callback)

    except OSError as e:
        if self.errorlevel > 0:
            raise
        else:
            if e.filename is None:
                self._dbg(1, "tarfile: %s" % e.strerror)
            else:
                self._dbg(1, "tarfile: %s %r" % (e.strerror, e.filename))
    except ExtractError as e:
        if self.errorlevel > 1:
            raise
        else:
            self._dbg(1, "tarfile: %s" % e)

def extractall(self, path=".", members=None, *, numeric_owner=False, callback=None):
    """Extract all members from the archive to the current working
       directory and set owner, modification time and permissions on
       directories afterwards. `path' specifies a different directory
       to extract to. `members' is optional and must be a subset of the
       list returned by getmembers(). If `numeric_owner` is True, only
       the numbers for user/group names are used and not the names.
    """
    directories = []

    if members is None:
        members = self

    copied = 0 #copied from already extracted members
    if callback is not None:
        single_file_callback = (lambda done: callback(done+copied))
    else:
        single_file_callback = None

    for tarinfo in members:
        if tarinfo.isdir():
            # Extract directories with a safe mode.
            directories.append(tarinfo)
            tarinfo = copy.copy(tarinfo)
            tarinfo.mode = 0o700
        # Do not set_attrs directories, as we will do that further down
        extract(self, tarinfo, path, set_attrs=not tarinfo.isdir(),
                     numeric_owner=numeric_owner,
                     callback=single_file_callback)
        copied += tarinfo.size

    # Reverse sort directories.
    directories.sort(key=lambda a: a.name)
    directories.reverse()

    # Set correct owner, mtime and filemode on directories.
    for tarinfo in directories:
        dirpath = os.path.join(path, tarinfo.name)
        try:
            self.chown(tarinfo, dirpath)
            self.utime(tarinfo, dirpath)
            self.chmod(tarinfo, dirpath)
        except ExtractError as e:
            if self.errorlevel > 1:
                raise
            else:
                self._dbg(1, "tarfile: %s" % e)

def unpack(archive_file, dst):
    archive = opentar(archive_file, 'r:gz')
    total = 0
    for info in archive.getmembers():
        total += info.size

    extractall(archive, dst, callback=lambda done: update_print(done, total))
    print()
