import copy
from ecauto.config import Config
from ecauto.printing import getTerminalSize

def readOpts(file):
    ret = None
    with open(file, 'r') as f:
        ret = f.read()
    
    return ret.split()

class TargetTemplate:
    def __init__(self, target_name, target, prep_fun = None, is_default = False, default_steps = [], fixed_flags = []):
        self.name = target_name
        self.target = None
        self._orig_target = target
        self.prep_fun = prep_fun
        self.config = Config()
        self._already_prepared = False
        self.is_default = is_default
        self.config.setDefaultSteps(default_steps)
        self.fixed_flags = []
        for flag in fixed_flags:
            self.fixed_flags += ['--flag', flag]

    def configure(self, args):
        print("Configuring meta-target "+self.getTargetName()+", args: "+repr(args))
        args = self.fixed_flags + args
        self.config.configure(args)

    def prepare(self):
        if len(self.config.steps) == 0:
            return

        print("Preparing meta-target "+self.getTargetName()+", steps defined: "+repr(self.config.steps))
        self.target = copy.deepcopy(self._orig_target)
        if self.prep_fun is not None:
            self.prep_fun(self.target, self.config)

    def getTargetName(self):
        return self.name

class Engine:
    def __init__(self, templates):
        self.templates = {}
        index = 0
        for t in templates:
            self.templates[t.getTargetName()] = t
            self.templates[t.getTargetName()].priority = index
            index += 1

        self.targets = []

    def configure_target(self, target, target_args):
        if target == "all":
            for _,t in self.templates.items():
                t.configure(target_args)
        if target == "default":
            for _,t in self.templates.items():
                if t.is_default:
                    t.configure(target_args)

        elif target not in self.templates:
            print("Unknown target: "+target)
            return

        else:
            self.templates[target].configure(target_args)

    def configure(self, args):
        if len(args) == 0:
            args = ['default:', 'default']

        print("Configuring engine: "+repr(args))
        opt = 0
        target = None
        target_first_opt = 0
        force_target_end = -1
        while opt < len(args):
            if opt == force_target_end:
                if target != None:
                    self.configure_target(target, args[target_first_opt:opt])
                    target = None

            if args[opt][0] == ':':
                if target != None:
                    self.configure_target(target, args[target_first_opt:opt])
                    target = None

                extra_args = readOpts(args[opt][1:])
                if len(extra_args) > 0:
                    force_target_end = opt+len(extra_args)+1
                else:
                    force_target_end = -1

                args = args[0:opt+1] + extra_args + args[opt+1:]
                print("Added args: "+repr(extra_args)+" from file "+args[opt][1:])

            else:
                if args[opt][-1] == ':':
                    if target != None:
                        self.configure_target(target, args[target_first_opt:opt-1])

                    target = args[opt][0:-1]
                    target_first_opt = opt+1

                elif target == None:
                    target = args[opt]
                    self.configure_target(target, ['all'])
                    target = None

            opt += 1

        if target is not None:
            self.configure_target(target, args[target_first_opt:])

    def prepare(self):
        self.targets = []
        for name,template in self.templates.items():
            template.prepare()
            if template.target is not None:
                template.target._ec_priority = template.priority
                self.targets.append(template.target)

        self.targets.sort(key=lambda target: target._ec_priority)

    def run(self):
         width, height = getTerminalSize()
         print("Terminal dim (WxH): " + repr((width, height)))
         print(str(len(self.targets)) + (" targets"  if len(self.targets) != 1 else " target")+ " defined for provided options")

         idx = 1
         for t in self.targets:
             print('Running target: ' + t.name + ' (%i/%i)' % (idx, len(self.targets)))
             t.run()
             idx += 1
    
             if t.required and not t.success:
                 print('   Required target failed: ' + t.name)
                 print("\n----------------\n    Fail\n----------------\n")
                 return False
        
         print("\n----------------\n    Success\n----------------\n")
         return True

