import json
from os import makedirs
from os.path import isdir, exists

class Cache:
    def __init__(self, file):
        if not isdir('tmp/cache'):
            makedirs('tmp/cache')
            
        self.name = 'tmp/cache/'+file
        if not exists(self.name):
            self.f = open(self.name, 'w')
            json.dump({}, self.f)
            self.f.close()
            self.f = None
        
        self.f = open(self.name, 'r+')
        self.cache = json.load(self.f)
        self.f.close()
        
    def flush(self):
        self.f = open(self.name, 'w')
        json.dump(self.cache, self.f)
        self.f.close()
