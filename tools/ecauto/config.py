
from enum import IntEnum
from ecauto.os import chooseByOs
from multiprocessing import cpu_count

class Step(IntEnum):
    get = 0
    configure = 1
    make = 2
    make_tests = 3
    test = 4
    clean = 5

class Config:
    def __init__(self):
        self.cmake_path = None
        self.c_compiler = None
        self.cxx_compiler = None
        self.config = 'Release'
        self.arch = None
        self.target = None

        self.steps = []
        self.default_steps = []
 
        self.flags = []
        self.tool_options = []
        self.cpus = 0

    def setDefaultSteps(self, def_steps):
        self.default_steps = def_steps

    def addFlag(self, flag):
        if '=' not in flag:
            flag += '=1'
        if flag[0:2] != '-D':
            flag = '-D'+flag

        if flag not in self.flags:
            self.flags.append(flag)
        else:
            print("Duplicated flag: "+flag)

        return self

    def addStep(self, step):
        if step not in self.steps:
            self.steps.append(step)

        return self

    def configure(self, args):
        i = 0
        while i < len(args):
            arg = args[i]
            next = None
            if arg[0:2] == '--':
                if i+1 >= len(args):
                    print("Missing argument for option: "+arg)
                    break

                next = args[i+1]

            if arg == '--compiler':
                self.c_compiler = next
                self.cxx_compiler = next
            elif arg == '--flag':
                self.addFlag(next)
            else:
                if next is not None:
                    try:
                        setattr(self, arg[2:], next)
                    except AttributeError:
                        print("Unknown config parameter: "+arg[2:])

                else:
                    if arg == 'all':
                        self.addStep(Step.get).addStep(Step.configure).addStep(Step.make)
                    elif arg == 'default':
                        for step in self.default_steps:
                            self.addStep(step)
                    elif arg == 'rebuild':
                        self.addStep(Step.clean).addStep(self.make)
                    else:
                        try:
                            step = Step[arg]
                            self.addStep(step)
                        except KeyError:
                            print("Unknown build step/type: "+arg)

            if next is not None:
                i += 2
            else:
                i += 1
    
    def getCmakeConfArgs(self):
        args = ['..']
        if self.c_compiler is not None:
            args += ['-DCMAKE_C_COMPILER='+self.c_compiler]
        if self.cxx_compiler is not None:
            args += ['-DCMAKE_CXX_COMPILER='+self.cxx_compiler]
        if self.arch is not None:
            args += ['-A', self.arch]
        
        args += ['-DCMAKE_BUILD_TYPE='+self.config]
        args += self.flags
        return args

    def getCmakeBuildArgs(self):
        if self.cpus == 0:
            self.cpus = cpu_count()

        args = ['--build', '.', '--config', self.config]
        if Step.clean in self.steps and Step.make not in self.steps:
            args += ['--target', 'clean']
        else:
            if Step.clean in self.steps:
                args += ['--clean-first']
            if self.target != None:
                args += ['--target', self.target]

        args += ['--']
        args += chooseByOs({
            'windows': '',
            'linux': ['-j'+str(self.cpus + 1)]
        })
        args += self.tool_options
        return args

