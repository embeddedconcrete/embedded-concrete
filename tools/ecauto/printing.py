import sys

def getTerminalSize():
    import os
    env = os.environ
    def ioctl_GWINSZ(fd):
        try:
            import fcntl, termios, struct, os
            cr = struct.unpack('hh', fcntl.ioctl(fd, termios.TIOCGWINSZ,
        '1234'))
        except:
            return
        return cr
    cr = ioctl_GWINSZ(0) or ioctl_GWINSZ(1) or ioctl_GWINSZ(2)
    if not cr:
        try:
            fd = os.open(os.ctermid(), os.O_RDONLY)
            cr = ioctl_GWINSZ(fd)
            os.close(fd)
        except:
            pass
    if not cr:
        cr = (env.get('LINES', 25), env.get('COLUMNS', 80))

        ### Use get(key[, default]) instead of a try/catch
        #try:
        #    cr = (env['LINES'], env['COLUMNS'])
        #except:
        #    cr = (25, 80)
    return int(cr[1]), int(cr[0])

def print_short(bytes, pad = True):
    unit = {
        0: ' b',
        1: ' kb',
        2: ' Mb',
        3: ' Gb',
        4: ' Tb'
    }
    
    idx = 0
    while 1000**(idx+1) < bytes:
        idx += 1

    if idx not in unit:
        suffix = " ?b"
    else:
        suffix = unit[idx]
        
    
    full = int(bytes/1000**idx)    
    if idx > 0:
        rem = int((bytes-(full*1000**idx))/1000**(idx-1))
    else:
        rem = 0
        
    if pad:
        if (full < 10):
            full = "  "+str(full)
        elif (full < 100):
            full = " "+str(full)
        else:
            full = str(full)
    else:
        full = str(full)
        
    if (rem < 10):
        rem = "00"+str(rem)
    elif (rem < 100):
        rem = "0"+str(rem)
    else:
        rem = str(rem)
        
    return "%s.%s%s" % (full, rem, suffix)

def update_print(done, total):
    width, height = getTerminalSize()
    
    txt_len = width-(3+2+22)
    status = ''
    if total is 0:
        status = '    Streaming...'
    else:
        percts = float(done*100/total)
        percts_per_char = float(100/txt_len)
        chars = float(percts/percts_per_char)+1
        status = '='*(int(chars)-1)+'>'
    
    status = status + ' '*(txt_len-len(status))
    if total != 0:
        sys.stdout.write("\r   [%s] %s/%s" % (status, print_short(done), print_short(total, False)) )
    else:
        sys.stdout.write("\r   [%s] %s" % (status, print_short(done)) )
    sys.stdout.flush()
