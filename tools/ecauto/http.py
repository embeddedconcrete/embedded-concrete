from email.utils import formatdate
from datetime import datetime
from time import mktime
import urllib.request
from urllib.error import HTTPError

from ecauto.utils import copyfileobj
from ecauto.cache import Cache
from ecauto.printing import update_print

http_cache = Cache('http')

def html_today():
    now = datetime.now()
    stamp = mktime(now.timetuple())
    return formatdate(
        timeval = stamp,
        localtime = False,
        usegmt = True )

def download(url, outfile):
    req = urllib.request.Request(url)
    if url in http_cache.cache:
        req.add_header('If-Modified-Since', http_cache.cache[url])
    
    try:
        with urllib.request.urlopen(req) as response:
            total = int(response.info().get('Content-Length', '0').strip())
            copyfileobj(response, outfile, lambda done: update_print(done, total))
            http_cache.cache[url] = html_today() #reponse.info().get('Last-Modified', html_today())
            http_cache.flush()
    except HTTPError as e:
        if e.code != 304:
            raise e
        else:
            print('   [ Unmodified since last download: ' + http_cache.cache[url] + ']')
            return True
    print()
    return False

