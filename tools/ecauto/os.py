import platform

def chooseByOs(values):
    if platform.system().lower() not in values:
        raise Exception('Unk platform.system(): ' + platform.system().lower())
            
    if values[platform.system().lower()] is None:
        raise Exception('Value unspecified for system: ' + platform.system().lower())
        
    return values[platform.system().lower()]
