from os import path, remove, makedirs, rmdir, getcwd, chdir
import shutil

from ecauto.http import download
from ecauto.zip import unpack as unpack_zip
from ecauto.tar import unpack as unpack_tar

global_autoclean = False
global_autoupdate = True
      
class Target:
    def __init__(self, name, url, root_dst, req = True, root_tmp = 'tmp', autoupdate = global_autoupdate, autoclean = global_autoclean):
        self.name = name
        self.url = url
        self.tmpfiles = []
        
        self.wdir = getcwd()
        
        self.tmpfiledir = path.join(self.wdir, path.join(root_tmp, self.name));
        self.tmpfilename = 'tmp{idx!s}'
        if len(root_dst) != 0:
            self.dstdir = path.join(self.wdir, path.join(root_dst, self.name))
        else:
            self.dstdir = self.wdir
        
        self.checktmpdir = False
        self.checkdstdir = False
        
        filename = self.url[self.url.rfind('/')+1:]
        self.unpack_type = filename[filename.find('.')+1:]
        if self.unpack_type.endswith('zip'):
            self.unpack_type = 'zip'
        elif self.unpack_type.endswith('tar.gz'):
            self.unpack_type = 'tar.gz'

        self.autoclean = autoclean
        self.autoupdate = autoupdate
        
        self.exists = path.isdir(self.dstdir)
        self.required = req
        self.success = False
        
        self.confs = []
        self.makes = []
        self.installs = []
        self.tests = []
        
        self.doDownload = True
        
    def run(self):
        oldcwd = getcwd()

        try:
            self.success = False
            
            if len(self.url) != 0 and self.doDownload:
                idx = self.download()
                if idx != -1 and self.success == False:
                    self.unpack(idx, self.autoclean)

            chdir(self.dstdir)
            if not self.configure():
                raise Exception()
                
            if not self.make():
                raise Exception()
                
            if not self.install():
                raise Exception()
                
            if not self.test():
                raise Exception()
            
            self.success = True
            
        except Exception as e:
            print("Error: " + str(e.args))
            pass
        finally:
            chdir(oldcwd)
            if self.autoclean:
                self.clean_tmp()

    def configure(self):
        if len(self.confs) == 0:
            return True
        print('   Configuring: ' + self.dstdir)
        for c in self.confs:
            c.run(self)
            print("      " + type(c).__name__ + ": " + str(c.success))
            if not c.success:
                return False
                
        return True
        
    def make(self):
        if len(self.makes) == 0:
            return True
        print('   Building: ' + self.dstdir)
        for c in self.makes:
            c.run(self)
            print("      " + type(c).__name__ + ": " + str(c.success))
            if not c.success:
                return False
                
        return True
        
    def install(self):
        if len(self.installs) == 0:
            return True
        print('   Installing: ' + self.dstdir)
        for c in self.installs:
            c.run(self)
            print("      " + type(c).__name__ + ": " + str(c.success))
            if not c.success:
                return False
                
        return True
        
    def test(self):
        if len(self.tests) == 0:
            return True
        print('   Testing: ' + self.dstdir)
        for c in self.tests:
            c.run(self)
            print("      " + type(c).__name__ + ": " + str(c.success))
            if not c.success:
                return False
                
        return True
        
    def _unifyStep(self, run, name):
        if not hasattr(run, 'success'):
            print(name + ' step ' + repr(run) + ' does not define success flag! Assuming always successful')
            run.success = True
            
        return run
        
    def suppressDownload(self):
        self.doDownload = False
        return self
        
    def addConfigureStep(self, run):
        self.confs.append(self._unifyStep(run, 'Configure'))
        return self
        
    def addBuildStep(self, run):
        self.makes.append(self._unifyStep(run, 'Build'))
        return self
        
    def addInstallStep(self, run):
        self.installs.append(self._unifyStep(run, 'Install'))
        return self
        
    def addTestStep(self, run):
        self.tests.append(self._unifyStep(run, 'Test'))
        return self
        
    def tmp(self):
        if not self.checktmpdir:
            if not path.isdir(self.tmpfiledir):
                makedirs(self.tmpfiledir)
                
        self.checktmpdir = True
        filename = path.join(self.tmpfiledir, self.tmpfilename.format(idx = len(self.tmpfiles)))
        ret = None
        try:
            ret = open(filename, 'wb')
            ret.tmpidx = len(self.tmpfiles)
            self.tmpfiles.append(filename)
        except IOError as exp:
            print(exp)
        finally:
            return ret
        
    def download(self):
        out_file = self.tmp()
        if out_file is None:
            return -1
        
        print('   Donwloading ' + self.url + ' into ' + out_file.name)
        if download(self.url, out_file):
            out_file.close()
            return -1
            
        out_file.close()
        return out_file.tmpidx
        
    def unpack(self, tmp, clean_dst = True):
        if not self.checkdstdir:
            if not path.isdir(self.dstdir):
                makedirs(self.dstdir)
                
        self.checkdstdir = True
        
        if clean_dst:
            shutil.rmtree(self.dstdir)
            makedirs(self.dstdir)
        
        print('   Unpacking ' + self.tmpfiles[tmp] + ' into ' + self.dstdir)
        if self.unpack_type == 'zip':
            unpack_zip(self.tmpfiles[tmp], self.dstdir)
        elif self.unpack_type == 'tar.gz':
            unpack_tar(self.tmpfiles[tmp], self.dstdir)
        else:
            raise Exception('Unk archive format! ' + self.unpack_type)
            
    def clean_tmp(self):
        for f in self.tmpfiles:
            remove(f)
            
        rmdir(self.tmpfiledir)
        self.tmpfiles = []
        self.checktmpdir = False
        
    def getpath(self, p):
        if p is None:
            return None

        oldp = p
        if p.startswith('///'):
            p = path.join(self.wdir, p[3:])
        elif p.startswith('//'):
            p = path.join(self.dstdir, p[2:])
        
        p = path.normpath(p.replace('{name}', self.name))
        return p
            
class Dependency(Target):
    def __init__(self, name, url, req = True, autoupdate = global_autoupdate, autoclean = global_autoclean):
        super().__init__(name, url, 'dep', req, 'tmp', autoupdate, autoclean)
        
class CMake(Target):
    def __init__(self, url, autoupdate = global_autoupdate, autoclean = global_autoclean):
        super().__init__('cmake', url, 'tools', True, 'tmp', autoupdate, autoclean)

class RootAction(Target):
    def __init__(self, autoupdate = global_autoupdate, autoclean = global_autoclean):
        super().__init__(path.basename(getcwd()), '', '', True, 'tmp', autoupdate, autoclean)
        
class Dummy(Target):
    def __init__(self):
        super().__init__('dummy', '', '')
