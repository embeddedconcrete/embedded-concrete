#pragma once

#include "ecconfig.h"
#include "packet.h"
#include "details/packets.def"

#include <tuple>
#include <vector>

EC_BEGIN

typedef std::tuple<std::string, worker_id_t> dbg_worker_address;
typedef std::tuple<dbg_worker_address, std::list<dbg_worker_address>> dbg_worker_info;

namespace packets {

#define EC_PACKET(name, ...) EC_DEFINE_PACKET(name, __VA_ARGS__)
#include "packets.inc"
#undef EC_PACKET

}

void createEmptyPacketDyn(OpCode code, void* ptr);
void destroyPacketDyn(OpCode code, void* ptr);
void movePacketDyn(OpCode code, void* dst, void* src);

EC_END
