#include "serializer.h"
#include "packets.h"
using namespace ec;

memblock ec::serializePacketDyn(OpCode oc, const void* ptr)
{
    switch (oc) {
#define EC_PACKET(OpCodeName, ...) case OpCode::OpCodeName: return Serializer::serialize(*static_cast<const packets::OpCodeName*>(ptr));
#include "packets.inc"
#undef EC_PACKET

    default:
        assert(false && "Unknown packet type");
        return memblock::empty;
    }
}

memlen_t ec::deserializePacketDyn(OpCode oc, void* ptr, memblock const& mem)
{
    switch (oc) {
#define EC_PACKET(OpCodeName, ...) case OpCode::OpCodeName: return Serializer::deserialize(*static_cast<packets::OpCodeName*>(ptr), mem);
#include "packets.inc"
#undef EC_PACKET

    default:
        assert(false && "Unknown packet type");
        return 0;
    }
}

memblock Serializer::serialize(std::string const & str)
{
    uint32 strsize = static_cast<uint32>(str.size());
    uint32 memsize = static_cast<uint32>(strsize*sizeof(*str.data()) + sizeof(strsize));
    char* buff = new char[memsize];

    memcpy(buff, &strsize, sizeof(strsize));
    memcpy(buff + sizeof(strsize), str.data(), str.size() * sizeof(*str.data()));

    return memblock::manage(buff, memsize);
}

memblock Serializer::serialize(std::wstring const & str)
{
    return Serializer::serialize(toutf8(str));
}

memlen_t Serializer::deserialize(std::string & str, memblock const & data)
{
    uint32 strsize;
    memcpy(&strsize, data.data(), sizeof(strsize));
    str.resize(strsize);
    memcpy(&str[0], data.data() + sizeof(strsize), strsize * sizeof(*str.data()));

    return sizeof(strsize) + strsize * sizeof(*str.data());
}

memlen_t Serializer::deserialize(std::wstring & str, memblock const & data)
{
    std::string tmp;
    auto result = Serializer::deserialize(tmp, data);
    str = fromutf8(tmp);
    return result;
}
