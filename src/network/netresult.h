#pragma once

#include "ecconfig.h"
#include "optional.h"
#include "details/rawnet.h"

EC_BEGIN

template <class T>
struct network_result : public maybe_error<T, int32>
{
    using parent = maybe_error<T, int32>;
public:
    using maybe_error<T, int32>::maybe_error;
    using maybe_error<T, int32>::operator =;

    const char* descr() {
        if (!parent::has_error())
            return "no error";

        return details::get_errorstr(parent::get_error());
    }
};

EC_END
