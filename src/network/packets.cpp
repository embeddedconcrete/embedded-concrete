#include "packets.h"
using namespace ec;

void ec::createEmptyPacketDyn(OpCode oc, void * ptr)
{
    switch (oc) {
#define EC_PACKET(OpCodeName, ...) case OpCode::OpCodeName: ::details::create<packets::OpCodeName>(ptr); break;
#include "packets.inc"
#undef EC_PACKET

    default:
        assert(false && "Unknown packet type");
        return;
    }
}

void ec::destroyPacketDyn(OpCode oc, void * ptr)
{
    switch (oc) {
#define EC_PACKET(OpCodeName, ...) case OpCode::OpCodeName: ::details::destroy(*reinterpret_cast<packets::OpCodeName*>(ptr)); break;
#include "packets.inc"
#undef EC_PACKET

    default:
        assert(false && "Unknown packet type");
        return;
    }
}

void ec::movePacketDyn(OpCode oc, void * dst, void * src)
{
    switch (oc) {
#define EC_PACKET(OpCodeName, ...) case OpCode::OpCodeName: ::details::create<packets::OpCodeName>(dst, std::move(*reinterpret_cast<packets::OpCodeName*>(src))); break;
#include "packets.inc"
#undef EC_PACKET

    default:
        assert(false && "Unknown packet type");
        return;
    }
}
