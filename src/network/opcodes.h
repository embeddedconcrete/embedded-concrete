#pragma once

#include "ecconfig.h"

EC_BEGIN

enum class OpCode : int16
{
#define EC_PACKET(name, ...) name,
#include "packets.inc"
#undef EC_PACKET
};

const char* nameForOpCode(OpCode const& OC);

EC_END
