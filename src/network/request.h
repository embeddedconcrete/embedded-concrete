#pragma once

#include "ecconfig.h"
#include "optional.h"
#include "packet.h"
#include "packets.h"
#include "netresult.h"
#include "socket.h"
#include "details/message.h"
#include <memory>

EC_BEGIN

namespace details {
    template <class... T> struct recv_helper;
}

template <class... T>
class Request : public Packet<T...>
{
    friend class ControlServer;
    friend class Request<memblock>;
    template <class... U> friend struct details::recv_helper;

private:

    using parent = Packet<T...>;
    
    template <class... U>
    Request(ControlServer* server, std::string&& tcppeerId, std::string&& peerAddr, uint32 routingId, bool moredata, U&&... u) : parent{ std::forward<U>(u)... }, srv(server), peerId(std::move(tcppeerId)), peer(std::move(peerAddr)), routing(routingId), hasmore(moredata)
    {}

public:
    using parent::parent;

    template <class U>
    std::enable_if_t<(sizeof...(T) > 1), Request<U>> getAs()
    {
        return Request<U>{ srv, std::move(peerId), std::move(peer), routing, hasmore, parent::template getAs<U>() };
    }

    template <class... U>
    network_result<bool> reply(Packet<U...>&& t, bool block = false, bool more = false) const
    {
        t.getHeader() = parent::getReplyHeader();
        return srv->reply(std::move(t), *this, block, more);
    }

    auto hasMore() const { return hasmore; }

    template <class... U>
    network_result<Request<U...>> more() const
    {
        if (!hasMore())
            return network_result<Request<U...>>::none;

        return srv->template recv<U...>(true); //note: message should always be available since ZeroMQ guarantees message atomicy
    }

    uint32 getRoutingId() const { return routing; }
    std::string const& getPeerAddress() const { return peer; }
    std::string const& getPeerId() const { return peerId; }

private:
    ControlServer* srv = nullptr;
    std::string peerId;
    std::string peer;
    uint32 routing = 0;
    bool hasmore = false;
};

template <>
class Request<memblock>
{
    friend class ControlServer;
    template <class... T> friend struct details::recv_helper;

private:
    Request(ControlServer* server, std::string&& tcppeerId, details::message_t&& msg) : srv(server), peerId(std::move(tcppeerId)), message(std::move(msg))
    {}
    void setMessage(details::message_t const& msg) { message = msg; }

public:
    auto hasMore() const { return message.hasMore(); }

    template <class... U>
    network_result<Request<U...>> more() const
    {
        if (!hasMore())
            return network_result<Request<U...>>::none;

        return srv->template recv<U...>(true); //note: message should always be available since ZeroMQ guarantees message atomicy
    }

    template <class... T>
    Request<T...> deserialize()
    {
        Request<T...> ret{ srv, std::move(peerId), getPeerAddress(), getRoutingId(), hasMore() };
        Serializer::deserialize(static_cast<Packet<T...>&>(ret), message);
        return std::move(ret);
    }

    template <class T>
    T deserializePart()
    {
        T ret;
        offset += Serializer::deserialize(ret, static_cast<memblock>(message).subblock(offset));
        return std::move(ret);
    }

    template <class T>
    T deserializePart(memlen_t newoffset)
    {
        offset = newoffset;
        return deserializePart<T>();
    }

    uint32 getRoutingId() const { return message.getRoutingId(); }
    std::string getPeerAddress() const { return message.peerInfo(); }

    memblock data() const { return static_cast<memblock>(message); }

private:
    ControlServer* srv = nullptr;
    std::string peerId;

    details::message_t message;
    memlen_t offset = 0;
};

EC_END
