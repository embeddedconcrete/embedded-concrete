#include "socket.h"
#include "strutils.h"
#include "request.h"
#include "details/zmq.inc"

using namespace ec;
using namespace std;


ec::details::TcpSocketFacet::TcpSocketFacet(uint32 type) : parent(ZMQ_STREAM)
{
    assert(type == ZMQ_STREAM && "Using TcpSocketFacet with non-tcp socket!");
}

network_result<ec::details::message_t> ec::details::TcpSocketFacet::recv(bool block)
{
    while (true)
    {
        if (lastRecvMsg.has_value())
        {
            lastRecvMsg.get().offset = offset;
            lastRecvMsg.get().length = lastRecvMsg.get().get_msg_length() - offset;
            if (lastRecvMsg.get().operator memblock().size() >= sizeof(memlen_t))
            {
                memlen_t len = 0;
                Serializer::ds_itr itr{ lastRecvMsg.get().operator memblock() };
                itr.read(len);
                lastRecvMsg.get().offset += static_cast<size_t>(itr);
                lastRecvMsg.get().length = len;
                offset += static_cast<size_t>(itr) + len;
                if (lastRecvMsg.get().get_msg_length() < offset)
                {
                    assert(false && "Not whole message in tcp zmq message (needs to get one more part)");
                    sLog.log("Missing ", offset - lastRecvMsg.get().get_msg_length(), " bytes in tcp message");
                    return network_result<details::message_t>::critical{ EINVAL };
                }

                return lastRecvMsg;
            }
            else if (lastRecvMsg.get().operator memblock().size() != 0)
                sLog.log(lastRecvMsg.get().operator memblock().size(), " extra bytes at the end of zmq tcp message");
        }

        auto res = parent::recv(block);
        if (!res)
            return res;

        assert(res.get().hasMore());
        memblock data = res.get();
        lastRecvPeer = std::string(data.data(), data.size());

        lastRecvMsg.~network_result<details::message_t>();
        new (&lastRecvMsg) network_result<details::message_t>(parent::recv(block));
        offset = 0;
        if (!res)
            return res;
    }
}

network_result<bool> ec::details::TcpSocketFacet::send(memblock&& data, std::string const& peerId, uint32 routing, bool block, bool more)
{
    assert(!more && "Tcp socket cannot send multipart messages");
    assert(!peerId.empty() && "Tcp peer not specified!");
    assert(routing == 0 && "routing id is unsed in tcp sockets");

    auto peer = memblock::readonly(peerId.data(), peerId.size());
    auto res = parent::send(std::move(peer), std::string{}, 0, block, true);
    if (!res)
        return res;

    memlen_t len = data.size();
    auto message = storage::join(std::array<memblock, 2>{ memblock::readonly(&len), std::move(data) });
    return parent::send(std::move(message), std::string{}, 0, block, false);
}

ControlServer::ControlServer() : parent(ZMQ_SERVER)
{
}

ControlClient::ControlClient() : parent(ZMQ_CLIENT)
{
    int value = 500;
    zmq_setsockopt(socket.get(), ZMQ_CONNECT_TIMEOUT, &value, sizeof(value));
}

network_result<bool> ControlServer::recv(Request<memblock>& p, bool block)
{
    auto msg = parent::recv(block);
    if (msg.has_error())
        return network_result<bool>::forward_error(std::move(msg));
    if (!msg)
        return network_result<bool>::none;

    p.peerId = std::move(parent::lastId);
    p.setMessage(msg.get());
    return true;
}

network_data::network_data(DataInput* srcsock, uint32 datalen, const char* data, bool moredata) : parent{ parent::none }, src(srcsock), len(datalen), hasmore(moredata)
{
    auto buff = storage::allocate<char>(datalen);
    buff.append(memblock::readonly(data, datalen));

    parent::operator=(memblock(std::move(buff)));
}

network_data network_data::more(bool block)
{
    if (!src)
        throw std::runtime_error("Incomplete network_data! No DataInput associated");

    if (!hasMore())
        return network_data::none;

    return src->recv(block);
}

DataInput::DataInput() : parent(ZMQ_PULL)
{
}

network_data DataInput::recv(bool block)
{
    auto result = Socket::recv(block);
    if (result.has_error())
        return network_data::forward_error(result);
    else if (!result)
        return network_data::none;

    auto mem = static_cast<memblock>(result.get());
    return network_data{ this, static_cast<uint32>(mem.size()), mem.data(), result.get().hasMore() };
}


DataOutput::DataOutput() : parent(ZMQ_PUSH)
{
}

network_result<bool> DataOutput::send(memblock const& data, bool block)
{
    memlen_t chunks = data.size() / EC_NETWORK_DATA_MAX_PACKET_SIZE;
    if (chunks * EC_NETWORK_DATA_MAX_PACKET_SIZE != data.size())
        ++chunks;

    for (memlen_t i = 0; i < chunks; ++i)
    {
        auto result = Socket::send(data.subblock(i*EC_NETWORK_DATA_MAX_PACKET_SIZE, EC_NETWORK_DATA_MAX_PACKET_SIZE), string{}, 0, block, (i + 1 < chunks));
        if (!result)
            return result;
    }

    return true;
}

DataInputUdp::DataInputUdp(std::string const& endpoint) : parent(endpoint)
{
    assert(!endpoint.empty());
}

network_result<memblock> DataInputUdp::recv(bool block)
{
    storage stor;
    parent::recv(stor);
    if (!stor)
        return network_result<memblock>::none;

    return stor;
}

DataOutputUdp::DataOutputUdp() : parent(string{})
{
    itr = outputs.end();
}

bool DataOutputUdp::connect(std::string const & endpoint)
{
    std::string addr;
    ec::port_t port;
    try {
        parent::get_addr_port(endpoint, addr, port);
    }
    catch (std::runtime_error& exp)
    {
        sLog.log("Could not connect to udp endpoint, ", exp.what());
        return false;
    }

    auto item = make_pair(addr, port);
    if (!outputs.empty() && find_if(outputs.begin(), outputs.end(), [&item](decltype(item) const& i1) { return (i1.first == item.first && i1.second == item.second); }) != outputs.end())
        return false;

    outputs.push_back(item);
    if (itr == outputs.end())
        itr = outputs.begin();

    return true;
}


network_result<bool> DataOutputUdp::send(memblock const & data, bool block)
{
    if (itr == outputs.end())
        return false;

    parent::send(itr->first, itr->second, data);

    ++itr;
    if (itr == outputs.end())
        itr = outputs.begin();

    return true;
}


DebugOutput::DebugOutput() : parent(ZMQ_DGRAM)
{
}

network_result<bool> DebugOutput::send(string const& address, packets::DebugInfo const & dbg, bool block)
{
    auto res = Socket::send(Serializer::serialize(address), std::string{}, 0, block, true);
    if (!res)
        return res;

    return Socket::send(Serializer::serialize(dbg), std::string{}, 0, block, false);
}

network_result<bool> DebugOutput::recv(std::string & sub, bool block)
{
    auto data = Socket::recv(block);
    if (data.has_error())
        return network_result<bool>::forward_error{ std::move(data) };
    if (!data)
        return false;

    Serializer::deserialize(sub, data.get());
    assert(data.get().hasMore() && "DGRAM socket - expected more data after ip address");
    data = Socket::recv(block);
    if (data.has_error())
        return network_result<bool>::forward_error{ std::move(data) };
    if (!data)
        return false;

    return true;
}

network_result<Request<memblock>> ec::details::recv_helper<memblock>::recv(ControlServer* s, bool block)
{
    auto msg = s->recvImpl(block);
    if (msg.has_error())
        return typename network_result<Request<memblock>>::forward_error{ std::move(msg) };
    if (!msg)
        return network_result<Request<memblock>>::none;

    return Request<memblock>{ s, std::move(s->lastId), std::move(msg.get()) };
}




DataInputTcp::DataInputTcp(std::string const& endpoint)
	:parent(endpoint, ec::details::TcpMultisock::ROUND_ROBIN )
{
	listen();
}


network_result<memblock> DataInputTcp::recv(bool block)
{
	parent::recv();
	if(is_packet_ready()) {
		return parent::get_next_packet();
	}
	return network_result<memblock>::none;
}


network_result<bool> DataOutputTcp::send(memblock const& data, bool block)
{
	return parent::send(data);
}
