
#ifdef EC_PACKET
 #define EC_CONTROL_PACKET(name, ...) EC_PACKET(name, __VA_ARGS__)
 #define EC_DATA_PACKET(name, ...) EC_PACKET(name, __VA_ARGS__)
#endif


/**
    New packets are created with EC_CONTROL_PACKET or EC_DATA_PACKET
    The first arg should be packet name, followed by pairs [type, fieldname],
    like:
    EC_CONTROL_PACKET(
        Ping,
        int, TTL
    ) 
*/

EC_CONTROL_PACKET(HelpRequest, worker_id_t, newWorkerId, std::string, programName, std::string, inputaddr, ec::port_t, rootport)
EC_CONTROL_PACKET(Acknowledge, std::string, inputaddr)
EC_CONTROL_PACKET(Decline, uint8, reason)
EC_CONTROL_PACKET(Configuration, std::vector<std::string>, outputs, std::list<std::string>, executableArgs, uint32, transferMode, uint32, packetSize)
EC_CONTROL_PACKET(WorkIsDone)
EC_CONTROL_PACKET(EndOfInput)
EC_CONTROL_PACKET(ClusterDismantle)
EC_CONTROL_PACKET(StartCluster, std::string, clusterDescFile)

EC_CONTROL_PACKET(DynamicWorker, std::string, nodeAddress, std::string, progName, std::string, inputaddr, std::list<worker_id_t>, inputWorkers, std::list<worker_id_t>, outputWorkers, std::list<std::string>, executableArgs, uint32, transferMode, uint32, packetSize)
EC_CONTROL_PACKET(NewOutput, std::string, output)
EC_CONTROL_PACKET(ConnectionStatus, bool, established)
EC_CONTROL_PACKET(WorkerDetach, worker_id_t, workerId)
EC_CONTROL_PACKET(TryOpenInputSocket, std::string, inputaddr)

EC_CONTROL_PACKET(DebugInfo, uint32, dataSent, uint64, timestamp)
EC_CONTROL_PACKET(DebugListRootsReq)
EC_CONTROL_PACKET(DebugListRootsRep, std::vector<cluster_id_t>, clusters)
EC_CONTROL_PACKET(DebugListWorkersReq)
EC_CONTROL_PACKET(DebugListWorkersRep, std::vector<dbg_worker_info>, workers)
EC_CONTROL_PACKET(DebugSubscribe)
EC_CONTROL_PACKET(DebugUnsubscribe)

EC_CONTROL_PACKET(Test, std::string, testStr)
