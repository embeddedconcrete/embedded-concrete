#pragma once

#include "ecconfig.h"
#include "memblock.h"
#include "nlqueue.h"

#include <list>
#include <string>
#include <thread>
#include <mutex>
#include <utility>

#ifdef LINUX
#include<sys/socket.h>    //socket
#include<arpa/inet.h> //inet_addr
#elif defined(WINDOWS)
#include <Windows.h>
#endif

using namespace std;

EC_BEGIN;
namespace details
{
    typedef uintptr_t sock_t;

    sock_t create_sock_impl();

    class RawTcpSocket
    {
    public:
        static const ec::port_t ANY_PORT = 0;
        RawTcpSocket(sock_t sock, std::string const& addr, ec::port_t port = 0);
        RawTcpSocket(std::string const& addr, ec::port_t port = 0);
        RawTcpSocket();

        RawTcpSocket(RawTcpSocket const&) = delete;
        RawTcpSocket(RawTcpSocket &&);

        virtual ~RawTcpSocket();

        auto getBoundAddress() const
        {
            return myaddr;
        }

        auto getBoundAddrs() const
        {
            return std::list<std::string>{ myaddr};
        }
        bool send(memblock const& data);
        bool recv();
        memblock get_next_packet();

        bool is_packet_ready() const
        {
            return !recvPackets.empty();
        }

        void bind(ec::port_t port = ANY_PORT);

    protected:
        struct sockaddr_in server;
        storage data;
        NlQueue<memblock> recvPackets;

        sock_t sock = -1;
        std::string myaddr;
        ec::port_t port;


        int packageBytesLeft = -1;
        int aval_bytes() const;
    };

    class RawTcpClientSocket : public RawTcpSocket
    {
    public:
        bool connect(std::string const& endpoint);
    private:
        std::string _endpoint;

    };

    class TcpServer
    {
    public:
        static const ec::port_t ANY_PORT = 0;
        TcpServer();
        TcpServer(std::string const& endpoint);
        bool bind(std::string const& endpoint);
        virtual void listen(std::function<void(RawTcpSocket&&)> onAccept);
        virtual ~TcpServer();
        auto getBoundAddress() const { return _endpoint; }
        auto getBoundAddrs() const { return std::list<std::string>{ _endpoint }; }
    protected:
        sock_t sock;
        std::thread *listener;
    private:
        static const unsigned MAX_WAITING_CONNECTIONS = 5;
        struct sockaddr_in server;
        std::string _endpoint;
    };

    class TcpMultisock
    {
    public:
        static const int ROUND_ROBIN = 0;
        static const int LOAD_BALANCING = 1;

        TcpMultisock(int type = ROUND_ROBIN) : type(type) { }
        virtual void recv();
        virtual bool send(memblock const& data);
        bool is_packet_ready();
        memblock get_next_packet();

    protected:

        void addSocket(RawTcpSocket s)
        {
            sockets.push_back(move(s));
        }
        std::mutex sockets_lock;
        std::list<RawTcpSocket> sockets;
        std::list<RawTcpSocket>::iterator currentRecvSock = sockets.end();
        std::list<RawTcpSocket>::iterator currentSendSock = sockets.end();
        int type = ROUND_ROBIN;
    };

    class TcpMultisockServer : public TcpMultisock, public TcpServer
    {
    public:
        TcpMultisockServer(std::string const& endpoint, int type) : TcpServer(endpoint), TcpMultisock(type) { }
        virtual void listen();
    };

    class TcpMultisockClient : public TcpMultisock
    {
    public:
        bool connect(std::string const& endpoint);
    };
}
EC_END
