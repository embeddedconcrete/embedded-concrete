#ifdef LINUX
#include "raw_udp.h"
#include "strutils.h"

#include <stdexcept>
#include <sys/ioctl.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>

using namespace std;
using namespace ec::details;

RawUdpSocket::RawUdpSocket(std::string const& endpoint) : sock(0)
{
    std::string addr;
    ec::port_t port;
    if (!endpoint.empty())
        get_addr_port(endpoint, addr, port);

    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

    if (sock == -1)
        throw std::runtime_error(makestr("socket failed: ", errno));

    if (endpoint.empty())
        return;

    struct sockaddr_in _addr;
    memset(&_addr, 0, sizeof (_addr));
    _addr.sin_family = AF_INET;
    _addr.sin_port = htons(port);
    _addr.sin_addr.s_addr = inet_addr(addr.c_str());

    auto result = ::bind(sock, (struct sockaddr*) &_addr, sizeof (_addr));
    if (result != 0) {
        close(sock);
        sock = 0;
        throw runtime_error(makestr("Could not bind udp socket at given endpoint ", endpoint, ": ", errno));
    }

    socklen_t addrlen = sizeof (_addr);
    result = getsockname(sock, (struct sockaddr*) &_addr, &addrlen);
    if (result == -1) {
        close(sock);
        sock = 0;
        throw runtime_error(makestr("Could not get bound socket address ", errno));
    }

    char buff[INET_ADDRSTRLEN];
    inet_ntop(_addr.sin_family, &_addr.sin_addr, buff, INET_ADDRSTRLEN);
    myaddr = makestr("udp://", buff, ":", ntohs(_addr.sin_port));
}

RawUdpSocket::~RawUdpSocket()
{
    close(sock);
}

void RawUdpSocket::send(std::string const& to, ec::port_t port, memblock const& data)
{
    struct sockaddr_in addr;
    memset(&addr, 0, sizeof (addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr(to.c_str());

    auto result = sendto(sock, data.data(), static_cast<int> (data.size()), 0, (struct sockaddr*) &addr, sizeof (addr));
    if (result == -1)
        throw std::runtime_error(makestr("sendto failed: ", errno));
    if (result != data.size())
        throw std::runtime_error(makestr("sendto: Could not send all data"));
}

void RawUdpSocket::recv(storage& data, std::string* from, ec::port_t* port)
{
    int aval;
    if (0 != ioctl(sock, FIONREAD, &aval))
        throw runtime_error("Could not peek available bytes from udp socket");

    if (!aval)
        return;

    data.clear();
    data.enlarge<char>(aval);

    struct sockaddr_in addr;
    memset(&addr, 0, sizeof (addr));
    socklen_t len = sizeof (addr);
    auto result = recvfrom(sock, data.end(), static_cast<int> (data.available()), 0, (struct sockaddr*) &addr, &len);
    if (result == -1) {
        throw std::runtime_error(makestr("recvfrom failed: ", errno));
    }

    data.update_size(result);

    if (from || port) {
        char buff[INET_ADDRSTRLEN];
        inet_ntop(addr.sin_family, &addr.sin_addr, buff, INET_ADDRSTRLEN);
        if (from)
            *from = buff;
        if (port)
            *port = ntohs(addr.sin_port);
    }
}

ec::port_t RawUdpSocket::any_port()
{
    return 0;
}


#endif
