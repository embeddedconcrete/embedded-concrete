#include "raw_tcp.h"
#include "strutils.h"

#include <cassert>
#include <stdexcept>

#include <cstdio>
#include <thread>

#include <sys/socket.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <unistd.h>

using namespace std;
using namespace ec::details;

RawTcpSocket::RawTcpSocket()
: myaddr(""), port(0)
{
    sock = create_sock_impl();
}

RawTcpSocket::RawTcpSocket(ec::details::sock_t sock, std::string const& addr, port_t port)
: myaddr(addr), port(port), sock(sock) { }

RawTcpSocket::RawTcpSocket(std::string const& addr, port_t port)
: myaddr(addr), port(port)
{
    sock = create_sock_impl();
}

RawTcpSocket::RawTcpSocket(RawTcpSocket && s)
: data(move(s.data)), server(server), port(s.port), myaddr(s.myaddr)
{
    sock = s.sock;
    s.sock = -1;
    memset(&s.server, 0, sizeof (s.server));
}

sock_t ec::details::create_sock_impl()
{
    sock_t sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        throw runtime_error(makestr("Error while creating socket (", errno, ")"));
    }
    return sock;
}

memblock RawTcpSocket::get_next_packet()
{
    return recvPackets.pop();
}

RawTcpSocket::~RawTcpSocket()
{
    if (sock != -1) {
        close(sock);
        sock = -1;
    }
}

bool RawTcpSocket::send(memblock const& data)
{
    int size = data.size();
    if (::send(sock, (char*) &size, sizeof (size), 0) < 0) {
        return false;
    }
    if (::send(sock, (char*) data.data(), data.size(), 0) < 0) {
        return false;
    }
    return true;
}

bool RawTcpSocket::recv()
{
    if (packageBytesLeft == -1) { // data has been taken by the user, time to read next package
        int avalBytes = aval_bytes();
        if (avalBytes >= sizeof (packageBytesLeft)) {
            auto res = ::recv(sock, (char*) &packageBytesLeft, sizeof (packageBytesLeft), 0);
            if (res == -1) {
                return false;
            }
        } else {
            return true;
        }
    }

    int avalBytes = aval_bytes();
    if (avalBytes > 0) {
        int bytesToRead = min(packageBytesLeft, avalBytes);
        data.enlarge<char>(bytesToRead);
        int readBytes = ::recv(sock, data.end(), bytesToRead, 0);
        if (readBytes < 0) {
            return false;
        }
        data.update_size(readBytes);
        packageBytesLeft -= readBytes;
    }

    if (packageBytesLeft == 0)
    {
        recvPackets.push(move(data));
        packageBytesLeft = -1;
    }

    return true;
}

int RawTcpSocket::aval_bytes() const
{
    int count;
    if (ioctl(sock, FIONREAD, &count) == -1) {
        throw runtime_error(makestr("Error while trying to get aval bytes on socket (", errno, ")"));
    }
    return count;
}

bool RawTcpClientSocket::connect(std::string const& endpoint)
{
    assert(startswith(endpoint, "tcp://"));
    _endpoint = endpoint;
    auto bpoint = _endpoint.find_last_of(':');
    if (!startswith(_endpoint, "tcp://") || bpoint == _endpoint.npos || bpoint <= 6)
        throw runtime_error(makestr("Invalid tcp endpoint", _endpoint));

    myaddr = _endpoint.substr(sizeof ("tcp://") - 1, bpoint - (sizeof ("tcp://") - 1));
    auto portstr = _endpoint.substr(bpoint + 1);
    if (portstr == "*")
        throw runtime_error("Cannot connect to port *!");
    else
        port = parse<ec::port_t>(portstr);

    memset(&server, 0, sizeof (server));
    server.sin_addr.s_addr = inet_addr(myaddr.c_str());
    server.sin_family = AF_INET;
    server.sin_port = htons(port);

    if (::connect(sock, (struct sockaddr*) &server, sizeof (server)) < -1) {
        return false;
    }
    return true;
}

bool TcpServer::bind(std::string const& endpoint)
{
    assert(startswith(endpoint, "tcp://"));
    _endpoint = endpoint;
    auto bpoint = _endpoint.find_last_of(':');
    if (!startswith(_endpoint, "tcp://") || bpoint == _endpoint.npos || bpoint <= 6)
        throw runtime_error(makestr("Invalid tcp endpoint", _endpoint));

    string myaddr = _endpoint.substr(sizeof ("tcp://") - 1, bpoint - (sizeof ("tcp://") - 1));
    string port = _endpoint.substr(bpoint + 1);
    memset(&server, 0, sizeof (server));
    server.sin_family = AF_INET;
    if (myaddr == "*") {
        server.sin_addr.s_addr = INADDR_ANY;
    } else {
        server.sin_addr.s_addr = inet_addr(myaddr.c_str());
    }
    server.sin_port = htons(port == "*" || port == "0" ? ANY_PORT : parse<ec::port_t>(port));

    bool result = ::bind(sock, (struct sockaddr*) &server, sizeof (server)) >= 0;
    if (!result) {
        throw runtime_error(makestr("Cannot bind ", errno));
    }
    if (port == "*" || port == "0") {
        socklen_t addrlen = sizeof (server);
        if (getsockname(sock, (struct sockaddr*) &server, &addrlen) == -1) {
            close(sock);
            sock = -1;
            throw runtime_error(makestr("Cannot get sockaddr after binding (", errno, ")"));
        } else {
            char buff[INET_ADDRSTRLEN];
            inet_ntop(server.sin_family, &server.sin_addr, buff, INET_ADDRSTRLEN);
            _endpoint = makestr("tcp://", buff, ":", ntohs(server.sin_port));
        }
    }
    return result;
}

TcpServer::TcpServer()
{
    sock = create_sock_impl();
}

TcpServer::TcpServer(std::string const& endpoint)
{
    sock = create_sock_impl();
    bind(endpoint);
}

void TcpServer::listen(std::function<void(RawTcpSocket&&) > onAccept)
{
    ::listen(sock, MAX_WAITING_CONNECTIONS);
    listener = new std::thread([onAccept(move(onAccept)), this]() {
        sock_t client_sock = 0;
        struct sockaddr_in client;
        socklen_t socklen = sizeof (client);
        while ((client_sock = ::accept(sock, (struct sockaddr *) &client, (socklen_t*) & socklen)) != -1) {
            onAccept(RawTcpSocket(client_sock, inet_ntoa(client.sin_addr), static_cast<ec::port_t> (client.sin_port)));
        }
    });
}

TcpServer::~TcpServer()
{
    if (sock != -1) {
        shutdown(sock, SHUT_RD); // to wake up all waiting threads (interrupt accept)
    }
    if (listener != nullptr) {
        listener->join();
        delete listener;
        listener = nullptr;
    }
}

void TcpMultisockServer::listen()
{
    TcpServer::listen([this](RawTcpSocket && sock) {
        sockets_lock.lock();
        addSocket(move(sock));
        sockets_lock.unlock();
    });
}

void TcpMultisock::recv()
{
    sockets_lock.lock();
    for (auto&sock : sockets) {
        sock.recv();
    }
    sockets_lock.unlock();
}

bool TcpMultisock::send(memblock const& data)
{
    if (sockets.empty())
        return false;

    if (currentSendSock == sockets.end()) {
        currentSendSock = sockets.begin();
    }
    if (!currentSendSock->send(data)) {
        sockets.erase(currentSendSock);
    }
    currentSendSock++;
    return true;
}

bool TcpMultisock::is_packet_ready()
{
    if (sockets.empty()) return false;

    if (currentRecvSock == sockets.end()) {
        currentRecvSock = sockets.begin();
    }

    switch (type) {
        case ROUND_ROBIN:
            return currentRecvSock->is_packet_ready();

        case LOAD_BALANCING:
            // leave currentSock on position where packet were detected
            sockets_lock.lock();
            for (currentRecvSock = sockets.begin(); currentRecvSock != sockets.end(); currentRecvSock++) {
                if (currentRecvSock->is_packet_ready()) {
                    sockets_lock.unlock();
                    return true;
                }
            }
            sockets_lock.unlock();
            return false;

        default:
            return false;
    }
}

memblock TcpMultisock::get_next_packet()
{
    memblock m = currentRecvSock->get_next_packet();

    sockets_lock.lock();
    if (++currentRecvSock == sockets.end()) {
        currentRecvSock = sockets.begin();
    }
    sockets_lock.unlock();

    return m;
}

bool TcpMultisockClient::connect(std::string const& endpoint)
{
    RawTcpClientSocket s;
    if (s.connect(endpoint)) {
        sockets_lock.lock();
        sockets.push_back(move(s));
        sockets_lock.unlock();
        return true;
    }
    return false;
}
