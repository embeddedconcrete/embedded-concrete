#include "zeromq/zmq.h"
#include "netresult.h"
#include <array>
#include <functional>

namespace {

#define ZMQ_CRIT(value) || zmqerrno == value
    constexpr bool is_zmq_critical_err(int32 zmqerrno) {
        return (false
            ZMQ_CRIT(EMTHREAD)
            ZMQ_CRIT(ETERM)
            ZMQ_CRIT(EMFILE)
            );
    }
#undef ZMQ_CRIT

    template <class T, class... U>
    bool is_one_of(T&& t, U&&... u)
    {
        std::array<bool, sizeof...(U)> eq = { std::equal_to<>{}.operator()(std::forward<T>(t), std::forward<U>(u))... };
        return std::find(eq.begin(), eq.end(), true) != eq.end();
    }

    template <class T, class... U>
    ec::network_result<T> zmqerr(U&&... expected) {
        auto errvalue = zmq_errno();
        if (is_zmq_critical_err(errvalue))
            return typename ec::network_result<T>::critical{ errvalue };
        else if (is_one_of(errvalue, std::forward<U>(expected)...))
            return ec::network_result<T>::none;
        else
            return typename ec::network_result<T>::error{ errvalue };
    }
}
