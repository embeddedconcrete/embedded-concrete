#ifdef WINDOWS
#include "raw_udp.h"
#include "strutils.h"

#include <stdexcept>

#include <WinSock2.h>
#include <WS2tcpip.h>

using namespace std;
using namespace ec::details;

namespace {
    bool WinSockInit = false;
    WSADATA wsaData;

    void initWinSock()
    {
        if (WinSockInit)
            return;

        auto iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (iResult != 0)
            throw runtime_error("Could not initialize WinSock 2.2");

        WinSockInit = true;
    }

}

RawUdpSocket::RawUdpSocket(std::string const& endpoint) : sock(0)
{
    initWinSock();

    std::string addr;
    ec::port_t port;
    if (!endpoint.empty())
        get_addr_port(endpoint, addr, port);

    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sock == INVALID_SOCKET)
        throw std::runtime_error(makestr("socket failed: ", WSAGetLastError()));

    if (endpoint.empty())
        return;

    struct sockaddr_in _addr;
    ZeroMemory(&_addr, sizeof(_addr));
    _addr.sin_family = AF_INET;
    _addr.sin_port = htons(port);
    _addr.sin_addr.s_addr = inet_addr(addr.c_str());
    
    auto result = ::bind(sock, (struct sockaddr*)&_addr, sizeof(_addr));
    if (result != 0)
    {
        closesocket(sock);
        sock = 0;
        auto error = WSAGetLastError();
        throw runtime_error(makestr("Could not bind udp socket at given endpoint ", endpoint, ": ", error));
    }

    int addrlen = sizeof(_addr);
    result = getsockname(sock, (struct sockaddr*)&_addr, &addrlen);
    if (result == SOCKET_ERROR)
    {
        closesocket(sock);
        sock = 0;
        auto error = WSAGetLastError();
        throw runtime_error(makestr("Could not get bound socket address ", error));
    }

    char buff[INET_ADDRSTRLEN];
    inet_ntop(_addr.sin_family, &_addr.sin_addr, buff, INET_ADDRSTRLEN);
    myaddr = makestr("udp://", buff, ":", ntohs(_addr.sin_port));
}

RawUdpSocket::~RawUdpSocket()
{
    closesocket(sock);
}

void RawUdpSocket::send(std::string const& to, ec::port_t port, memblock const& data)
{
    struct sockaddr_in addr;
    ZeroMemory(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.S_un.S_addr = inet_addr(to.c_str());

    auto result = sendto(sock, data.data(), static_cast<int>(data.size()), 0, (struct sockaddr*)&addr, sizeof(addr));
    if (result == SOCKET_ERROR)
        throw std::runtime_error(makestr("sendto failed: ", WSAGetLastError()));
    if (result != data.size())
        throw std::runtime_error(makestr("sendto: Could not send all data"));
}

void RawUdpSocket::recv(storage& data, std::string* from, ec::port_t* port)
{
    u_long aval;
    if (0 != ioctlsocket(sock, FIONREAD, &aval))
        throw runtime_error("Could not peek available bytes from udp socket");

    if (!aval)
        return;

    data.clear();
    data.enlarge<char>(aval);

    struct sockaddr_in addr;
    ZeroMemory(&addr, sizeof(addr));
    int len = sizeof(addr);
    auto result = recvfrom(sock, data.end(), static_cast<int>(data.available()), 0, (struct sockaddr*)&addr, &len);
    if (result == SOCKET_ERROR)
    {
        auto error = WSAGetLastError();
        throw std::runtime_error(makestr("recvfrom failed: ", error));
    }

    data.update_size(result);

    if (from || port)
    {
        char buff[INET_ADDRSTRLEN];
        inet_ntop(addr.sin_family, &addr.sin_addr, buff, INET_ADDRSTRLEN);
        if (from)
            *from = buff;
        if (port)
            *port = ntohs(addr.sin_port);
    }
}

ec::port_t RawUdpSocket::any_port()
{
    return 0;
}


#endif
