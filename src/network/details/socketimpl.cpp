#include "socketimpl.h"
#include "details/zmq.inc"

#include <string>

using namespace ec;
using namespace ec::details;
using namespace std;

memlen_t message_t::get_msg_length() const
{
    return zmq_msg_size(static_cast<zmq_msg_t*>(get()));
}

message_t::operator memblock() const
{
    if (!get())
        return memblock::empty;

    if (offset+length > get_msg_length())
        return memblock::empty;

    return memblock::readonly((char*)zmq_msg_data(static_cast<zmq_msg_t*>(get()))+offset, length);
}

uint32 message_t::getRoutingId() const
{
    if (!get())
        return 0;

    return zmq_msg_routing_id(static_cast<zmq_msg_t*>(get()));
}

bool message_t::hasMore() const
{
    if (!get())
        return false;

    return zmq_msg_more(static_cast<zmq_msg_t*>(get())) != 0;
}

std::string message_t::peerInfo() const
{
    if (!get())
        return "";

    return zmq_msg_gets(static_cast<zmq_msg_t*>(get()), "Peer-Address");
}

Socket::Socket(int type) : socket_type{ type }
{
    auto result = create_socket(socket_type);
    if (result)
        socket = socket_t{ result.get(), destroy_socket };
}

network_result<bool> Socket::connect(std::string const & endpoint)
{
    auto status = zmq_connect(socket.get(), endpoint.c_str());
    if (!status)
    {
        if (socket_type == ZMQ_STREAM)
        {
            char buff[255];
            auto len = sizeof(buff);
            zmq_getsockopt(socket.get(), ZMQ_IDENTITY, buff, &len);
            if (!lastId.empty())
            {
                if (lastId != string(buff, len))
                    throw runtime_error("New identity for tcp the socket!");
            }
            else
                lastId = string(buff, len);
        }
        return true;
    }

    return zmqerr<bool>();
}

network_result<bool> Socket::disconnect(std::string const & endpoint)
{
    auto status = zmq_disconnect(socket.get(), endpoint.c_str());
    if (!status)
    {
        if (socket_type == ZMQ_STREAM)
            lastId = "";

        return true;
    }

    return zmqerr<bool>();
}

network_result<bool> Socket::bind(std::string const & front)
{
    auto status = zmq_bind(socket.get(), front.c_str());
    if (!status)
    {
        char endpoint[100];
        auto size = sizeof(endpoint);
        if (!zmq_getsockopt(socket.get(), ZMQ_LAST_ENDPOINT, &endpoint, &size))
            addr.push_back(endpoint);

        return true;
    }

    return zmqerr<bool>();
}

network_result<bool> Socket::setTimeout(int timeout)
{
    auto status = zmq_setsockopt(socket.get(), ZMQ_RCVTIMEO, &timeout, sizeof(timeout));
    if (!status)
        return true;

    return zmqerr<bool>();
}
