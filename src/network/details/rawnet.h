#pragma once

#include "ecconfig.h"
#include "memlen.h"
#include <cstddef>

EC_BEGIN

template <class T> struct network_result;

namespace details {
    const char* get_errorstr(int32 err);
    network_result<void*> create_socket(int type);
    void destroy_socket(void* socket);
    network_result<void*> recv_msg(void* socket, bool block);
    network_result<bool> send_msg(void* s, const char* ptr, memlen_t len, const char* group, uint32 routing, bool more, bool block);
    void close_msg(void* msg);
}

EC_END
