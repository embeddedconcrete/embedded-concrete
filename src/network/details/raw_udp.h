#pragma once

#include "ecconfig.h"
#include "memblock.h"

#include <list>
#include <string>

EC_BEGIN
namespace details {
    class RawUdpSocket
    {
    public:
        RawUdpSocket(std::string const& endpoint);
        virtual ~RawUdpSocket();

        auto getBoundAddress() const { return myaddr; }
        auto getBoundAddrs() const { return std::list<std::string>{ myaddr }; }

    private:
        uintptr_t sock = 0;
        std::string myaddr;

    protected:
        static void get_addr_port(std::string const& endpoint, std::string& addr, ec::port_t& port);
        static ec::port_t any_port();

        void send(std::string const& to, ec::port_t port, memblock const& data);
        void recv(storage& data, std::string* from = nullptr, ec::port_t* port = nullptr);
    };

}
EC_END
