#include "rawnet.h"
#include "details/zmq.inc"

#include <string>
#include <cstring>

using namespace ec;
using namespace ec::details;

network_result<void*> ec::details::create_socket(int type)
{
    static void* ctx = zmq_ctx_new();
    auto result = zmq_socket(ctx, type);
    if (result)
        return result;

    return zmqerr<void*>();
}

void ec::details::destroy_socket(void * socket)
{
    if (!socket)
        return;

    zmq_close(socket);
}

network_result<void*> ec::details::recv_msg(void* s, bool block)
{
    zmq_msg_t* msg = new zmq_msg_t;
    zmq_msg_init(msg);

    auto status = zmq_msg_recv(msg, s, (block ? 0 : ZMQ_DONTWAIT));
    if (status == -1) {
        delete msg;
        return zmqerr<void*>(block ? 0 : EAGAIN); //if block == false, error EAGAIN is expected as valid result (returns network_result::none)
    }

    return msg;
}

network_result<bool> ec::details::send_msg(void* s, const char * ptr, memlen_t len, const char* group, uint32 routing, bool more, bool block)
{
    zmq_msg_t msg;
    zmq_msg_init_size(&msg, len);
    memcpy(zmq_msg_data(&msg), ptr, len);
    if (routing)
        zmq_msg_set_routing_id(&msg, routing);
    if (group)
        zmq_msg_set_group(&msg, group);

    int flags = 0;
    if (more)
        flags |= ZMQ_SNDMORE;
    if (!block)
        flags |= ZMQ_DONTWAIT;

    auto status = zmq_msg_send(&msg, s, flags);
    if (status == -1) {
        zmq_msg_close(&msg);
        return zmqerr<bool>(block ? 0 : EAGAIN);
    }

    //note: according to ZeroMQ docs: "You do not need to call zmq_msg_close() after a successful zmq_msg_send()"
    return true;
}

void ec::details::close_msg(void * msg)
{
    if (!msg)
        return;

    zmq_msg_t* ptr = static_cast<zmq_msg_t*>(msg);
    zmq_msg_close(ptr);
    delete ptr;
}

const char* ec::details::get_errorstr(int32 err)
{
    return zmq_strerror(err);
}
