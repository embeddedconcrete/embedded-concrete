#pragma once

#include "ecconfig.h"
#include "memblock.h"
#include "details/rawnet.h"
#include <memory>

EC_BEGIN

namespace details {

    struct message_t : public std::shared_ptr<void>
    {
        message_t(void* msg) : std::shared_ptr<void>(msg, close_msg), offset(0), length(get_msg_length()) {}

        message_t(message_t&& msg) : std::shared_ptr<void>(std::move(msg)), offset(msg.offset), length(msg.length) {}
        message_t(message_t const& msg) : std::shared_ptr<void>(msg), offset(msg.offset), length(msg.length) {}

        message_t& operator =(message_t&& msg)
        {
            std::shared_ptr<void>::operator=(std::move(msg));
            offset = msg.offset;
            length = msg.length;
            return *this;
        }

        message_t& operator =(message_t const& msg)
        {
            std::shared_ptr<void>::operator=(msg);
            offset = msg.offset;
            length = msg.length;
            return *this;
        }

        operator memblock() const;

        uint32 getRoutingId() const;
        bool hasMore() const;
        std::string peerInfo() const;

        memlen_t get_msg_length() const;

        memlen_t offset;
        memlen_t length;
    };
}

EC_END
