#include "raw_udp.h"
#include "strutils.h"

#include <cassert>
#include <stdexcept>

using namespace std;
using namespace ec::details;

void RawUdpSocket::get_addr_port(std::string const& endpoint, std::string & addr, ec::port_t & port)
{
    assert(startswith(endpoint, "udp://"));
    auto bpoint = endpoint.find_last_of(':');
    if (!startswith(endpoint, "udp://") || bpoint == endpoint.npos || bpoint <= 6)
        throw runtime_error(makestr("Invalid udp endpoint", endpoint));

    addr = endpoint.substr(sizeof("udp://") - 1, bpoint - (sizeof("udp://") -1 ));
    auto portstr = endpoint.substr(bpoint + 1);
    if (portstr == "*")
        port = any_port();
    else
        port = parse<ec::port_t>(portstr);
}
