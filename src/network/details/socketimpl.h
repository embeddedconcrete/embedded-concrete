#pragma once

#include "ecconfig.h"
#include "memblock.h"
#include "netresult.h"
#include "details/rawnet.h"
#include "details/message.h"

#include <list>
#include <memory>
#include <string>

EC_BEGIN

class ControlServer;

template <class... T> class Request;

namespace details {

    typedef std::shared_ptr<void> socket_t;

    class Socket
    {
    public:
        Socket(int type);

        Socket(Socket&& s) = default;
        Socket(Socket const& s) = default;
        ~Socket() = default;

        network_result<bool> connect(std::string const& endpoint);
        network_result<bool> disconnect(std::string const& endpoint);
        network_result<bool> bind(std::string const& front);

        network_result<bool> setTimeout(int timeout);

        std::list<std::string> const& getBoundAddrs() { return addr; }

    protected:
        socket_t socket;
        int socket_type;

        std::list<std::string> addr;

        std::string lastId;

    protected:
        network_result<message_t> recv(bool block)
        {
            return recv_msg(socket.get(), block);
        }
        network_result<bool> send(memblock mem, std::string const& group, uint32 routing, bool block, bool more)
        {
            return send_msg(socket.get(), mem.data(), mem.size(), group.c_str(), routing, more, block);
        }
    };
}

EC_END
