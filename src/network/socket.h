#pragma once

#include "ecconfig.h"
#include "packets.h"
#include "details/socketimpl.h"
#include "details/raw_udp.h"
#include "mtlog.h"
#include "details/raw_tcp.h"

#include <cassert>
#include <string>

EC_BEGIN

template <class... T> class Request;

namespace details {
    template <class... T>
    struct recv_helper;

    class TcpSocketFacet : public Socket
    {
        using parent = Socket;

    protected:
        TcpSocketFacet(uint32 type);

        network_result<details::message_t> recv(bool block);
        network_result<bool> send(memblock&& data, std::string const& peerId, uint32 routing, bool block, bool more);

        network_result<details::message_t> lastRecvMsg = network_result<details::message_t>::none;
        memlen_t offset = 0;
        std::string lastRecvPeer;

    };
}



class ControlServer : public details::Socket
{
    using parent = details::Socket;
    template <class... T> friend struct details::recv_helper;

    auto recvImpl(bool block) { return parent::recv(block); }

public:
    ControlServer();
    ControlServer(ControlServer const&) = default;
    ControlServer(ControlServer&&) = default;

    template <class... T>
    network_result<Request<T...>> recv(bool block = true)
    {
        return details::recv_helper<T...>::recv(this, block);
    }

    template <class... T>
    network_result<bool> recv(Request<T...>& p, bool block = true)
    {
        auto msg = parent::recv(block);
        if (msg.has_error())
            return network_result<bool>::forward_error(std::move(msg));
        if (!msg)
            return network_result<bool>::none;

        Serializer::deserialize(p.get(), msg.get());
        p.hasmore = msg.get().hasMore();
        p.routing = msg.get().getRoutingId();
        p.peerId = std::move(parent::lastId);
        p.peer = std::move(msg.get().peerInfo());
        return true;
    }

    network_result<bool> recv(Request<memblock>& p, bool block = true);

    template <class... T, class... Arg>
    network_result<bool> reply(Packet<T...>&& t, Request<Arg...> const& req, bool block = true, bool more = false)
    {
        t.getHeader() = req.getReplyHeader();
        return parent::send(Serializer::serialize(t), req.getPeerId(), req.getRoutingId(), block, more);
    }

    template <class... T>
    network_result<bool> send(Packet<T...> const& t, bool block = true, bool more = false)
    {
        return parent::send(Serializer::serialize(t), lastId, 0, block, more);
    }
};




class ControlClient : public details::Socket
{
    using parent = details::Socket;

public:
    ControlClient();
    ControlClient(ControlClient const&) = default;
    ControlClient(ControlClient&&) = default;

    template <class... T>
    network_result<bool> recv(Packet<T...>& t, bool block = true)
    {
        auto msg = parent::recv(block);
        if (msg.has_error())
            return network_result<bool>::forward_error{ std::move(msg) };
        if (!msg)
            return network_result<bool>::none;

        Serializer::deserialize(t, msg.get());
        return true;
    }

    template <class... T>
    network_result<Packet<T...>> recv(bool block = true)
    {
        auto msg = parent::recv(block);
        if (msg.has_error())
            return typename network_result<Packet<T...>>::forward_error{ std::move(msg) };
        if (!msg)
            return network_result<Packet<T...>>::none;

        Packet<T...> t{};
        Serializer::deserialize(t, msg.get());
        return std::move(t);
    }

    template <class... T>
    network_result<bool> send(Packet<T...> const& t, bool block = true, bool more = false)
    {
        return parent::send(Serializer::serialize(t), lastId, 0, block, more);
    }

    template <class... T>
    network_result<bool> signal(std::string const& endpoint, Packet<T...> const& t, bool block = true)
    {
        auto res = connect(endpoint);
        if (!res)
            return res;

        res = send(t, true, false);
        if (!res)
            return res;

        return disconnect(endpoint);
    }
};

class DataInput;



struct network_data : public network_result<memblock>
{
private:
    using parent = network_result<memblock>;
    using parent::parent;
    using parent::operator =;

public:
    network_data(DataInput* srcsock, uint32 datalen, const char* data, bool moredata);

public:
    
    auto lenght() const { return len; }
    bool hasMore() const { return hasmore; }

    network_data more(bool block = true);

private:
    DataInput* src = nullptr;
    uint32 len = 0;
    bool hasmore = false;
};




class DataInput : public details::Socket
{
    using parent = details::Socket;

public:
    DataInput();

    network_data recv(bool block = true);
};




class DataOutput : public details::Socket
{
    using parent = details::Socket;

public:
    DataOutput();

    network_result<bool> send(memblock const& data, bool block = true);
};




class DataInputUdp : public details::RawUdpSocket
{
    using parent = details::RawUdpSocket;

public:
    DataInputUdp(std::string const& endpoint);

    network_result<memblock> recv(bool block = true);
};




class DataOutputUdp : public details::RawUdpSocket
{
    using parent = details::RawUdpSocket;

public:
    DataOutputUdp();

    bool connect(std::string const& endpoint);

    network_result<bool> send(memblock const& data, bool block = true);

private:
    std::list<std::pair<std::string, ec::port_t>> outputs;
    decltype(outputs)::iterator itr;
};


class DataOutputTcp : public details::TcpMultisockClient
{
	using parent = details::TcpMultisockClient;
public:
    network_result<bool> send(memblock const& data, bool block = true);
};

class DataInputTcp : public details::TcpMultisockServer
{
	using parent = details::TcpMultisockServer;
public:
    DataInputTcp(std::string const& endpoint);

    network_result<memblock> recv(bool block = true);
};


class DebugOutput : public details::Socket
{
    using parent = details::Socket;

public:
    DebugOutput();

    network_result<bool> send(std::string const& dst, packets::DebugInfo const& dbg, bool block = true);
    network_result<bool> recv(std::string& sub, bool block = true);
};



namespace details {
    template <class... T>
    struct recv_helper
    {
        static network_result<Request<T...>> recv(ControlServer* s, bool block)
        {
            auto msg = s->recvImpl(block);
            if (msg.has_error())
                return typename network_result<Request<T...>>::forward_error{ std::move(msg) };
            if (!msg)
                return network_result<Request<T...>>::none;

            Request<T...> req{ s, std::move(s->lastId), std::move(msg.get().peerInfo()), msg.get().getRoutingId(), msg.get().hasMore() };
            Serializer::deserialize(req, msg.get());
            return std::move(req);
        }
    };

    template <>
    struct recv_helper<memblock>
    {
        static network_result<Request<memblock>> recv(ControlServer* s, bool block);
    };
}

EC_END
