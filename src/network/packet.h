#pragma once

#ifdef _MSC_VER
 #define EC_PACK_EMPTY_BASES __declspec(empty_bases)
#else
 #define EC_PACK_EMPTY_BASES
#endif

#include "ecconfig.h"
#include "opcodes.h"
#include "meta.h"
#include "serializer.h"
#include "optional.h"
#include "strutils.h"

#include <cstring>
#include <array>

EC_BEGIN

namespace details {
    //helpers
    struct PacketImplMarker {};

    //forward declarations
    template <class Fields>
    class PacketImpl;

    template <size_t I, class T>
    struct PacketFieldStorage;

    template <class... Packets>
    class AnyOfPackets;

    //choosing mechanism
    template <size_t I, class... T>
    struct make_storage { using type = meta::pack<>; };

    template <size_t I, class T, class... Rest>
    struct make_storage<I, T, Rest...> { using type = typename make_storage<I + 1, Rest...>::type::template prepend<PacketFieldStorage<I, T>>; };

    template <class... T>
    using make_storage_t = typename make_storage<0, T...>::type;

    template <bool PacketsAlternative, typename... T> struct choose_packet { using type = AnyOfPackets<T...>; };
    template <class T> struct choose_packet<true, T> { using type = T; };
    template <class... T> struct choose_packet<false, T...> { using type = PacketImpl<make_storage_t<T...>>; };

    template <class... T>
    using choose_packet_t = typename choose_packet<(sizeof...(T) > 0) && meta::all<std::is_base_of<PacketImplMarker, T>::value...>::value, T...>::type;
}

#pragma pack(push, 1)

struct PacketHeader
{
    PacketHeader() = default;
    PacketHeader(cluster_id_t cid, worker_id_t did, worker_id_t sid) : cluster(cid), dst(did), src(sid)
    {}

    cluster_id_t cluster = 0;
    worker_id_t dst = 0;
    worker_id_t src = 0;

    PacketHeader getReplyHeader() const { return PacketHeader{ cluster, src, dst }; }
    uid_t getSrcUid() const { return EC_GET_UID(cluster, src); }
    uid_t getDstUid() const { return EC_GET_UID(cluster, dst); }
};

#pragma pack(pop)

template <class... T>
class Packet : public details::choose_packet_t<T...>::packet_t
{
    using parent = typename details::choose_packet_t<T...>::packet_t;
public:
    using parent::parent;
    using parent::operator =;
};

namespace details {

#pragma pack(push, 1)

    template <size_t I, class T>
    struct PacketFieldStorage
    {
        static_assert(!std::is_base_of<PacketImplMarker, T>::value, "Packet cannot be encapsulated into another!");
        T field;

        PacketFieldStorage() = default;
        template <class U>
        PacketFieldStorage(U&& u) : field(std::forward<U>(u)) {}

        auto& get() { return field; }
        auto const& get() const { return field; }
    };

    struct OpCodeStorage { OpCode opcode; };

#pragma pack(pop)

    template <bool Trivial, class Impl, size_t... I>
    struct PacketSerializer
    {
        auto serialize() const
        {
            std::array<memblock, sizeof...(I) + 2> data = {
                Serializer::serialize(static_cast<const Impl*>(this)->getHeader()),
                Serializer::serialize(static_cast<const Impl*>(this)->getOpCode()),
                Serializer::serialize(static_cast<const Impl*>(this)->template get<I>())... };

            return storage::join(data);
        }

        memlen_t deserialize(memblock const& mem)
        {
            OpCode recvOpCode;
            if (mem.size() < sizeof(recvOpCode) + sizeof(PacketHeader))
                throw std::runtime_error("Received data too short!");

            Serializer::ds_itr itr{ mem };
            itr.read(static_cast<Impl*>(this)->getHeader());
            itr.read(recvOpCode);

            if (recvOpCode != static_cast<Impl*>(this)->getOpCode())
                throw std::runtime_error(makestr("Unexpected opcode received: ", recvOpCode, " (expected: ", static_cast<Impl*>(this)->getOpCode(), ")"));

            itr.read(static_cast<Impl*>(this)->template get<I>()...);
            return itr;
        }
    };

    template <class Impl, size_t... I>
    struct PacketSerializer<true, Impl, I...> {};

    template <size_t... Is, class... Ts>
    class EC_PACK_EMPTY_BASES PacketImpl<meta::pack<PacketFieldStorage<Is, Ts>...>> :
        protected PacketHeader,
        protected OpCodeStorage,
        protected PacketFieldStorage<Is, Ts>...,
        private PacketImplMarker,
        public PacketSerializer<meta::all<std::is_trivial<PacketHeader>::value, std::is_trivial<Ts>::value...>::value, PacketImpl<meta::pack<PacketFieldStorage<Is, Ts>...>>, Is...>
    {
    protected:
        PacketImpl(OpCode code) : OpCodeStorage{ code }, PacketFieldStorage<Is, Ts>()... {}

        template <class... U, std::enable_if_t<meta::all<std::is_constructible<PacketFieldStorage<Is, Ts>, U&&>::value...>::value, bool> Guard = false>
        PacketImpl(OpCode code, U&&... u) : OpCodeStorage{ code }, PacketFieldStorage<Is, Ts>(std::forward<U>(u))... {}

    public:
        using packet_t = PacketImpl<meta::pack<PacketFieldStorage<Is, Ts>...>>;

        auto const& getOpCode() const { return opcode; }

        auto& getHeader() { return static_cast<PacketHeader&>(*this); }
        auto const& getHeader() const { return static_cast<PacketHeader const&>(*this); }

        template <size_t I> decltype(auto) get()       { static_assert(I < sizeof...(Ts), "Data index out of packet's boundary"); return static_cast<PacketFieldStorage<I, meta::ith_t<I, Ts...>>&>(*this).get(); }
        template <size_t I> decltype(auto) get() const { static_assert(I < sizeof...(Ts), "Data index out of packet's boundary"); return static_cast<PacketFieldStorage<I, meta::ith_t<I, Ts...>> const&>(*this).get(); }
    };

    template <>
    class EC_PACK_EMPTY_BASES PacketImpl<meta::pack<>> :
        protected PacketHeader,
        protected OpCodeStorage,
        private PacketImplMarker,
        public PacketSerializer<std::is_trivial<PacketHeader>::value, PacketImpl<meta::pack<>>>
    {
    protected:
        PacketImpl(OpCode code) : OpCodeStorage{ code } {}

    public:
        using packet_t = PacketImpl<meta::pack<>>;
        auto const& getOpCode() const { return opcode; }

        auto& getHeader() { return static_cast<PacketHeader&>(*this); }
        auto const& getHeader() const { return static_cast<PacketHeader const&>(*this); }
    };

    template <class... Packets>
    class AnyOfPackets
    {
    public:
        using packet_t = AnyOfPackets<Packets...>;

        AnyOfPackets() {}

        template <typename U, class Guard = std::enable_if_t<meta::is_one_of<U, Packets...>::value>>
        AnyOfPackets(U&& u)
        {
            ::details::create<U>(std::addressof(storage), std::forward<U>(u));
            valid = true;
            alloc = true;
        }

        AnyOfPackets(AnyOfPackets&& other)
        {
            if (other.alloc)
                movePacketDyn(other.getOpCode().get(), std::addressof(storage), std::addressof(other.storage));

            alloc = other.alloc;
            valid = other.valid;
            other.valid = other.alloc = false;
        }

        ~AnyOfPackets()
        {
            if (alloc)
                destroyPacketDyn(getOpCode().get(), std::addressof(storage));
        }

        template <class T>
        std::enable_if_t<meta::is_one_of<T, Packets...>::value, bool> isType() const { return (!isValid() ? false : getOpCode().get() == T::OC); }

        template <class T>
        std::enable_if_t<std::is_same<T, Packet<>>::value, bool> isType() const { return isValid(); }

        template <typename U>
        U& getAs()
        {
            static_assert(meta::is_one_of<U, Packet<>, Packets...>::value, "Unexpected packet type as cast dst type");
            if (!isType<U>())
                throw std::runtime_error("Casting received packet to incompatible type");

            return reinterpret_cast<U&>(storage);
        }

        template <typename U>
        U const& getAs() const
        {
            static_assert(meta::is_one_of<U, Packet<>, Packets...>::value, "Unexpected packet type as cast dst type");
            if (!isType<U>())
                throw std::runtime_error("Casting received packet to incompatible type");

            return reinterpret_cast<U const&>(storage);
        }

        optional<OpCode> getOpCode() const { if (isValid()) return getAs<Packet<>>().getOpCode(); else return optional<OpCode>::none; }
        optional<PacketHeader> getHeader() const { if (isValid()) return getAs<Packet<>>().getHeader(); else return optional<PacketHeader>::none; }

        bool isValid() const { return valid; }
        operator bool() const { return isValid(); }

        memblock serialize() const
        {
            if (!isValid())
                return memblock::empty;

            return serializePacketDyn(getOpCode(), &storage);
        }

        size_t deserialize(memblock const& mem)
        {
            if (mem.size() < sizeof(Packet<>))
                return 0;

            PacketHeader header;
            OpCode code;
            Serializer::ds_itr itr(mem);
            itr.read(header);
            itr.read(code);

            createEmptyPacketDyn(code, std::addressof(storage));
            alloc = true;
            auto bytes = deserializePacketDyn(code, std::addressof(storage), mem);
            if (bytes)
                valid = true;

            return bytes;
        }

    private:
        static constexpr auto max_size = meta::max<size_t, sizeof(Packets)..., sizeof(Packet<>)>::value;

        bool valid = false;
        bool alloc = false;
        std::aligned_union_t<max_size, Packets..., Packet<>> storage;
    };
}

EC_END
