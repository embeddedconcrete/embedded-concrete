#pragma once

#include "ecconfig.h"
#include "meta.h"
#include "opcodes.h"
#include "memblock.h"

#include <type_traits>
#include <utility>
#include <vector>
#include <stdexcept>
#include <string>
#include <cstring>
#include <tuple>

EC_BEGIN

namespace details {
    template <class T>
    struct is_key_value_container {
    private:
        template <class U>
        static decltype(static_cast<void>(std::declval<typename U::key_type>()), static_cast<void>(std::declval<typename U::mapped_type>()), std::true_type{}) helper(std::nullptr_t);

        template <class U>
        static std::false_type helper(void*);

    public:
        static constexpr bool value = decltype(helper<T>(nullptr))::value;
    };

    template <class T>
    std::enable_if_t<std::is_destructible<T>::value, void> reinit(T& t)
    {
        t.~T();
        new (std::addressof(t)) T;
    }

    template <class T>
    std::enable_if_t<!std::is_destructible<T>::value, void> reinit(T&)
    {}
}

class Serializer
{
private:
    template <class Seq>
    struct tuple_helper;

    template <std::size_t... I>
    struct tuple_helper<std::integer_sequence<std::size_t, I...>> {
        template <class... T>
        static memblock serialize(std::tuple<T...> const& t)
        {
            std::array<memblock, sizeof...(T)> data = {
                Serializer::serialize(std::get<I>(t))... };

            return storage::join(data);
        }

        template <class...T>
        static memlen_t deserialize(std::tuple<T...>& t, memblock const& data)
        {
            ds_itr itr{ data };
            itr.read(std::get<I>(t)...);
            return itr;
        }
    };

public:
    struct output_iterator
    {
    public:
        output_iterator(memblock const& block) : mem(block)
        {}

        output_iterator(output_iterator&& itr) : mem(itr.mem), offset(itr.offset)
        {}

        output_iterator(output_iterator const& itr) : mem(itr.mem), offset(itr.offset)
        {}

        auto rem() const { return mem.subblock(offset); }
        void set(memlen_t pos) { offset = std::min(pos, mem.size()); }

        template <class T>
        void read(T& t)
        {
            offset += Serializer::deserialize(t, rem());
        }

        template <class... T>
        void read(T&... t)
        {
            const bool tab[] = {
                (read(t), true)...
            };
        }

        operator memlen_t() const { return offset; }

    private:
        memblock const& mem;
        memlen_t offset = 0;
    };

    using ds_itr = output_iterator;

    /*
        Serialization
    */

    template <typename T>
    static std::enable_if_t<std::is_trivially_copyable<T>::value, memblock> serialize(T&& exp)
    {
        static_assert(meta::always_false<T>::value, "Since serialization of trivially_copyable types reuses it's storage, there's no possibility to serialize ex-value this way.");
    }

    template <typename T>
    static std::enable_if_t<std::is_trivially_copyable<T>::value, memblock> serialize(T const& t)
    {
        return memblock::readonly(&t);
    }

    template <typename T>
    static std::enable_if_t<!std::is_trivially_copyable<T>::value, memblock> serialize(T const& t)
    {
        return t.serialize();
    }

    static memblock serialize(std::string const& str);
    static memblock serialize(std::wstring const& str);

    template <template <class, class...> class Container, class T, class... Other>
    static auto serialize(Container<T, Other...> const& c)
        -> std::enable_if_t<meta::is_container<Container<T, Other...>>::value && !details::is_key_value_container<Container<T, Other...>>::value, memblock>
    {
        auto size = static_cast<uint32>(c.size());

        std::vector<memblock> blocks;
        blocks.reserve(size + 1);
        blocks.push_back(serialize(size));

        for (auto const& e : c)
            blocks.push_back(serialize(e));

       return storage::join(blocks);
    }

    template <template <class, class, class...> class Container, class Key, class Value, class... Other>
    static auto serialize(Container<Key, Value, Other...> const& c)
        -> std::enable_if_t<meta::is_container<Container<Key, Value, Other...>>::value && details::is_key_value_container<Container<Key, Value, Other...>>::value, memblock>
    {
        auto size = static_cast<uint32>(c.size());

        std::vector<memblock> blocks;
        blocks.reserve(size*2 + 1);
        blocks.push_back(serialize(size));

        for (auto const& e : c) {
            blocks.push_back(serialize(e.first));
            blocks.push_back(serialize(e.second));
        }

        return storage::join(blocks);
    }

    template <class T, size_t N>
    static std::enable_if_t<!std::is_trivially_copyable<std::array<T, N>>::value, memblock> serialize(std::array<T, N> const& a)
    {
        std::vector<memblock> blocks;
        blocks.reserve(N);

        for (auto const& t : a)
            blocks.push_back(serialize(t));

        return storage::join(blocks);
    }

    template <class T, size_t N>
    static std::enable_if_t<!std::is_trivially_copyable<T>::value, memblock> serialize(T(&a)[N])
    {
        std::vector<memblock> blocks;
        blocks.reserve(N);

        for (size_t i=0; i<N; ++i)
            blocks.push_back(serialize(a[i]));

        return storage::join(blocks);
    }

    template <class... T>
    static std::enable_if_t<!std::is_trivially_copyable<std::tuple<T...>>::value, memblock> serialize(std::tuple<T...> const& t)
    {
        return tuple_helper<std::make_integer_sequence<std::size_t, sizeof...(T)>>::serialize(t);
    }

    /*
        Deserialization
    */

    template <typename T>
    static std::enable_if_t<std::is_trivially_copyable<T>::value, memlen_t> deserialize(T& t, memblock const& data)
    {
        if (data.size() < sizeof(T))
            return 0;

        memcpy(&t, data.data(), sizeof(T));
        return sizeof(T);
    }

    template <typename T>
    static std::enable_if_t<!std::is_trivially_copyable<T>::value, memlen_t> deserialize(T& t, memblock const& data)
    {
        return t.deserialize(data);
    }

    static memlen_t deserialize(std::string& str, memblock const& data);
    static memlen_t deserialize(std::wstring& str, memblock const& data);

    template <template <class, class...> class Container, class T, class... Other>
    static auto deserialize(Container<T, Other...>& c, memblock const& data)
        -> std::enable_if_t<meta::is_container<Container<T, Other...>>::value && !details::is_key_value_container<Container<T, Other...>>::value, memlen_t>
    {
        ds_itr itr{ data };
        uint32 size;
        itr.read(size);

        T element;
        while (size--) {
            itr.read(element);
            c.insert(c.end(), std::move(element));
            details::reinit(element);
        }

        return itr;
    }

    template <template <class, class, class...> class Container, class Key, class Value, class... Other>
    static auto deserialize(Container<Key, Value, Other...>& c, memblock const& data)
        -> std::enable_if_t<meta::is_container<Container<Key, Value, Other...>>::value && details::is_key_value_container<Container<Key, Value, Other...>>::value, memlen_t>
    {
        ds_itr itr{ data };
        uint32 size;
        itr.read(size);

        Key k;
        Value v;
        while (size--) {
            itr.read(k, v);
            c.insert(std::make_pair(std::move(k), std::move(v)));
            details::reinit(k);
            details::reinit(v);
        }

        return itr;
    }

    template <class T, size_t N>
    static std::enable_if_t<!std::is_trivially_copyable<std::array<T, N>>::value, memlen_t> deserialize(std::array<T, N>& a, memblock const& data)
    {
        ds_itr itr{ data };
        for (auto& t : a)
            itr.read(t);

        return itr;
    }

    template <class T, size_t N>
    static std::enable_if_t<!std::is_trivially_copyable<T>::value, memlen_t> deserialize(T (&a)[N], memblock const& data)
    {
        ds_itr itr{ data };
        for (size_t i=0; i<N; ++i)
            itr.read(a[i]);

        return itr;
    }

    template <class... T>
    static std::enable_if_t<!std::is_trivially_copyable<std::tuple<T...>>::value, memlen_t> deserialize(std::tuple<T...>& t, memblock const& data)
    {
        return tuple_helper<std::make_integer_sequence<std::size_t, sizeof...(T)>>::deserialize(t, data);
    }
};

memblock serializePacketDyn(OpCode oc, const void* ptr);
memlen_t deserializePacketDyn(OpCode oc, void* ptr, memblock const& mem);

EC_END
