#include "opcodes.h"
using namespace ec;

const char * ec::nameForOpCode(OpCode const & OC)
{
    switch (OC) {
#define EC_PACKET(name,...) case ec::OpCode::name: return #name;
#include "packets.inc"
#undef EC_PACKET
    default:
        return "(unk packet)";
    }
}
