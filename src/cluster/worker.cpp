#include "worker.h"
using namespace ec;

Worker::Worker(Cluster * mycluster, worker_id_t myid, Node const& mycontrolnode, Node const& mydatanode,  Description const& desc) 
	: cluster(mycluster), id(myid), datanode(mydatanode), controlnode(mycontrolnode), mode(desc.mode), packetSize(desc.packetSize)
{
    if (desc.args.size() > 0)
    {
        auto args = split(desc.args, ' ');
        // remove empty splits
        for (auto iter = args.begin(); iter != args.end(); iter++)
            if (iter->size() == 0)
                args.erase(iter);
        runargs = args;
    }
}

packets::Configuration & Worker::buildCfgPacket(packets::Configuration & packet) const
{
    packet.outputs().clear();
    for (auto const& out : outputs)
        packet.outputs().push_back(out->getDataNode().getTcpAddress());

    packet.executableArgs() = runargs;
    packet.transferMode() = static_cast<uint32>(mode);
    packet.packetSize() = packetSize;
    return packet;
}
