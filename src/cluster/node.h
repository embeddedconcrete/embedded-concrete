#pragma once

#include "ecconfig.h"

#include <string>

EC_BEGIN

class Node
{
public:
    Node(std::string const& myaddr);

    std::string getTcpAddress() const { return tcpAddress; }
    std::string getUdpAddress() const { return "udp://" + tcpAddress.substr(6); }

    bool operator <(Node const& right) const
    {
        return (tcpAddress < right.tcpAddress);
    }

    bool operator == (Node const& right) const
    {
        return (tcpAddress == right.tcpAddress);
    }

private:
    std::string tcpAddress;
};

EC_END
