#pragma once

#include "ecconfig.h"
#include "node.h"
#include "worker.h"
#include "context.h"

#include <map>
#include <set>
#include <atomic>

EC_BEGIN

class Cluster
{
public:
    struct Description
    {
        std::list<Worker::Description*> workers;

        struct ParseOptions
        {
            bool allow_dangling_workers = false;
            bool suppress_output = false;
            bool suppress_errors = false;
            bool suppress_warnings = false;
        };

        static bool parse(std::istream& in, Description& desc, ParseOptions const& opts, std::string const& input_name = std::string{});
        static bool parse(std::istream& in, Description& desc, std::string const& input_name = std::string{})
        {
            return parse(in, desc, ParseOptions{}, input_name);
        }
    };

    typedef std::set<Node> PeersList;
    typedef std::map<worker_id_t, Worker*> WorkersMap;

    enum cluster_state
    {
        allocated,
        acquired,
        dismantled
    };

public:
    Cluster(port_t inputp);

    bool acquire(Description const& desc, PeersList availablePeers);
    void dismantle();

    auto& getContext() { return ctx; }
    auto& getContext() const { return ctx; }

    bool requestHelp(Node const& peer, const Worker::Description* workerinfo, worker_id_t worker_id, std::string& workeraddr);
    void configureWorkers();
    void reconfigureWorker(Worker* worker, packets::Configuration& cfg);

    void sendDismantle(Worker* worker);
    bool allDone() const;
    void cleanup();

    Worker* getWorker(worker_id_t worker) const;
    
    auto getId() const { return clusterId; }
    auto const& getWorkers() const { return workers; }

    auto getState() const { return state; }

    auto getNextWorkerId() { return nextWorkerId++; }

private:
    cluster_id_t clusterId;
    cluster_state state = allocated;

    WorkersMap workers;

    Context ctx;

    static std::atomic<cluster_id_t> nextClusterId;
    std::atomic<worker_id_t> nextWorkerId;

    port_t inputPort;
};

EC_END
