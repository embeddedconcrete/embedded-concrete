#pragma once

#include "ecconfig.h"
#include "process_wrapper.h"
#include "packets.h"
#include "node.h"

#include <string>

EC_BEGIN

class Cluster;

class Worker
{
public:
    struct Description
    {
        Node* node = nullptr;
        std::string app;
        std::string args;
        std::vector<Description*> outputs;
        process_wrapper::output_mode mode = process_wrapper::one_shot;
        uint32 packetSize = 0;
        bool isDataSource = false;
    };

public:
    Worker(Cluster* mycluster, worker_id_t myid, Node const& mycontrolnode, Node const& mydatanode, Description const& desc);

    packets::Configuration& buildCfgPacket(packets::Configuration& packet) const;

    auto getId() const { return id; }
    auto getCluster() const { return cluster; }
    auto getControlNode() const { return controlnode; }
    auto getDataNode() const { return datanode; }

    void addOutput(Worker* worker) { outputs.push_back(worker); }
    auto const& getOutputs() const { return outputs; }

    void addInput(Worker* worker) { inputs.push_back(worker); }
    auto const& getInputs() const { return inputs; }

    void setRunargs(StringList const& args) { runargs = args; }
    auto const& getRunargs() const { return runargs; }

    void setFetchMode(process_wrapper::output_mode fmode) { mode = fmode; }
    auto getFetchMode() const { return mode; }

    void setPacketSize(uint32 newsize) { packetSize = newsize; }
    auto getPacketSize() const { return packetSize; }

    auto hasDone() const { return done || dismantled; }
    void markAsDone() { done = true; }

    auto isDismantled() const { return dismantled; }
    void markDismantled() { dismantled = true; }

private:
    Cluster* cluster;
    worker_id_t id;
    Node datanode;
    Node controlnode;

    StringList runargs;
    process_wrapper::output_mode mode = process_wrapper::one_shot;
    uint32 packetSize;
    std::list<Worker*> outputs;
    std::list<Worker*> inputs;

    bool done = false;
    bool dismantled = false;
};

EC_END
