#pragma once

#include "ecconfig.h"
#include "worker.h"
#include "node.h"
#include "packet.h"
#include "socket.h"

EC_BEGIN

class Context
{
public:
    Context(cluster_id_t mycluster, worker_id_t myworker = 0);

    auto clusterId() const { return cluster; }
    auto workerId() const { return worker; }

    /*
        send to peer (dst worker_id = 0)
    */
    template <class... T>
    auto send(Packet<T...>& packet, Node const& peer, bool block = true, bool more = false)
    {
        auto res = reconnect(peer.getTcpAddress());
        if (!res)
            return res;

        packet.getHeader().cluster = cluster;
        packet.getHeader().dst = 0;
        packet.getHeader().src = worker;
        return socket.send(packet, block, more);
    }

    /*
        send to worker
    */
    template <class... T>
    auto send(Packet<T...>& packet, Worker const& dstworker, bool block = true, bool more = false)
    {
        auto res = reconnect(dstworker.getControlNode().getTcpAddress());
        if (!res)
            return res;

        packet.getHeader().cluster = cluster;
        packet.getHeader().dst = dstworker.getId();
        packet.getHeader().src = worker;
        return socket.send(packet, block, more);
    }
	/*
        send to worker, overload for const packets
    */
    template <class... T>
    auto send(Packet<T...>&& packet, Worker const& dstworker, bool block = true, bool more = false)
    {
        auto res = reconnect(dstworker.getControlNode().getTcpAddress());
        if (!res)
            return res;

        packet.getHeader().cluster = cluster;
        packet.getHeader().dst = dstworker.getId();
        packet.getHeader().src = worker;
        return socket.send(packet, block, more);
    }

    /*
        recv from last peer
    */
    template <class... T>
    auto recv(bool block = true)
    {
        return socket.recv<T...>(block);
    }

private:
    cluster_id_t cluster;
    worker_id_t worker;

    ControlClient socket;
    std::string lastPeer;

    network_result<bool> reconnect(std::string&& newpeer)
    {
        if (newpeer.empty())
            return false;

        if (newpeer == lastPeer)
            return true;

        if (!lastPeer.empty())
        {
            auto res = socket.disconnect(lastPeer);
            if (!res)
                return res;
        }

        lastPeer = std::move(newpeer);
        return socket.connect(lastPeer);
    }
};

EC_END
