#include "cluster.h"
#include "socket.h"
#include "packets.h"

#include "mtlog.h"
//#include <atomic>

using namespace std;
using namespace ec;

atomic<cluster_id_t> ec::Cluster::nextClusterId = {1};

Cluster::Cluster(port_t inputp) : clusterId{ nextClusterId++ }, workers(), ctx{ clusterId }, nextWorkerId{ 1 }, inputPort{ inputp }
{
}

bool Cluster::acquire(Description const & desc, PeersList availablePeers)
{
    if (state == acquired)
        return false;

    state = acquired;

    std::map<Worker::Description*, worker_id_t> applied_descs;

    worker_id_t worker_id = getNextWorkerId();

	string workeraddr;
    auto peers_itr = availablePeers.begin();
    auto peers_end = availablePeers.begin();

    for (int step = 0; step < 2; ++step)
    {
        sLog.log("Staring step ", step + 1, " of workers finding");
        for (auto const& wd : desc.workers)
        {
            bool worker_allocated = false;
            if (wd->node != nullptr && step == 0)
            {
                sLog.log("Finding peer for worker: ", worker_id, " (requested application: ", wd->app);

                sLog.log("Worker is marked as fixed at node: ", wd->node->getTcpAddress());
                if (!requestHelp(Node{ wd->node->getTcpAddress() }, wd, worker_id, workeraddr))
                    sLog.log("Could not get help from fixed peer");
                else
                {
                    availablePeers.insert(Node{ wd->node->getTcpAddress() });
                    worker_allocated = true;
                }
            }
            else if (wd->node == nullptr && step == 1)
            {
                sLog.log("Finding peer for worker: ", worker_id, " (requested application: ", wd->app);

                while (peers_itr != availablePeers.end())
                {
                    if (requestHelp(*peers_itr, wd, worker_id, workeraddr))
                    {
                        peers_end = peers_itr;
                        worker_allocated = true;
                        break;
                    }

                    ++peers_itr;
                    if (peers_itr == availablePeers.end())
                        peers_itr = availablePeers.begin();

                    if (peers_itr == peers_end)
                        peers_itr = availablePeers.end();
                }
            }
            else
                continue;

            if (!worker_allocated)
            {
                sLog.log("At least one worker could not be allocated - abandoning cluster organisation");
                dismantle();
                break;
            }
            else
            {
                applied_descs.insert(make_pair(wd, worker_id));
                worker_id = getNextWorkerId();
            }
        }

        if (state != acquired)
            break;
    }

    if (state == acquired)
    {
        for (auto const& app_desc : applied_descs)
        {
            auto worker = workers.at(app_desc.second);
            auto desc = app_desc.first;
            
            for (auto const& dstdesc : desc->outputs)
            {
                auto dst = workers.at(applied_descs.at(dstdesc));
                worker->addOutput(dst);
                dst->addInput(worker);
            }
        }
    }

    return (state == acquired);
}

void Cluster::configureWorkers()
{
    packets::Configuration cfg;
    for (auto const& worker : workers)
        reconfigureWorker(worker.second, worker.second->buildCfgPacket(cfg));
}

void Cluster::reconfigureWorker(Worker* worker, packets::Configuration& cfg)
{
    if (!worker)
        return;

    sLog.log("Cluster ", clusterId, ": sending Configuration packet to worker ", worker->getId(), " (node: ", worker->getControlNode().getTcpAddress(), ")");
    auto result = ctx.send(cfg, *worker);
    if (!result)
        sLog.log("Cluster ", clusterId, ": reconfiguration of worker failed", result.get_error(), " (", result.descr(), ")");
}

Worker* Cluster::getWorker(worker_id_t worker) const
{
    auto itr = workers.find(worker);
    return (itr == workers.end() ? nullptr : itr->second);
}

bool Cluster::allDone() const
{
    for (auto worker : workers)
        if (!worker.second->isDismantled())
            return false;

    return true;
}

void Cluster::cleanup()
{
    if (!allDone())
        return sLog.log("Cluster ", clusterId, ": requested to cleanup but some nodes are still running, request ignored");

    sLog.log("Cluster ", clusterId, ": cleaning up after all nodes has been dismantled");
    for (auto const& worker : workers)
        delete worker.second;

    workers.clear();
    nextWorkerId = 1;

    state = dismantled;
}

bool Cluster::requestHelp(Node const& peer, const Worker::Description * workerinfo, worker_id_t worker_id, std::string& workeraddr)
{
    if (state != acquired)
        return (void)sLog.log("Trying to send help requuest from either not-initialized or already dismantled cluster"), false;

    sLog.log("Requesting help from '", peer.getTcpAddress(), "', requested app name: ", workerinfo->app);

    string addr;
    if (!workerinfo->isDataSource) //for data source send empty input addr so input socket wont be allocated
        addr = peer.getTcpAddress().substr(0, peer.getTcpAddress().find_last_of(':')) + ":0";

    packets::HelpRequest hreq{ worker_id, workerinfo->app, move(addr), inputPort };

    if (!ctx.send(hreq, peer))
        return (void)sLog.log("Could not send HelpRequest to peer '", peer.getTcpAddress(), "'"), false;

    auto response = ctx.recv<packets::Acknowledge, packets::Decline>();
    if (!response)
        return (void)sLog.log("Could not get HelpRequest response from peer '", peer.getTcpAddress(), "'"), false;

    if (response.get().getOpCode().get() == OpCode::Acknowledge) {
		workeraddr = move(response.get().getAs<packets::Acknowledge>().inputaddr());

        sLog.log("Worker ", worker_id, " has been allocated at node: ", peer.getTcpAddress());
        Worker* worker = new Worker(this, worker_id, peer, workeraddr, *workerinfo);
        workers.insert(make_pair(worker_id, worker));

        return true;
	}

    sLog.log("Peer '", peer.getTcpAddress(), "' rejected due to reason: ", static_cast<int>(response.get().getAs<packets::Decline>().reason()));
    return false;
}

void Cluster::sendDismantle(Worker* worker)
{
    if (!worker)
        return;

    if (state != acquired)
        return sLog.log("Cluster ", clusterId, ": requested to send dismantle message to worker ", worker->getId(), " but cluster is not in acquired state");
    else if (worker->isDismantled())
        return sLog.log("Cluster ", clusterId, ": requested to send dismantle message to already dismantled worker ", worker->getId());

    sLog.log("[Cluster ", clusterId, "] Sending dismantle message to worker: ", worker->getId());
    ctx.send(packets::ClusterDismantle{}, *worker);
    worker->markDismantled();
}

void Cluster::dismantle()
{
    if (state != acquired)
        return;

    for (auto const& worker : workers)
        if (!worker.second->isDismantled())
            sendDismantle(worker.second);

    cleanup();
}
