#include "cluster.h"
#include "mtlog.h"

#include <regex>
#include <cctype>
#include <ctime>
using namespace std;
using namespace ec;

namespace
{
    enum class token_type
    {
        kw_config,
        kw_node,
        kw_worker,

        identifier,
        list_start,
        list_end,
        quantity,
        arrow,
        worker_flag,
        worker_node,
        braket_text,

        eof
    };

    static string ttype2str(token_type ttype)
    {
        switch (ttype)
        {
        case token_type::kw_node: return "kw_node";
        case token_type::kw_config: return "kw_config";
        case token_type::kw_worker: return "kw_worker";
        case token_type::identifier: return "identifier";
        case token_type::list_start: return "list_start";
        case token_type::list_end: return "list_end";
        case token_type::quantity: return "quantity";
        case token_type::arrow: return "arrow";
        case token_type::worker_flag: return "worker_flag";
        case token_type::worker_node: return "worker_node";
        case token_type::braket_text: return "braket_text";
        case token_type::eof: return "eof";
        default:
            return "<unk>";
        }
    }

    struct token_t
    {
        token_t() = default;
        token_t(token_type t, string&& s, uint32 l, uint32 c) : type(t), content(move(s)), line(l), column(c)
        {}

        token_type type = token_type::eof;
        string content = "";
        uint32 line = 0;
        uint32 column = 0;
    };

    class Diagnostics
    {
    public:
        Diagnostics(string const& inputname) : input{ inputname }
        {
        }

        ~Diagnostics()
        {
        }

        void show_info(clock_t msecs)
        {
            sLog.log(input, ": ", errors, " errors and ", warnings, " warnings");
            sLog.log("Parsing time: ", msecs / (CLOCKS_PER_SEC), " seconds");
        }

        void error(token_t const& t, string const& msg)
        {
            ++errors;
            if (en_e)
                sLog.log(input, ':', t.line, ':', t.column, " error: ", msg);
        }

        void warning(token_t const& t, string const& msg)
        {
            ++warnings;
            if (en_w)
                sLog.log(input, ':', t.line, ':', t.column, " warning: ", msg);
        }

        bool ok() const { return errors == 0; }

        void suppress(bool sup_errors, bool sup_warnings) { en_e = !sup_errors; en_w = !sup_warnings; }

    private:
        string input;

        uint32 errors = 0;
        uint32 warnings = 0;

        bool en_e = true;
        bool en_w = true;
    };

    class Tokenizer
    {
    public:
        Tokenizer(istream& ins, Diagnostics& d) : in(ins), diag(d)
        {}

        token_t get();

    private:
        string buff;
        istream& in;

        uint32 line = 1;
        uint32 column = 0;

        uint32 token_start_line = 0;
        uint32 token_start_column = 0;

        bool eof = false;

        Diagnostics& diag;

        char getchar_raw()
        {
            if (eof)
                return 0;

            char c = in.get();
            if (!in)
            {
                eof = true;
                return 0;
            }

            if (c == '\n')
            {
                ++line;
                column = 0;
            }
            else
                ++column;

            return c;
        }

        /*
            if token has stared (at least one character in 'buff') returns true and appends character to buff if either next character is non-whitespace or 'token_accepts whitespaces' is set to true, otherwise returns false and doesn't append
            if token hasn't started skips as many whitespaces as possible until non-whitespace is found, returns true and appends character to buff
            in both cases last character is returned via 'out'
            returns false and sets eof to true if in either case extraction from stream failed (in.good() == false), no character is appended nor returned via 'out' in that case
        */
        bool getchar(char& out, bool token_accepts_whitespaces = false) {
            char c;
            while (true) {
                c = getchar_raw();
                if (eof)
                    return false;

                if (isspace(c) && !buff.empty() && !token_accepts_whitespaces)
                {
                    c = out;
                    return false;
                }
                else if (!isspace(c) || token_accepts_whitespaces)
                    break;

            }

            out = c;
            if (buff.empty())
            {
                token_start_column = column;
                token_start_line = line;
            }

            buff += c;
            return true;
        }

        void skip()
        {
            while (true)
            {
                char c = getchar_raw();
                if (eof)
                    break;

                if (isspace(c))
                    break;
            }

            buff.clear();
        }

        void skipLine()
        {
            while (true)
            {
                char c = getchar_raw();
                if (eof)
                    break;

                if (c == '\n')
                    break;
            }

            buff.clear();
        }

        token_t tryKeyword(const char* rem, token_type type);
        token_t getQuantity();
        token_t braket();
        optional<token_t> getArrowOrSkipComment();
        token_t getIdentifier();

        token_t create(token_type type)
        {
            auto ret = token_t{ type, move(buff), token_start_line, token_start_column };
            return ret;
        }

        void error(string const& msg) { diag.error(token_t{ token_type::eof, "", token_start_line, token_start_column }, msg); }
        void warning(string const& msg) { diag.warning(token_t{ token_type::eof, "", token_start_line, token_start_column }, msg); }
    };

    class Parser
    {
    public:
        Parser(istream& in, ec::Cluster::Description::ParseOptions const& opts, string const& input_name = string{}) : diag{ input_name }, tokens{ in, diag }, token{}, tmp{}, pconfig{ opts }
        {
            begin = clock();

            if (pconfig.suppress_output)
                diag.suppress(true, true);
            else
                diag.suppress(pconfig.suppress_errors, pconfig.suppress_warnings);
        }

        ~Parser();

        bool parse();
        bool genworkers();
        void apply(Cluster::Description& desc);

    private:
        Diagnostics diag;
        Tokenizer tokens;
        token_t token;
        token_t tmp;
        bool reuse = false;

        bool parsed = false;
        bool gen = false;

        uint32 errors = 0;
        list<ec::Worker::Description*> workers;

        ec::Cluster::Description::ParseOptions const& pconfig;
        clock_t begin;
        clock_t end;

        void nextToken()
        {
            
            if (!reuse)
            {
                tmp = token;
                token = tokens.get();
            }
            else
            {
                reuse = false;
                token = tmp;
            }
        }

        void parseConfig();
        void parseNode();
        void parseWorker();

        void skipStructure();
        void putBack() { assert(!reuse); swap(tmp, token); reuse = true; }

        bool parseQuantity(string const& qunt, uint32& result);

        bool checkToken(token_type expectedType)
        {
            nextToken();
            bool result = (token.type == expectedType);
            putBack();
            return result;
        }

        void processList(std::function<void(void)> const& fun)
        {
            while (true)
            {
                nextToken();
                if (token.type == token_type::eof)
                {
                    error("Expected list end before EOF");
                    break;
                }

                if (token.type == token_type::list_end)
                    break;

                putBack();
                fun();
            }
        }

        void error(string const& msg) { diag.error(token, msg); }
        void warning(string const& msg) { diag.warning(token, msg); }

    private:
        struct SourceStructure
        {
            SourceStructure() : line(0), column(0)
            {}
            SourceStructure(token_t const& t) : line(t.line), column(t.column)
            {}

            uint32 line;
            uint32 column;
        };

        struct Config : public SourceStructure
        {
            using SourceStructure::SourceStructure;

            optional<process_wrapper::output_mode> mode;
            optional<uint32> packetSize;
            optional<bool> source;
        };

        struct Node : public SourceStructure
        {
            using SourceStructure::SourceStructure;

            optional<string> address;
            optional<string> port;
        };

        struct Worker : public SourceStructure
        {
            using SourceStructure::SourceStructure;

            string cmdline = string{};
            Config cfg;
            Node node;
            list<Worker*> dst = list<Worker*>();
            list<Node*> fixed_dst = list<Node*>();
            uint32 quant = 1;

            ec::Worker::Description* _cgresult = nullptr;
            uint32 _cginputs = 0;
        };

        enum var_type
        {
            var_config,
            var_node,
            var_worker
        };

        struct Var
        {
            var_type type;
            void* ptr;
        };

        map<string, Var> vars;

        bool parseConfigField(Config& cfg);
        bool parseNodeField(Node& node);
        bool isValidNodeAddress(string const& str);
        bool parseWorkerDataDst(Worker& worker);
        void setNodeAddress(Node& node, string const& str);

        void genWorker(Worker& w, ec::Worker::Description& desc);

        template <class T>
        void set(optional<T>& dst, T const& u, const char* name)
        {
            if (dst.has_value())
                warning(makestr(string("Overriding previously set ") + name));
            dst = u;
        }

        void copy(Config& dst, Config const& src)
        {
            if (src.mode.has_value())
                set(dst.mode, src.mode.get(), "transfer mode");

            if (src.packetSize.has_value())
                set(dst.packetSize, src.packetSize.get(), "packet size");

            if (src.source.has_value())
                set(dst.source, src.source.get(), "data source flag");
        }

        void copy(Node& dst, Node const& src)
        {
            if (src.address.has_value())
                set(dst.address, src.address.get(), "node address");

            if (src.port.has_value())
                set(dst.port, src.port.get(), "node port");
        }
    };
}

token_t Tokenizer::get()
{
    char c;

    while (true)
    {
        if (!getchar(c))
            return create(token_type::eof);

        switch (c)
        {
        case 'n': return tryKeyword("ode", token_type::kw_node);
        case 'c': return tryKeyword("onfig", token_type::kw_config);
        case 'w': return tryKeyword("orker", token_type::kw_worker);
        case '[': return braket();
        case '{': return create(token_type::list_start);
        case '}': return create(token_type::list_end);
        case '(': return getQuantity();
        case '+': return create(token_type::worker_flag);
        case '@': return create(token_type::worker_node);
        case '-':
        {
            auto ret = getArrowOrSkipComment();
            if (ret.has_value())
                return ret.get();

            break;
        }
        default:
            if (isalpha(c))
                return getIdentifier();
            else
                error("Unexpected character " + string(1, c) + ", all identifiers should start from alphabetical letters");
        }
    }
}

token_t Tokenizer::tryKeyword(const char* rem, token_type type)
{
    char c;
    while (*rem)
    {
        if (!getchar(c))
            return create(token_type::identifier);
        if (c != *rem)
            return getIdentifier();

        ++rem;
    }

    if (getchar(c))
        return getIdentifier();

    return create(type);
}


token_t Tokenizer::getQuantity()
{
    char c = '?';
    buff.clear(); //remove '('
    while (c != ')')
    {
        if (!getchar(c, true))
            return create(token_type::eof);
    }

    buff.pop_back();
    return create(token_type::quantity);
}

token_t Tokenizer::braket()
{
    buff.clear();

    char c;
    while (true)
    {
        if (!getchar(c, true))
        {
            error("No closing ']' found");
            return create(token_type::eof);
        }

        if (c == ']')
        {
            buff.pop_back();
            return create(token_type::braket_text);
        }
    }
}

optional<token_t> Tokenizer::getArrowOrSkipComment()
{
    char c;
    if (!getchar(c))
    {
        if (eof)
        {
            error("Unexpected character '-'");
            return create(token_type::eof);
        }
        else
        {
            error("Expected '>' or '-' got whitespace");
            buff.clear();
            return optional<token_t>::none;
        }
    }

    if (c != '>' && c != '-')
    {
        skip();
        error("Unexpected character sequence: expected -> or --");
        return optional<token_t>::none;
    }

    if (c == '>')
        return create(token_type::arrow);
    else
    {
        skipLine();
        return optional<token_t>::none;
    }
}

token_t Tokenizer::getIdentifier()
{
    char c;
    while (true)
    {
        if (!getchar(c))
            return create(token_type::identifier);

        if (c == '(' || c == '-' || c == '@' || c == '+' || c == '[' || c == '{')
        {
            in.putback(c);
            buff.pop_back();
            return create(token_type::identifier);
        }
        else if (!isalnum(c) && c != '_' && c != '-')
        {
            error("Unexpected character in identifier " + string(1, c) + ", only alphanumeric letters, '_' and '-' are allowed - identifier will be trimmed to this point");
            buff.pop_back();
            auto trunc = move(buff);
            skip();
            buff = move(trunc);
            return create(token_type::identifier);
        }
    }
}

Parser::~Parser()
{
    for (auto const& var : vars)
    {
        switch (var.second.type)
        {
        case var_config: delete reinterpret_cast<Config*>(var.second.ptr); break;
        case var_node:  delete reinterpret_cast<Node*>(var.second.ptr); break;
        case var_worker:
            for (auto* fixed : reinterpret_cast<Worker*>(var.second.ptr)->fixed_dst)
                delete fixed;
            delete reinterpret_cast<Worker*>(var.second.ptr);
            break;

        default:
            assert(false && "Unexpected varaible type");
            break;
        }
    }

    end = clock();
    if (!pconfig.suppress_output)
        diag.show_info(end-begin);
}

bool Cluster::Description::parse(istream& in, Cluster::Description& desc, Cluster::Description::ParseOptions const& opts, string const& input_name)
{
    Parser p{ in, opts, input_name };
    if (!p.parse())
        return false;

    if (!p.genworkers())
        return false;

    p.apply(desc);
    return true;
}

bool Parser::parse()
{
    if (parsed)
        return true;

    while (true)
    {
        nextToken();
        if (token.type == token_type::eof)
            break;

        switch (token.type)
        {
        case token_type::kw_config: parseConfig(); break;
        case token_type::kw_node: parseNode(); break;
        case token_type::kw_worker: parseWorker(); break;
        default:
            error("Only 'config', 'node' and 'worker' keywords are allowed at global scope, found: " + ttype2str(token.type) + " token instead");
        }
    }

    parsed = diag.ok();
    return parsed;
}

bool Parser::genworkers()
{
    if (!parsed)
        return false;

    for (auto const& var : vars)
    {
        if (var.second.type != var_worker)
            continue;

        Worker& w = *reinterpret_cast<Worker*>(var.second.ptr);
        ec::Worker::Description* desc = new ec::Worker::Description;
        
        genWorker(w, *desc);
        workers.push_back(desc);
    }

    for (auto const& var : vars)
    {
        if (var.second.type != var_worker)
            continue;

        Worker& w = *reinterpret_cast<Worker*>(var.second.ptr);
        for (auto* dst : w.dst)
            if (w._cgresult && dst->_cgresult)
            {
                w._cgresult->outputs.push_back(dst->_cgresult);
                ++dst->_cginputs;
            }
            else
                error("Internal error, worker description doesn't exist after first step code gen");
    }

    if (diag.ok())
    {
        for (auto const& var : vars)
        {
            if (var.second.type != var_worker)
                continue;

            Worker& w = *reinterpret_cast<Worker*>(var.second.ptr);
            if (!w._cgresult->isDataSource && !w._cginputs)
                if (pconfig.allow_dangling_workers)
                    warning("Worker not marked as data source doesn't have input nodes");
                else
                    error("Worker not marked as data source doesn't have input nodes");
            else if (w._cgresult->isDataSource && w._cginputs)
                error("Data source node has input nodes");
        }
    }

    gen = diag.ok();
    return gen;
}

void Parser::genWorker(Worker& w, ec::Worker::Description& desc)
{
    auto bpoint = w.cmdline.find(' ');
    if (bpoint != w.cmdline.npos)
        desc.args = w.cmdline.substr(bpoint + 1);
    desc.app = w.cmdline.substr(0, bpoint);

    desc.isDataSource = (w.cfg.source.has_value() && w.cfg.source.get());
    desc.mode = (w.cfg.mode.has_value() ? w.cfg.mode.get() : process_wrapper::one_shot);
    if (w.node.address.has_value() && w.node.port.has_value())
        desc.node = new ec::Node{ "tcp://" + w.node.address.get() + ":" + w.node.port.get() };
    else
        desc.node = nullptr;

    desc.packetSize = (w.cfg.packetSize.has_value() ? w.cfg.packetSize.get() : 0);

    w._cgresult = &desc;

    //outputs are handled in further steps (after all desciptions are made so we can make reference to them)
}

void Parser::apply(Cluster::Description& desc)
{
    if (!gen)
        return;

    desc.workers = move(workers);
}

void Parser::parseConfig()
{
    nextToken();
    if (token.type != token_type::identifier)
    {
        error("Expected identifier after 'config' keyword, got: " + ttype2str(token.type));
        skipStructure();
        return;
    }

    auto configName = move(token.content);
    if (vars.find(configName) != vars.end())
    {
        error("Structure named " + configName + " already exists, second definition ignored");
        skipStructure();
        return;
    }

    Config* cfg = new Config{ token };
    bool ok = true;

    nextToken();
    if (token.type == token_type::list_start)
        processList([&cfg, &ok, this]() { ok &= parseConfigField(*cfg); });
    else if (!parseConfigField(*cfg))
        ok = false;

    if (ok)
        vars.insert(make_pair(configName, Var{ var_config, cfg }));
    else
        delete cfg;
}

bool Parser::parseConfigField(Config& cfg)
{
    nextToken();
    if (token.type != token_type::identifier)
    {
        error("Expected identifier");
        return false;
    }

    if (token.content == "fixed")
    {
        set(cfg.mode, process_wrapper::fixed_packets, "transfer mode");
        if (checkToken(token_type::quantity))
        {
            nextToken();
            uint32 quant;
            if (!parseQuantity(token.content, quant))
                error("Invalid quantity " + token.content);
            else
                set(cfg.packetSize, quant, "packet size");
        }
        else if (!cfg.packetSize.has_value())
        {
            warning("No quantity defined for 'fixed' transport mode, defaults to 1 byte");
            cfg.packetSize = 1;
        }
    }
    else if (token.content == "oneshot")
        set(cfg.mode, process_wrapper::one_shot, "transfer mode");
    else if (token.content == "prefix")
        set(cfg.mode, process_wrapper::prefixed_packets, "transfer mode");
    else if (token.content == "sync")
        set(cfg.mode, process_wrapper::synchronized, "transfer mode");
    else if (token.content == "source")
        set(cfg.source, true, "data source flag");
    else
    {
        auto itr = vars.find(token.content);
        if (itr == vars.end())
        {
            error("Invalid identifier " + token.content);
            return false;
        }
        else
        {
            if (itr->second.type == var_config)
                copy(cfg, *reinterpret_cast<Config*>(itr->second.ptr));
            else
            {
                error("Expected 'config' structure as data source");
                return false;
            }
        }
    }

    return true;
}

void Parser::skipStructure()
{
    do
    {
        token = tokens.get();
    } while (token.type != token_type::eof && token.type != token_type::kw_config && token.type != token_type::kw_node && token.type != token_type::kw_worker);

    putBack();
}

bool Parser::parseQuantity(string const& str, uint32& result)
{
    stringstream ss{ str };

    double quant;
    string unit;
    ss >> quant;
    if (!ss)
        return false;

    ss >> unit;

    if (unit == "B" || unit.empty())
    {
        result = static_cast<uint32>(quant);
        if (result != quant) //part of a byte requested
            return false;
    }
    else if (unit == "KB")
    {
        result = static_cast<uint32>(quant * 1024);
        if (quant * 1024 != result)
            warning("Quantity rounded to " + makestr(result) + " bytes");
    }
    else if (unit == "MB")
    {
        result = static_cast<uint32>(quant * 1024 * 1024);
        if (quant * 1024 * 1024 != result)
            warning("Quantity rounded to " + makestr(result) + " bytes");
    }
    else
        return false;

    return true;
}

void Parser::parseNode()
{
    nextToken();
    if (token.type != token_type::identifier)
    {
        error("Expected identifier after 'node' keyword, got: " + ttype2str(token.type));
        skipStructure();
        return;
    }

    auto nodeName = move(token.content);
    if (vars.find(nodeName) != vars.end())
    {
        error("Structure named " + nodeName + " already exists, second definition ignored");
        skipStructure();
        return;
    }

    Node* node = new Node{ token };
    bool ok = true;

    if (!parseNodeField(*node))
        ok = false;

    if (ok)
        vars.insert(make_pair(nodeName, Var{ var_node, node }));
    else
        delete node;
}

void Parser::setNodeAddress(Node& node, string const& str)
{
    auto pos = str.find(':');
    if (pos != str.npos)
    {
        if (pos != 0)
            set(node.address, str.substr(0, pos), "node address");

        set(node.port, str.substr(pos + 1), "node port");
    }
    else
        set(node.address, str, "node address");
}

bool Parser::parseNodeField(Node& node)
{
    nextToken();
    if (token.type == token_type::braket_text)
    {
        if (isValidNodeAddress(token.content))
            setNodeAddress(node, token.content);
        else
        {
            error("Invalid ip address provided in node's definition");
            return false;
        }
    }
    else if (token.type == token_type::identifier)
    {
        auto itr = vars.find(token.content);
        if (itr == vars.end())
        {
            error("Invalid identifier " + token.content);
            return false;
        }
        else
        {
            if (itr->second.type == var_node)
                copy(node, *reinterpret_cast<Node*>(itr->second.ptr));
            else
            {
                error("Expected 'node' structure as data source");
                return false;
            }
        }
    }

    return true;
}

bool Parser::isValidNodeAddress(string const& str)
{
    if (str.empty())
        return false;
    if (str.size() > 2 && str.front() == '!' && str.back() == '!')
        return (str.find(':') == str.npos);

//    const regex re{ "(((2((5[0-5])|([0-4][0-9]))|(1[0-9]{1,2}))|([1-9][0-9]{0,1}))\\.){3}"
//              "((2((5[0-5])|([0-4][0-9]))|(1[0-9]{1,2}))|([1-9][0-9]{0,1}))"
//              "(:(6(5(5(3([0-5])|([0-2][0-9]))|([0-4][0-9]))|([0-4][0-9]{3}))|([7-9][0-9]{0,3})|([0-9]{1,4})|([1-5][0-9]{0,4})))?" };
	const regex re {
		 "(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(:[0-9]+)?"
		};

    return regex_match(str.c_str(), re);
}

void Parser::parseWorker()
{
    nextToken();
    if (token.type != token_type::identifier)
    {
        error("Expected identifier after 'worker' keyword, got: " + ttype2str(token.type));
        skipStructure();
        return;
    }

    auto workerName = move(token.content);
    if (vars.find(workerName) != vars.end())
    {
        error("Structure named " + workerName + " already exists, second definition ignored");
        skipStructure();
        return;
    }

    Worker* worker = new Worker{ token };
    bool ok = true;

    nextToken();
    if (token.type != token_type::braket_text)
    {
        error("Expected braket with execution command immedietly after worker definition");
        skipStructure();
        ok = false;
    }
    else
    {
        worker->cmdline = move(token.content);
        bool end = false;
        while (!end)
        {
            nextToken();
            switch (token.type)
            {
            case token_type::worker_flag: ok &= parseConfigField(worker->cfg); break;
            case token_type::worker_node: ok &= parseNodeField(worker->node); break;
            case token_type::quantity:    ok &= parseQuantity(token.content, worker->quant); break;

            case token_type::arrow:
                if (checkToken(token_type::list_start))
                {
                    nextToken();
                    processList([&ok, &worker, this]() {
                        ok &= parseWorkerDataDst(*worker);
                    });
                }
                else
                    ok &= parseWorkerDataDst(*worker);

                break;

            default:
                end = true;
                putBack();
                break;
            }
        }
    }

    if (ok)
        vars.insert(make_pair(workerName, Var{ var_worker, worker }));
    else
        delete worker;
}

bool Parser::parseWorkerDataDst(Worker& worker)
{
    nextToken();
    if (token.type != token_type::identifier && token.type != token_type::braket_text)
    {
        error("Expected either identifier or bracket address as data destination for worker, got " + ttype2str(token.type) + " instead");
        return false;
    }

    if (token.type == token_type::identifier)
    {
        auto name = move(token.content);
        bool nameok = false;
        auto itr = vars.find(name);
        if (itr != vars.end())
        {
            if (itr->second.type == var_node)
            {
                Node* var = reinterpret_cast<Node*>(itr->second.ptr);
                if (!var->address.has_value())
                    error("Node '" + name + "' used as worker output doesnt specify destination ip address");
                if (!var->port.has_value())
                    error("Node '" + name + "' used as worker output doesn't specify destination port");

                if (var->address.has_value() && var->port.has_value())
                {
                    worker.fixed_dst.push_back(new Node{ *var });
                    nameok = true;
                }
            }
            else if (itr->second.type == var_worker)
            {
                worker.dst.push_back(reinterpret_cast<Worker*>(itr->second.ptr));
                nameok = true;
            }
            else
                error("Expected 'worker' or 'node' as destination for worker data");
        }
        else
            error("Identifier '" + name + "' does not name any defined structure");

        return nameok;
    }
    else
    {
        if (!isValidNodeAddress(token.content))
            return false;

        Node dst{ token };
        setNodeAddress(dst, token.content);
        if (!dst.address.has_value())
            error("Destination address for worker should contain ip address");
        if (!dst.port.has_value())
            error("Destination address for worker should containt port number");

        if (dst.address.has_value() && dst.port.has_value())
            worker.fixed_dst.push_back(new Node{ dst });
    }

    return true;
}
