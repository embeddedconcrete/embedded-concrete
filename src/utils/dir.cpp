#include "dir.h"
#include <algorithm>
#include <stdexcept>
using namespace std;

Dir::Dir() : myself() { }

Dir::Dir(wstring const& dir) : myself(dir)
{
    performDirectoryPathFixup();
}

void Dir::performDirectoryPathFixup()
{
    replace(myself.begin(), myself.end(), '\\', '/');
    if (myself.back() != '/')
        myself.append(L"/");
}

DirsList Dir::getSubDirs(wstring const& regexp, ListingFlags listing_mask) const
{
    listing_mask.add(ExcludeFiles);
    DirsList ret;
    for (auto const& f : getEntries(regexp, listing_mask))
        ret.push_back(move(f));
    return ret;
}

void Dir::appendDir(Dir const& directory)
{
    if (directory.isAbsolutePath() && !myself.empty())
        throw logic_error{ "Appending absolute path to a directory" };

    myself.append(directory.myself);
}

bool Dir::createIfNotExist() const
{
    for (auto const& subdir : splitPath())
        if (!subdir.exists())
            if (!subdir.mkdir_impl())
                return false;

    return true;
}

DirsList Dir::splitPath() const
{
    DirsList ret{};

    size_t off = 0;
    size_t pos = 0;
    while (true)
    {
        pos = myself.find('/', off);
        if (pos == myself.npos)
            break;

        if (pos != 0) //path doesn't begin with '/'
            ret.push_back(myself.substr(0, pos));
        off = pos + 1;
    }

    ret.push_back(myself.substr(0, pos));
    return ret;
}

FilesList Dir::getComponents() const
{
    FilesList ret{};

    size_t off = 0;
    size_t pos = 0;
    while (true)
    {
        pos = myself.find('/', off);
        if (pos == myself.npos)
            break;

        if (pos != 0) //path doesn't begin with '/'
            ret.push_back(myself.substr(off, pos));
        off = pos + 1;
    }

    ret.push_back(myself.substr(off, pos));
    return ret;
}
