#ifndef SHARED_STR_UTILS_H
#define SHARED_STR_UTILS_H

#include <string>
#include <list>
#include <sstream>
#include <ctime>
#include <chrono>
#include <vector>
#include <locale>
#include <codecvt>
#include <cstring>
#include "optional.h"

#pragma warning(disable: 4996)

inline std::wstring fromutf8(std::string const& str) { return std::wstring_convert<std::codecvt_utf8<wchar_t>>{}.from_bytes(str); }
inline std::wstring fromutf8(const char* str) { return std::wstring_convert<std::codecvt_utf8<wchar_t>>{}.from_bytes(str); }

inline std::string toutf8(std::wstring const& wstr) { return std::wstring_convert<std::codecvt_utf8<wchar_t>>{}.to_bytes(wstr); }
inline std::string toutf8(const wchar_t* wstr) { return std::wstring_convert<std::codecvt_utf8<wchar_t>>{}.to_bytes(wstr); }

bool startswith(std::string const& str, std::string const& with);
bool startswith(std::string const& str, const char* with);
template <std::size_t N>
bool startwwith(std::string const& str, const char with[N]) { return str.compare(0, N, with) == 0; }

bool endswith(std::string const& str, std::string const& with);
bool endswith(std::string const& str, const char* with);
template <std::size_t N>
bool endswith(std::string const& str, const char with[N]) { if (str.size() < N) return false; return str.compare(str.size()-N, N, with) == 0; }


template <class T, class U = typename T::element_type>
std::list<T> split(T const& t, U const& delim, std::size_t max_breaks = std::numeric_limits<std::size_t>::max()) {
	if (t.empty())
		return std::list<T>{ t };

    std::list<T> ret{};
    auto b = t.begin();
    auto c = t.begin() + 1;
    while (c != t.end() && ret.size() != max_breaks) {
        if (*c == delim) {
            ret.push_back(T{ b, c });
            b = ++c;
        }

        if (c != t.end())
            ++c;
    }

    if (b != t.end())
        ret.push_back(T{ b, c });

    return ret;
}

template <class T, class U = typename T::element_type>
T join(std::list<T> const& t, U const& delim) {
    T ret{};
    decltype(ret.size()) total_size = 0;
    for (auto const& tt : t)
        total_size += t.size() + 1;

    ret.reserve(total_size);
    for (auto const& tt : t)
        ret += tt + delim;

    return ret;
}

char hex2char(unsigned i);
std::string bytes2str(const void* ptr, size_t len);

namespace details
{
    template <typename T, bool = std::is_enum<T>::value>
    struct _Writer
    {
        static inline std::ostream& push(std::ostream& out, T const& t) { return out << t; }
    };

    template <typename T>
    struct _Writer<T, true>
    {
        static inline std::ostream& push(std::ostream& out, T const& t) { return out << static_cast<const std::underlying_type_t<T>>(t); }
    };

    template <typename T>
    struct _Writer<std::vector<T>, false>
    {
        static inline std::ostream& push(std::ostream& out, std::vector<T> const& ts)
        {
            out << "[";
            for (decltype(ts.size()) i = 0; i < ts.size(); ++i)
            {
                _Writer<T>::push(out, ts[i]);
                if (i + 1 < ts.size())
                    out << ", ";
            }
            out << "]";
            return out;
        }
    };

    template <typename T>
    struct _Writer<std::list<T>, false>
    {
        static inline std::ostream& push(std::ostream& out, std::list<T> const& ts)
        {
            out << "(";
            auto itr = ts.begin();
            while (itr != ts.end())
            {
                _Writer<T>::push(out, *itr);
                ++itr;
                if (itr != ts.end())
                    out << ", ";
            }
            out << ")";
            return out;
        }
    };

    template <>
    struct _Writer<std::chrono::system_clock::time_point, false>
    {
        static inline std::ostream& push(std::ostream& out, std::chrono::system_clock::time_point const& tp)
        {
            std::time_t tt;
            tt = std::chrono::system_clock::to_time_t(tp);

            struct tm* t;
            t = localtime(&tt);

            char tmp[64];
            strftime(tmp, sizeof(tmp), "%d %B %Y %X", t);
            return out << tmp;
        }
    };

    template <typename T>
    struct _Writer<optional<T>, false>
    {
        static inline std::ostream& push(std::ostream& out, optional<T> const& opt)
        {
            if (!opt.has_value())
                return out << "(not set)";
            else
                return _Writer<T>::push(out, opt.get());
        }
    };

    template <>
    struct _Writer<std::wstring, false>
    {
        static inline std::ostream& push(std::ostream& out, std::wstring const& str)
        {
            return out << toutf8(str);
        }
    };

    template <typename T>
    inline void _makestr(std::ostream& out, T const& t)
    {
        _Writer<T>::push(out, t);
    }

    template <typename T, typename... ARGS>
    inline void _makestr(std::ostream& out, T const& t, ARGS const&... args)
    {
        return _makestr(_Writer<T>::push(out, t), args...);
    }
}

template <typename... T>
std::string makestr(T const&... t)
{
    if (sizeof...(t) == 0)
        return "";

    std::stringstream ss;
    ::details::_makestr(ss, t...);
    return ss.str();
}

namespace details
{
    template <typename T>
    struct _Evaluator
    {
        static inline T evaluate(std::string const& str)
        {
            std::stringstream ss;
            ss.str(str);

            T ret;
            ss >> ret;
            return ret;
        }
    };

    template <>
    struct _Evaluator<bool>
    {
        static inline bool evaluate(std::string const& str)
        {
            if (str == "1" || str == "y" || str == "yes" || str == "t" || str == "tak" || str == "true")
                return true;
            if (str == "0" || str == "n" || str == "no" || str == "nie" || str == "f" || str == "false")
                return false;
            throw std::runtime_error("Invalid value '" + str + "' passed to be parsed as bool!");
            return false;
        }
    };

    template <>
    struct _Evaluator<std::string>
    {
        static inline std::string evaluate(std::string const& str) { return str; }
    };

    template <>
    struct _Evaluator<std::wstring>
    {
        static inline std::wstring evaluate(std::string const& str) { return fromutf8(str); }
    };

    template <typename T>
    struct _Evaluator<optional<T>>
    {
        static inline optional<T> evaluate(std::string const& str) { return optional<T>(_Evaluator<T>::evaluate(str)); }
    };
}

template <typename T>
inline T parse(std::string const& str)
{
    return details::_Evaluator<T>::evaluate(str);
}

template <typename T>
inline bool tryParse(std::string const& str, T& var)
{
    try { var = parse<T>(str); return true; }
    catch (...) { return false; }
}

#endif //SHARED_STR_UTILS_H
