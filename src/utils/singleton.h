#ifndef SHARED_SINGLETON_H
#define SHARED_SINGLETON_H

#include <utility>

template <class C>
class Singleton
{
public:
    template <typename... T>
    static C& singleton(T&&... args)
    {
        static C _me{ std::forward<T>(args)... };
        return _me;
    }
};

#endif //SHARED_SINGLETON_H
