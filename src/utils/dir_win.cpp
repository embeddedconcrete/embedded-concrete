#ifdef WINDOWS
#include "dir.h"
#include "strutils.h"

#include <regex>

#define NOMINMAX
#include <Windows.h>

using namespace std;

/*
Note: Windows uses utf16 encoding
*/

DirsList Dir::getRootDirs()
{
    DWORD mask = GetLogicalDrives();
    DirsList ret;

    char c = 'A';
    for (DWORD i = 0x01; i < mask; i <<= 1, ++c)
        if (mask & i)
            ret.push_back(Dir(c + L":/"));

    return ret;
}

FilesList Dir::getEntries(wstring const& regexp, ListingFlags listing_options) const
{
    if (myself.empty())
        return FilesList();

    auto path = (myself + L"*");
    wregex _regexp(regexp == L"*" ? L".*" : regexp);

    WIN32_FIND_DATAW found_file;
    HANDLE search = FindFirstFileW((LPCWSTR)path.data(), &found_file);
    if (search == INVALID_HANDLE_VALUE)
        throw runtime_error("Cannot open directory " + toutf8(myself));

    FilesList ret;
    wstring name;

    do {
        if (!listing_options[IncludeDots] && wcscmp(found_file.cFileName, L".") == 0)
            continue;
        if (!listing_options[IncludeDots] && wcscmp(found_file.cFileName, L"..") == 0)
            continue;
        if (listing_options[ExcludeDirectories] && found_file.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
            continue;
        if (listing_options[ExcludeFiles] && ((found_file.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0))
            continue;
        if (listing_options[ExcludeVisible] && ((found_file.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) == 0))
            continue;
        if (!listing_options[IncludeHidden] && found_file.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN)
            continue;
        if (!listing_options[IncludeEncrypted] && found_file.dwFileAttributes & FILE_ATTRIBUTE_ENCRYPTED)
            continue;
        if (!listing_options[IncludeSystem] && found_file.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM)
            continue;
        if (listing_options[ExcludeTemporary] && found_file.dwFileAttributes & FILE_ATTRIBUTE_TEMPORARY)
            continue;
        if (listing_options[ExcludeReadonly] && found_file.dwFileAttributes & FILE_ATTRIBUTE_READONLY)
            continue;

        name = move(wstring(&found_file.cFileName[0], lstrlenW(found_file.cFileName)));
        if (!regex_match(name, _regexp))
            continue;

        ret.push_back(myself + name);
    } while (FindNextFileW(search, &found_file));

    FindClose(search);

    return ret;
}

Dir Dir::getCurrentDirectory()
{
    WCHAR buffer[MAX_PATH];
    GetCurrentDirectoryW(MAX_PATH, buffer);

    return Dir(buffer);
}

bool Dir::isAbsolutePath() const
{
    if (myself.size() == 0)
        return false;
    if (myself[0] == '/')
        return true;

    if (myself.size() < 3)
        return false;
    if (myself[1] == ':' && myself[2] == '/')
        return true;

    return false;
}

bool Dir::exists() const
{
    DWORD ftyp = GetFileAttributesW(myself.c_str());
    if (ftyp == INVALID_FILE_ATTRIBUTES)
        return false; //something is wrong with your path!

    if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
        return true; // this is a directory!

    return false; // this is not a directory!
}

bool Dir::mkdir_impl() const
{
    return CreateDirectoryW(myself.c_str(), NULL) == TRUE;
}

#endif //WIN32
