#pragma once

#include "meta.h"

#include <memory>
#include <cassert>
#include <algorithm>
#include <type_traits>
#include <stdexcept>

#ifdef _MSC_VER
#pragma warning(disable: 4297)
#endif

namespace details {
    template <class T, class... U>
    std::enable_if_t<std::is_constructible<T, U&&...>::value> create(void* ptr, U&&... u) {
        new (ptr) T{ std::forward<U>(u)... };
    }

    template <class T, class... U>
    std::enable_if_t<!std::is_constructible<T, U&&...>::value> create(void* ptr, U&&... u) {
        static_assert(meta::always_false<T>::value, "Object of given type could not be constructed by passing provided arguments.");
    }

    template <class T>
    std::enable_if_t<std::is_destructible<T>::value, void> destroy(T& t) {
        t.~T();
    }

    template <class T>
    std::enable_if_t<!std::is_destructible<T>::value, void> destroy(T&) {
        return;
    }

    template <class T, class... U>
    void reinit(T& t, U&&... u) {
        destroy(t);
        create<T>(&t, std::forward<U>(u)...);
    }
}

template <typename T>
struct optional
{
public:
    struct none_t {};
    static constexpr none_t none = none_t{};

    optional() = default;
    constexpr optional(none_t) {}

    optional(T const& t) : assigned(true)
    {
        ::details::create<T>(std::addressof(storage), t);
    }
    optional(T&& t) : assigned(true)
    {
        ::details::create<T>(std::addressof(storage), std::move(t));
    }

    template <typename T1, typename... TS>
    optional(T1&& arg1, TS&&... args) : assigned(true)
    {
        ::details::create<T>(std::addressof(storage), std::forward<T1>(arg1), std::forward<TS>(args)...);
    }

    optional(optional const& other)
    {
        assigned = other.assigned;
        if (assigned)
            ::details::create<T>(std::addressof(storage), other.get());
    }
    optional(optional&& other)
    {
        assigned = other.assigned;
        if (assigned)
            ::details::create<T>(std::addressof(storage), std::move(other.get()));
    }
    ~optional()
    {
        abandon();
    }
    
    T* ptr()                    { return reinterpret_cast<T*>(std::addressof(storage)); }
    const T* ptr() const        { return reinterpret_cast<const T*>(std::addressof(storage)); }
    T* operator ->()            { return ptr(); }
    T* operator ->() const      { return ptr(); }

    bool has_value() const      { return assigned; }
    explicit operator bool()    { return has_value(); }
    
    T& get()                    { assert(has_value() && "Dereferencing optional variable which wasn't set!"); return *ptr(); }
    T const& get() const        { assert(has_value() && "Dereferencing optional variable which wasn't set!"); return *ptr(); }

    T& operator*()              { return get(); }
    T const& operator *() const { return get(); }
    
    void abandon()              { if (assigned) ::details::destroy(get()); assigned = false; }

    void set_has_value()        { assigned = true; }

    optional& operator =(T const& t)
    {
        if (assigned)
            ::details::destroy(get());
        else
            assigned = true;

        ::details::create<T>(std::addressof(storage), t);
        return *this;
    }
    optional& operator =(T&& t)
    {
        if (assigned)
            ::details::destroy(get());
        else
            assigned = true;

        ::details::create<T>(std::addressof(storage), std::move(t));
        return *this;
    }

    optional& operator =(optional const& opt)       { if (!opt.has_value()) abandon(); else operator=(*opt); return *this; }
    optional& operator =(optional&& opt)            { if (!opt.has_value()) abandon(); else operator=(*opt); return *this; }

    optional& operator =(none_t)                    { abandon(); return *this; }

private:
    bool assigned = false;
    std::aligned_storage_t<sizeof(T), alignof(T)> storage;
};

template <class T, class ErrT = int>
struct maybe_error
{
    template <class TT, class EE> friend struct maybe_error;
public:
    struct error { 
        template <class... U, class Guard = std::enable_if_t<std::is_constructible<ErrT, U&&...>::value>>
        constexpr explicit error(U&&... u) : e{ std::forward<U>(u)... }
        {}

        ErrT e;
    };
    struct critical {
        template <class... U, class Guard = std::enable_if_t<std::is_constructible<ErrT, U&&...>::value>>
        constexpr explicit critical(U&&... u) : e{ std::forward<U>(u)... }
        {}

        ErrT e;
    };
    struct forward_error {
        template <class U>
        explicit forward_error(maybe_error<U, ErrT>& m) : crit{ m.is_critical() }, e{ m.get_error() } //note: get_error() clears throwing flag so we need to copy it before copying error
        {}

        template <class U>
        explicit forward_error(maybe_error<U, ErrT>&& m) : crit{ m.is_critical() }, e{ m.get_error() }
        {}

        bool crit;
        ErrT e;
    };

    struct none_t {};
    static constexpr none_t none = none_t{};

public:

    maybe_error(maybe_error const& m)
    {
        if(m.has_value())
            ::details::create<T>(std::addressof(err_or_value), m.get());
        else if(m.has_error())
            ::details::create<ErrT>(std::addressof(err_or_value), m.get_error());

        state = m.state;
    }
    
    maybe_error(maybe_error&& m)
    {
        if(m.has_value())
            ::details::create<T>(std::addressof(err_or_value), std::move(m.get()));
        else if(m.has_error())
            ::details::create<ErrT>(std::addressof(err_or_value), std::move(m.get_error()));

        state = m.state;
        m.state &= ~throw_flag;
    }

    template <class TT, class EE, class Guard = std::enable_if_t<std::is_convertible<TT const&,T>::value && std::is_convertible<EE const&, ErrT>::value>>
    maybe_error(maybe_error<TT, EE> const& m)
    {
        if (m.has_value())
            ::details::create<T>(std::addressof(err_or_value), m.get());
        else if (m.has_error())
            ::details::create<ErrT>(std::addressof(err_or_value), m.get_error());

        state = m.state;
    }

    template <class TT, class EE, class Guard = std::enable_if_t<std::is_convertible<TT const&, T>::value && std::is_convertible<EE const&, ErrT>::value>>
    maybe_error(maybe_error<TT, EE>&& m)
    {
        if (m.has_value())
            ::details::create<T>(std::addressof(err_or_value), std::move(m.get()));
        else if (m.has_error())
            ::details::create<ErrT>(std::addressof(err_or_value), std::move(m.get_error()));

        state = m.state;
        m.state &= ~throw_flag;
    }

    maybe_error(T&& t) : state(value_flag)
    {
        ::details::create<T>(std::addressof(err_or_value), std::move(t));
    }
    maybe_error(T const& t) : state(value_flag)
    {
        ::details::create<T>(std::addressof(err_or_value), t);
    }
    template <class... U, class Guard = std::enable_if_t<std::is_constructible<T, U&&...>::value>>
    maybe_error(U&&... u) : state(value_flag)
    {
        ::details::create<T>(std::addressof(err_or_value), std::forward<U>(u)...);
    }

    maybe_error(error&& e) : state(error_flag)
    {
        ::details::create<ErrT>(std::addressof(err_or_value), std::move(e.e));
    }
    maybe_error(error const& e) : state(error_flag)
    {
        ::details::create<ErrT>(std::addressof(err_or_value), e.e);
    }

    maybe_error(critical&& e) : state(error_flag | throw_flag)
    {
        ::details::create<ErrT>(std::addressof(err_or_value), std::move(e.e));
    }
    maybe_error(critical const& e) : state(error_flag | throw_flag)
    {
        ::details::create<ErrT>(std::addressof(err_or_value), e.e);
    }

    maybe_error(forward_error&& e) : state(error_flag | (e.crit ? throw_flag : 0))
    {
        ::details::create<ErrT>(std::addressof(err_or_value), std::move(e.e));
    }
    maybe_error(forward_error const& e) : state(error_flag | (e.crit ? throw_flag : 0))
    {
        ::details::create<ErrT>(std::addressof(err_or_value), e.e);
    }

    constexpr maybe_error(none_t) : state(0)
    {}

    ~maybe_error() throw()
    {
        if (!is_critical()) {
            abandon();
            return;
        }

        //make sure ErrT's dtor will be called after exception throw
        // extra: maybe pass somehow e to exception info?
        ErrT e = std::move(get_error());
        abandon();
        throw std::runtime_error("Unchecked critical error");
    }

    maybe_error& operator =(T const& t)
    {
        abandon();
        ::details::create<T>(std::addressof(err_or_value), t);
        state = value_flag;
        return *this;
    }

    maybe_error& operator =(T&& t)
    {
        abandon();
        ::details::create<T>(std::addressof(err_or_value), std::move(t));
        state = value_flag;
        return *this;
    }

    maybe_error& operator =(maybe_error const& opt)
    {
        abandon();
        if (opt)
            ::details::create<T>(std::addressof(err_or_value), opt.get());
        else if (opt.has_error())
            ::details::create<ErrT>(std::addressof(err_or_value), opt.get_error());

        state = opt.state;
        return *this;
    }

    maybe_error& operator =(maybe_error&& opt)
    {
        abandon();
        if (opt)
            ::details::create<T>(std::addressof(err_or_value), std::move(opt.get()));
        else if (opt.has_error())
            ::details::create<ErrT>(std::addressof(err_or_value), std::move(opt.get_error()));

        state = opt.state;
        opt.state &= ~throw_flag;
        return *this;
    }

    maybe_error& operator =(none_t)
    {
        abandon();
        return *this;
    }

    maybe_error& operator =(error const& e) { abandon(); set_error(e, false); return *this; }
    maybe_error& operator =(error&& e) { abandon(); set_error(std::move(e), false); return *this; }

    maybe_error& operator =(critical const& e) { abandon(); set_error(e, true); return *this; }
    maybe_error& operator =(critical&& e) { abandon(); set_error(std::move(e), true); return *this; }

    constexpr auto has_value() const    { return (state & value_flag) != 0; }
    constexpr auto has_error() const    { return (state & error_flag) != 0; }
    constexpr auto is_critical() const  { return (state & throw_flag) != 0; }

    auto& get() { return reinterpret_cast<T&>(err_or_value); }
    auto& get_error() { assert(has_error() && "no error"); state &= ~throw_flag; return reinterpret_cast<ErrT&>(err_or_value); }

    operator bool() const { return has_value(); }

    void abandon()
    {
        if (has_value())
            ::details::destroy(get());
        else if (has_error())
            ::details::destroy(get_error());

        state = 0;
    }

    maybe_error& make_critical()    { state |= throw_flag; return *this; }
    maybe_error& make_silent()      { state &= ~throw_flag; return *this; }

private:
    enum {
        value_flag = 0x01,
        error_flag = 0x02,
        throw_flag = 0x04
    };

    char state;
    std::aligned_union_t<meta::max<size_t, sizeof(T), sizeof(ErrT)>::value, T, ErrT> err_or_value;

    template <class U>
    ErrT&& forward_err(U&& e) { return std::move(e.e); }

    template <class U>
    ErrT const& forward_err(U const& e) { return e.e; }

    template <class U>
    void set_error(U&& err, bool crit)
    {
        abandon();
        ::details::create<ErrT>(std::addressof(err_or_value), forward_err(err.e));

        state = error_flag;
        if (crit)
            state |= throw_flag;
    }

    auto& get() const { return reinterpret_cast<T const&>(err_or_value); }
    auto& get_error() const { assert(has_error() && "no error"); return reinterpret_cast<ErrT const&>(err_or_value); }
};


