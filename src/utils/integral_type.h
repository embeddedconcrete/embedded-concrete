#pragma once

#include <limits>
#include <iostream>
#include <type_traits>
#include "meta.h"

#ifndef NDEBUG
#include <stdexcept>

#define WRITE_INTEGRAL_TYPE_VALUE(value) (( static_cast<std::common_type_t<T, U>>(value) > static_cast<std::common_type_t<T, U>>(std::numeric_limits<T>::max()) \
                                            || static_cast<std::common_type_t<T, U>>(value) < static_cast<std::common_type_t<T, U>>(std::numeric_limits<T>::lowest())) \
                                            ? (throw std::runtime_error("integral_type: narrowing assigment")) \
                                            : (static_cast<T>(std::forward<U>(value))))

#define READ_INTEGRAL_TYPE_VALUE         (( static_cast<std::common_type_t<T, U>>(storage) > static_cast<std::common_type_t<T, U>>(std::numeric_limits<U>::max()) \
                                            || static_cast<std::common_type_t<T, U>>(storage) < static_cast<std::common_type_t<T, U>>(std::numeric_limits<U>::lowest())) \
                                            ? (throw std::runtime_error("integral_type: narrowing cast")) \
                                            : (static_cast<U>(storage)))

#else
 #define WRITE_INTEGRAL_TYPE_VALUE(value)
 #define READ_INTEGRAL_TYPE_VALUE
#endif


template <class T>
struct integral_type
{
    static_assert(std::is_integral<T>::value, "integral_type: storage must be std::is_integral");

    friend std::ostream& operator << (std::ostream& out, integral_type const& t)
    {
        return out << t.storage;
    }
    friend std::istream& operator >> (std::istream& in, integral_type& t)
    {
        return in >> t.storage;
    }

    constexpr integral_type() = default;
    constexpr integral_type(integral_type const&) = default;
    constexpr integral_type(integral_type&&) = default;

    integral_type& operator =(integral_type const&) = default;
    integral_type& operator =(integral_type&&) = default;

    template <class U, std::enable_if_t<std::is_convertible<U&&, T>::value, bool> Guard = false>
    constexpr integral_type(U&& t) : storage(WRITE_INTEGRAL_TYPE_VALUE(t))
    {
    }

    template <class U, std::enable_if_t<std::is_convertible<T const&, U>::value && !std::is_same<U, bool>::value, bool> Guard = false>
    constexpr operator U() const 
    {
#ifndef NDEBUG
        return READ_INTEGRAL_TYPE_VALUE;

#else
        return static_cast<U>(storage);
#endif
    }

    explicit operator bool() const { return storage != 0; }

    operator T() const { return storage; }

    template <class U>
    std::enable_if_t<std::is_convertible<U&&, T>::value, integral_type&> operator =(U&& t)
    {
        set(std::forward<U>(t));
        return *this;
    }

    template <class U>
    integral_type& operator +=(U&& t) { set(static_cast<std::common_type_t<U&&, T>>(storage) + static_cast<std::common_type_t<U&&, T>>(std::forward<U>(t))); return *this; }

    template <class U>
    integral_type& operator -=(U&& t) { set(static_cast<std::common_type_t<U&&, T>>(storage) - static_cast<std::common_type_t<U&&, T>>(std::forward<U>(t))); return *this; }

    template <class U>
    integral_type& operator *=(U&& t) { set(static_cast<std::common_type_t<U&&, T>>(storage) * static_cast<std::common_type_t<U&&, T>>(std::forward<U>(t)) ); return *this; }

    template <class U>
    integral_type& operator /=(U&& t) { set(static_cast<std::common_type_t<U&&, T>>(storage) / static_cast<std::common_type_t<U&&, T>>(std::forward<U>(t))); return *this; }

    template <class U>
    integral_type& operator %=(U&& t) { set(static_cast<std::common_type_t<U&&, T>>(storage) % static_cast<std::common_type_t<U&&, T>>(std::forward<U>(t))); return *this; }

    template <class U>
    integral_type& operator +=(integral_type<U> t) { set(static_cast<std::common_type_t<U, T>>(storage) + static_cast<std::common_type_t<U, T>>(std::forward<U>(t.storage))); return *this; }

    template <class U>
    integral_type& operator -=(integral_type<U> t) { set(static_cast<std::common_type_t<U, T>>(storage) - static_cast<std::common_type_t<U, T>>(std::forward<U>(t.storage))); return *this; }

    template <class U>
    integral_type& operator *=(integral_type<U> t) { set(static_cast<std::common_type_t<U, T>>(storage) * static_cast<std::common_type_t<U, T>>(std::forward<U>(t.storage))); return *this; }

    template <class U>
    integral_type& operator /=(integral_type<U> t) { set(static_cast<std::common_type_t<U, T>>(storage) / static_cast<std::common_type_t<U, T>>(std::forward<U>(t.storage))); return *this; }

    template <class U>
    integral_type& operator %=(integral_type<U> t) { set(static_cast<std::common_type_t<U, T>>(storage) % static_cast<std::common_type_t<U, T>>(std::forward<U>(t.storage))); return *this; }

    template <class U>
    constexpr integral_type operator +(U&& t) const { return integral_type{ static_cast<std::common_type_t<U&&, T>>(storage) + static_cast<std::common_type_t<U&&, T>>(std::forward<U>(t)) }; }

    template <class U>
    constexpr integral_type operator -(U&& t) const { return integral_type{ static_cast<std::common_type_t<U&&, T>>(storage) - static_cast<std::common_type_t<U&&, T>>(std::forward<U>(t)) }; }

    template <class U>
    constexpr integral_type operator *(U&& t) const { return integral_type{ static_cast<std::common_type_t<U&&, T>>(storage) * static_cast<std::common_type_t<U&&, T>>(std::forward<U>(t)) }; }

    template <class U>
    constexpr integral_type operator /(U&& t) const { return integral_type{ static_cast<std::common_type_t<U&&, T>>(storage) / static_cast<std::common_type_t<U&&, T>>(std::forward<U>(t)) }; }

    template <class U>
    constexpr integral_type operator %(U&& t) const { return integral_type{ static_cast<std::common_type_t<U&&, T>>(storage) % static_cast<std::common_type_t<U&&, T>>(std::forward<U>(t)) }; }

    template <class U>
    constexpr integral_type operator +(integral_type<U> t) const { return integral_type{ static_cast<std::common_type_t<U, T>>(storage) + static_cast<std::common_type_t<U, T>>(t.storage) }; }

    template <class U>
    constexpr integral_type operator -(integral_type<U> t) const { return integral_type{ static_cast<std::common_type_t<U, T>>(storage) - static_cast<std::common_type_t<U, T>>(t.storage) }; }

    template <class U>
    constexpr integral_type operator *(integral_type<U> t) const { return integral_type{ static_cast<std::common_type_t<U, T>>(storage) * static_cast<std::common_type_t<U, T>>(t.storage) }; }

    template <class U>
    constexpr integral_type operator /(integral_type<U> t) const { return integral_type{ static_cast<std::common_type_t<U, T>>(storage) / static_cast<std::common_type_t<U, T>>(t.storage) }; }

    template <class U>
    constexpr integral_type operator %(integral_type<U> t) const { return integral_type{ static_cast<std::common_type_t<U, T>>(storage) % static_cast<std::common_type_t<U, T>>(t.storage) }; }

    template <class U>
    bool operator <(integral_type<U> const& r) const { return static_cast<std::common_type_t<T, U>>(storage) < static_cast<std::common_type_t<T, U>>(r.storage); }

    template <class U>
    bool operator >(integral_type<U> const& r) const { return static_cast<std::common_type_t<T, U>>(storage) > static_cast<std::common_type_t<T, U>>(r.storage); }

    template <class U>
    bool operator ==(integral_type<U> const& r) const { return static_cast<std::common_type_t<T, U>>(storage) == static_cast<std::common_type_t<T, U>>(r.storage); }

    template <class U>
    bool operator !=(integral_type<U> const& r) const { return static_cast<std::common_type_t<T, U>>(storage) != static_cast<std::common_type_t<T, U>>(r.storage); }

    template <class U>
    bool operator <=(integral_type<U> const& r) const { return static_cast<std::common_type_t<T, U>>(storage) <= static_cast<std::common_type_t<T, U>>(r.storage); }

    template <class U>
    bool operator >=(integral_type<U> const& r) const { return static_cast<std::common_type_t<T, U>>(storage) >= static_cast<std::common_type_t<T, U>>(r.storage); }

    integral_type& operator ++() { ++storage; return *this; }
    integral_type& operator --() { --storage; return *this; }
    integral_type operator ++(int) { return integral_type{ storage++ }; }
    integral_type operator --(int) { return integral_type{ storage-- }; }

private:
    void set(T&& t)
    {
        storage = static_cast<T>(std::forward<T>(t));
    }

    template <class U>
    void set(U&& t)
    {
#ifndef NDEBUG
        storage = WRITE_INTEGRAL_TYPE_VALUE(t);
#else
        storage = static_cast<T>(std::forward<U>(t));
#endif
    }

    T storage;
};
