#pragma once

#include <string>

class file_utils
{
public:
    enum file_type
    {
        regular,
        executable,
        directory,
        socket,
        invalid //doesnt exist
    };
    enum access_mode
    {
        read = 0x01,
        write = 0x02,
        execute = 0x04,

        read_write = (read | write),
        read_execute = (read | execute),
        write_execute = (write | execute),
        all = (read | write | execute)
    };


    static bool exists(std::string const& path);
    static file_type type(std::string const& path);
    static bool can_access(std::string const& path, access_mode mode);
};

inline file_utils::access_mode operator |(file_utils::access_mode m1, file_utils::access_mode m2) 
{ 
    return static_cast<file_utils::access_mode>(static_cast<unsigned char>(m1) | static_cast<unsigned char>(m2));
}