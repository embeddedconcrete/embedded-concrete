#ifndef SHARED_LOG_H
#define SHARED_LOG_H

#include "strutils.h"
#include "nlqueue.h"
#include <ostream>
#include <thread>
#include <list>

class Log
{
public:
    Log();
    ~Log();

    void log(std::string str)
    {
        if (!run)
            return;

        return _log(std::move(str));
    }

    template <typename... T>
    void log(T const&... t)
    {
        if (sizeof...(t) == 0 || !run)
            return;

        return _log(makestr(t...));
    }

    void cout(std::string str, bool print_header = true)
    {
        if (!run)
            return;

        return _log(std::move(str), true, print_header);
    }

    template <typename... T>
    void cout(T const&... t)
    {
        if (sizeof...(t) == 0 || !run)
            return;

        return _log(makestr(t...), true);
    }

    void addOutput(std::ostream* out)
    {
        if (out == nullptr)
            return;

        outs.push_back(out);
    }

    void removeOutput(std::ostream* out)
    {
        outs.remove(out);
    }

    void close(bool flush = true)
    {
        if (run.exchange(false))
            worker.join();
        if (flush)
            _flush();

        _clearQueue();
        outs.clear();
    }

    void flush()
    {
        sync = false;
        while (!sync.exchange(false))
            continue;
    }

    static Log& singleton();

    struct lock_guard
    {
        lock_guard(Log& l) : mylog(l) {
            bool dummy;
            while (mylog.lockflag.compare_exchange_strong(dummy, true))
                std::this_thread::sleep_for(std::chrono::microseconds(10));
        }

        ~lock_guard() {
            mylog.lockflag = false;
        }

        Log& mylog;
    };

    friend struct lock_guard;

    auto lock() { return lock_guard(*this); }

    void suppress(bool supp = true);

private:
    struct Msg
    {
        std::string msg;
        decltype(std::this_thread::get_id()) thread_id;
        std::chrono::system_clock::time_point queue_time;
        bool cout = false;
        bool print_header = true;
    };

    void _log(std::string str, bool cout = false, bool print_header = true);

    bool init;
    std::atomic_bool run;
    std::atomic_bool lockflag;
    std::atomic_bool sync;
    std::atomic_bool print;
    std::thread worker;

    NlQueue<Msg*> msgs;
    std::list<std::ostream*> outs;

    void _flush();
    void _clearQueue();
};

#define sLog Log::singleton()

#define BEGIN_LOCKED_REGION(LogInst) { auto _log_lock = (LogInst).lock();
#define END_LOCKED_REGION }

#endif //SHARED_LOG_H
