#ifdef WINDOWS

#include "file_utils.h"

#include <Windows.h>

bool file_utils::exists(std::string const& path)
{
    DWORD dwAttrib = GetFileAttributes(path.c_str());
    return (dwAttrib != INVALID_FILE_ATTRIBUTES || (GetLastError() != ERROR_FILE_NOT_FOUND && GetLastError() != ERROR_PATH_NOT_FOUND));
}

file_utils::file_type file_utils::type(std::string const& path)
{
    DWORD dwAttrib = GetFileAttributes(path.c_str());
    if (dwAttrib == INVALID_FILE_ATTRIBUTES)
        return invalid;

    if (dwAttrib & FILE_ATTRIBUTE_DIRECTORY)
        return directory;

    DWORD type;
    if (FALSE == GetBinaryType(path.c_str(), &type))
        return regular;

    return executable;
}

bool file_utils::can_access(std::string const& path, access_mode mode)
{
    bool bRet = false;
    DWORD length = 0;
    if (!GetFileSecurity(path.c_str(), OWNER_SECURITY_INFORMATION | GROUP_SECURITY_INFORMATION | DACL_SECURITY_INFORMATION, NULL, NULL, &length) && ERROR_INSUFFICIENT_BUFFER == GetLastError())
    {
        PSECURITY_DESCRIPTOR security = static_cast<PSECURITY_DESCRIPTOR>(malloc(length));
        if (security && GetFileSecurity(path.c_str(), OWNER_SECURITY_INFORMATION | GROUP_SECURITY_INFORMATION | DACL_SECURITY_INFORMATION, security, length, &length))
        {
            HANDLE hToken = NULL;
            if (OpenProcessToken(GetCurrentProcess(), TOKEN_IMPERSONATE | TOKEN_QUERY | TOKEN_DUPLICATE | STANDARD_RIGHTS_READ, &hToken))
            {
                HANDLE hImpersonatedToken = NULL;
                if (DuplicateToken(hToken, SecurityImpersonation, &hImpersonatedToken)) 
                {
                    GENERIC_MAPPING mapping = { 0xFFFFFFFF };
                    PRIVILEGE_SET privileges = { 0 };
                    DWORD grantedAccess = 0, privilegesLength = sizeof(privileges);
                    BOOL result = FALSE;

                    DWORD genericAccess = mode;

                    mapping.GenericRead = read;
                    mapping.GenericWrite = write;
                    mapping.GenericExecute = execute;
                    mapping.GenericAll = all;

                    MapGenericMask(&genericAccess, &mapping);
                    if (AccessCheck(security, hImpersonatedToken, genericAccess, &mapping, &privileges, &privilegesLength, &grantedAccess, &result))
                        bRet = (result == TRUE);

                    CloseHandle(hImpersonatedToken);
                }
                CloseHandle(hToken);
            }
        }

        if (security)
            free(security);
    }

    return bRet;
}

#endif //WINDOWS