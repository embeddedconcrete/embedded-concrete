/*
 *  WxFramework all rights reserved to Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 */

#include "strutils.h"


bool startswith(std::string const& str, std::string const& with) { return str.compare(0, with.size(), with) == 0; }
bool startswith(std::string const& str, const char* with) { return str.compare(0, strlen(with), with) == 0; }

bool endswith(std::string const & str, std::string const & with) { if (str.size() < with.size()) return false; return str.compare(str.size()-with.size(), with.size(), with) == 0; }
bool endswith(std::string const & str, const char * with) { auto with_size = strlen(with); if (str.size() < with_size) return false; return str.compare(str.size() - with_size, with_size, with) == 0; }

char hex2char(unsigned i)
{
    if (i >= 0 && i <= 9)
        return '0' + i;
    if (i >= 10 && i <= 15)
        return 'a' + (i - 10);
    return '?';
}
std::string bytes2str(const void* ptr, size_t len)
{
    const unsigned char* cptr = (const unsigned char*)ptr;
    std::string ret;
    ret.reserve(len * 5);
    for (int i = 0; i<len; ++i)
    {
        ret.append("0x");
        ret.push_back(hex2char(((unsigned)cptr[i]) / 16));
        ret.push_back(hex2char(((unsigned)cptr[i]) % 16));
        ret.push_back(' ');
    }

    return ret;
}
