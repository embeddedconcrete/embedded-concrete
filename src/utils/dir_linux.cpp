#ifdef LINUX
#include "dir.h"
#include "strutils.h"

#include <regex>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>
#include <limits>

using namespace std;

#define MAX_PATH 256

DirsList Dir::getRootDirs()
{
    return DirsList{ Dir(L"/") };
}

FilesList Dir::getEntries(wstring const& regexp, ListingFlags listing_options) const
{
    if (myself.empty())
        return FilesList();

    string myself_str = toutf8(myself);
    string path = toutf8(myself + L"*");

    regex _regexp(toutf8(regexp == L"*" ? L".*" : regexp));


    DIR * search = opendir(path.c_str());
    if (search == nullptr)
        throw runtime_error("Cannot open directory " + toutf8(myself));

    FilesList ret;
    string name;

    struct stat path_stat;
    stat(path.c_str(), &path_stat);
    dirent *entry = nullptr;

    while (nullptr != (entry = readdir(search))) {
        if (!listing_options[IncludeDots] && (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0))
            continue;
        if (listing_options[ExcludeDirectories] && S_ISDIR(path_stat.st_mode))
            continue;
        if (listing_options[ExcludeFiles] && !S_ISDIR(path_stat.st_mode))
            continue;
        if (listing_options[ExcludeVisible] && entry->d_name[0] != '.')
            continue;
        if (!listing_options[IncludeHidden] && entry->d_name[0] == '.')
            continue;

        string fullpath = myself_str + entry->d_name;
        if (listing_options[ExcludeReadonly] && !access(fullpath.c_str(), W_OK))
            continue;

        if (listing_options[ExcludeFiles] && S_ISREG(path_stat.st_mode))
            continue;

        name = entry->d_name;
        if (!regex_match(name, _regexp))
            continue;

        ret.push_back(myself + fromutf8(name));
    }
    return ret;
}

Dir Dir::getCurrentDirectory()
{
    char buffer[MAX_PATH];
    if (!getcwd(buffer, sizeof(buffer)))
        return Dir{};

    return Dir{ fromutf8(buffer) };
}

bool Dir::isAbsolutePath() const
{
    if (myself.size() == 0)
        return false;
    if (myself[0] == '/')
        return true;

    return false;
}


bool Dir::exists() const
{
    return access(toutf8(myself).c_str(), F_OK) != -1;
}

bool Dir::mkdir_impl() const
{
    char tmp[256];
    auto str = toutf8(myself);
    if (str.size() > sizeof(tmp))
        return false;

    str.back() = 0;
    memcpy(tmp, str.data(), str.size());
    mkdir(tmp, S_IRWXU);
    return true;
}

#endif //LINUX
