#include "mtlog.h"
#include "singleton.h"
#include <thread>
#include <iostream>
using namespace std;

#define LOCKING_LOG

Log& Log::singleton()
{
    return Singleton<Log>::singleton();
}

Log::Log() : init(false), run(),
#if defined(EC_DEBUG) && defined(LOCKING_LOG)
    worker([] { return; }), msgs()
#else
    worker([this]{ while (run || !init){ if (init) _flush(); std::this_thread::sleep_for(std::chrono::milliseconds(100)); }}), msgs()
#endif
{
    atomic_init(&run, true);
    atomic_init(&lockflag, false);
    atomic_init(&sync, false);
    atomic_init(&print, true);
    init = true;
}

Log::~Log()
{
    close();
}

void Log::suppress(bool supp)
{
    print = !supp;
}

void Log::_log(string str, bool cout, bool print_header)
{
    if (!run)
        return;

    Msg* m = new Msg();
    m->msg = std::move(str);
    m->queue_time = std::chrono::system_clock::now();
    m->thread_id = std::this_thread::get_id();
    m->cout = cout;
    m->print_header = print_header;
    msgs.push(m);
#if defined(EC_DEBUG) && defined(LOCKING_LOG)
    _flush();
#endif
}

void Log::_flush()
{
    if (!print)
        return;

    for (auto msg : msgs.fetchAll())
    {
        BEGIN_LOCKED_REGION(*this)
            for (auto o : outs)
                if (!msg->cout || o == &std::cout)
                {
                    if (msg->print_header)
                        (*o) << makestr("[Thread ", msg->thread_id, " at ", msg->queue_time, "]:  ");
                    (*o) << msg->msg;
                    if (msg->print_header)
                        (*o) << '\n';

                    o->flush();
                    if (msg->cout)
                        break;
                }
        END_LOCKED_REGION
        delete msg;
    }

    sync = true;
}

void Log::_clearQueue()
{
    for (auto msg : msgs.fetchAll())
        delete msg;
}
