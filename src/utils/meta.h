#pragma once

#include <type_traits>


namespace meta {

struct invalid_type { };

struct empty {
    template <class... T> empty(T...) {}
    template <class T> operator T() { return T{}; }
    template <class T> empty& operator =(T) { return *this; }
    template <class T> bool operator ==(T) { return false; }
    template <class T> bool operator !=(T) { return false; }
};

template <class T> struct always_false : public std::false_type {};
template <class T> struct always_true : public std::true_type {};

struct none { };

template <class... T>
struct pack { template <class U> using prepend = pack<U, T...>; template <class U> using append = pack<T..., U>; };

template <class A, class B>
struct is_same : public std::false_type { };

template <class T>
struct is_same<T, T> : public std::true_type { };

template <class T, class... TT>
struct is_one_of : public std::false_type { };

template <class T, class U, class... Rest>
struct is_one_of<T, U, Rest...> : is_one_of<T, Rest...> { };

template <class T, class... Rest>
struct is_one_of<T, T, Rest...> : std::true_type { };

template <class FunType, class ExpectedReturn>
struct is_valid_invoke
{
private:
    template <class U>
    static std::integral_constant<bool, std::is_same<std::result_of_t<FunType>, ExpectedReturn>::value> check(std::nullptr_t);

    template <class U>
    static std::false_type check(void*);

public:
    static constexpr bool value = decltype(check<FunType>(nullptr))::value;
};

template <std::size_t I, class... T>
struct ith;

template <std::size_t I, class T, class... Rest>
struct ith<I, T, Rest...> : public ith<I - 1, Rest...> {};

template <class T, class... Rest>
struct ith<0, T, Rest...> { using type = T; };

template <std::size_t I, class... T>
using ith_t = typename ith<I, T...>::type;

template <bool... B>
struct all : public std::true_type {};

template <bool... Rest>
struct all<true, Rest...> : all<Rest...> {};

template <bool... Rest>
struct all<false, Rest...> : std::false_type {};

namespace details {
    template <class T, T Max, T... Value>
    struct max : std::integral_constant<T, Max> {};

    template <class T, T Max, T Head, T... Tail>
    struct max<T, Max, Head, Tail...> : max<T, (Head > Max ? Head : Max), Tail...> {};
}

template <class T, T... Values>
struct max {};

template <class T, T Head, T... Tail>
struct max<T, Head, Tail...> : details::max<T, Head, Tail...> {};

template <class T>
struct is_container
{
private:
    template <class U>
    static auto helper(std::nullptr_t)
        -> std::enable_if_t<
                std::is_same<decltype(std::declval<U>().begin()), decltype(std::declval<U>().end())>::value &&
                always_true<decltype(std::declval<U>().size())>::value,
                std::true_type>;
    template <class U>
    static std::false_type helper(void*);

public:
    static constexpr bool value = decltype(helper<T>(nullptr))::value;
};

}
