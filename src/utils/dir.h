#pragma once

#include "flag.h"
#include "stringlist.h"

class Dir;
typedef std::list<Dir> DirsList;

class Dir
{
public:

    enum ListingFlag
    {
        ExcludeVisible = 0x001,
        IncludeHidden = 0x002,
        IncludeEncrypted = 0x004,
        IncludeSystem = 0x008,
        ExcludeTemporary = 0x010,
        ExcludeReadonly = 0x020,

        ExcludeDirectories = 0x040,
        ExcludeFiles = 0x080,
        IncludeDots = 0x100,

        All = IncludeHidden | IncludeEncrypted | IncludeSystem,
        AllFiles = All | ExcludeDirectories,
        AllDirs = All | ExcludeFiles,

        Files = ExcludeDirectories,
        Dirs = ExcludeFiles,

        DefaultListing = 0
    };

    typedef Flag<std::underlying_type_t<ListingFlag>> ListingFlags;

    // ^
    //maska rowna 0 (domyslna) bedzie zawierala:
    // widoczne,
    // nie szyfrowane,
    // nie systemowe,
    //pliki, zarowno tymczasowe jak i przeznaczone jedynie do odczytu

public:
    Dir();
    Dir(std::wstring const& dir);

    static DirsList getRootDirs();

    FilesList getEntries(std::wstring const& regexp = L".*", ListingFlags listing_mask = DefaultListing) const;
    DirsList getSubDirs(std::wstring const& regexp = L".*", ListingFlags listing_mask = DefaultListing) const;

    inline std::wstring path() const
    {
        return myself;
    }

    inline std::wstring name() const
    {
        auto ret = myself.substr(myself.rfind('/', 1) + 1);
        ret.pop_back();
        return ret;
    }

    void appendDir(Dir const& directory);
    bool createIfNotExist() const;
    bool exists() const;

    bool isAbsolutePath() const;

    /*
        Parses path:
            /this/is/sample/path
        into directory list:
            /this
            /this/is
            /this/is/sample
            /this/is/sample/path
    */
    DirsList splitPath() const;

    /*
        Breaks held path at every separator and returns resulting list.
        i.e. path /this/is/sample/path will be broken into:
         this
         is
         sample
         path
    */
    FilesList getComponents() const;

    static Dir getCurrentDirectory();

private:
    // path
    std::wstring myself;

    void performDirectoryPathFixup();

    bool mkdir_impl() const;
};
