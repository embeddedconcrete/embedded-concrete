#ifndef SHARED_CMDLINE_H
#define SHARED_CMDLINE_H

#include "strutils.h"
#include "stringlist.h"

#include <list>
#include <string>
#include <vector>
#include <functional>
#include <type_traits>

//callback to be called when processing values for CmdOpt
typedef std::function<bool(StringList&&)> OptionHandler;

template <typename T>
struct AssigmentHandler
{
    inline AssigmentHandler(T& t) : ref(t)
    {}

    T& ref;

    bool operator()(StringList&& values)
    {
        return tryParse(values.front(), ref);
    };
};

template <typename T>
struct AssigmentHandler<optional<T>>
{
    inline AssigmentHandler(optional<T>& t) : ref(t)
    {}

    optional<T>& ref;

    bool operator()(StringList&& values)
    {
        auto ret = AssigmentHandler<T>{ *ref.ptr() }.operator()(std::move(values));
        if (ret)
            ref.set_has_value();
        return ret;
    }
};

template <>
struct AssigmentHandler<bool>
{
    inline AssigmentHandler(bool& t) : ref(t)
    {}

    bool& ref;

    bool operator()(StringList&& values)
    {
        if (values.empty())
        {
            ref = true;
            return true;
        }

        return tryParse(values.front(), ref);
    }
};

template <>
struct AssigmentHandler<std::string>
{
    inline AssigmentHandler(std::string& str) : ref(str)
    {}

    std::string& ref;

    bool operator()(StringList&& values)
    {
        ref = std::move(values.front());
        return true;
    }
};

#define CMDLINE_ASSIGMENT_HANDLER(variable) \
    AssigmentHandler<decltype(variable)>(variable)

struct CmdOpt;

class CmdLine
{
    friend struct CmdOpt;

public:
    CmdLine();
    ~CmdLine();

    void parse(char* args[], unsigned int count);
    void parse(std::string args);
    void parse(StringList args);


    void setDefaultOptionHandler(OptionHandler const& val)
    {
        default_handler = val;
    }

    static CmdLine& singleton();

private:
    std::list<CmdOpt*> opts;
    OptionHandler default_handler;

    void _breakString(std::string str, StringList& list);

    void _preParse(StringList& list);
    void _handle(CmdOpt* opt, StringList values);
    StringList _loadFromFile(std::string const& filename);

    void _registerOpt(CmdOpt* opt)
    {
        opts.push_back(opt);
    }
    void _unregisterOpt(CmdOpt* opt)
    {
        opts.remove(opt);
    }
};

struct CmdOpt
{
    CmdOpt()
    {
        CmdLine::singleton()._registerOpt(this);
    }
    CmdOpt(StringList const& names, std::string&& desc, unsigned int min_args, unsigned int max_args, OptionHandler&& hnd)
        :names(names)
    {
        desc = desc; min_args = min_args; max_args = max_args; handler = hnd;
        CmdLine::singleton()._registerOpt(this);
    }
    ~CmdOpt()
    {
        CmdLine::singleton()._unregisterOpt(this);
    }

    StringList names = StringList();
    OptionHandler handler = [](StringList&&) -> bool { throw std::runtime_error("CmdOpt's handler called but not set!");  return false; };
    std::string desc = "";
    unsigned int min_args = 0;
    unsigned int max_args = 0;
};

#endif //SHARED_CMDLINE_H
