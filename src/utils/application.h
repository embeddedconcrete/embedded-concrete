#pragma once

#include "singleton.h"

class Application : public Singleton<Application>
{
public:
    Application() = default;

    void terminate();
};

#define sApp Application::singleton()
