#pragma once

#include "mtlog.h"
#include <type_traits>
#include <iostream>

class ApplicationStep
{
public:
    virtual bool run() = 0;
    virtual void clean() = 0;
};

namespace details {

    template <unsigned int StepIdx, typename... Steps>
    class ApplicationControllerImpl
    {};

    template <unsigned int StepIdx, typename Step, typename... Rest>
    class ApplicationControllerImpl<StepIdx, Step, Rest...> : public ApplicationControllerImpl<StepIdx+1, Rest...>
    {
        typedef ApplicationControllerImpl<StepIdx + 1, Rest...> Parent;

    public:
        ApplicationControllerImpl(Step&& s, Rest&&... rest) : Parent(std::forward<Rest>(rest)...), _mystep(std::forward<Step>(s))
        {}

        bool run()
        {
            bool result = false;
            sLog.log("Application controller: starting step ", StepIdx);
            try {
                result = _mystep.run();
                if (!result)
                    sLog.log("Application controller: step ", StepIdx, " failed!");
                else
                    Parent::run();

                sLog.log("Application controller: cleaning after step: ", StepIdx);
                _mystep.clean();
            }
            catch (std::exception const& e) {
                sLog.log("Application controller: unhandled std::exception occured while running step: ", StepIdx, ". Error message: ", e.what());
            }
            catch (...) {
                sLog.log("Application controller: unhandled unknown exception occured while running step: ", StepIdx);
            }

            return result;
        }

    private:
        std::remove_reference_t<Step> _mystep;
    };

    template <unsigned int StepIdx>
    class ApplicationControllerImpl<StepIdx>
    {
    public:
        ApplicationControllerImpl() {}

        bool run() { sLog.log("Application controller: all ", StepIdx, " steps succeeded. Application is going to close now."); return true; }
    };
}

template <typename... Steps>
class ApplicationController : public details::ApplicationControllerImpl<0, Steps...>
{
    typedef details::ApplicationControllerImpl<0, Steps...> Parent;
public:
    ApplicationController(Steps&&... steps) : Parent(std::forward<Steps>(steps)...)
    {}

    bool run() { return Parent::run(); }
};

template <typename... Steps>
ApplicationController<Steps...> makeAppController(Steps&&... steps)
{
    return ApplicationController<Steps...>(std::forward<Steps>(steps)...);
}
