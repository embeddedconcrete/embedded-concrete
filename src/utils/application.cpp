#include "application.h"
#include "mtlog.h"
#include <cassert>

void Application::terminate()
{
    sLog.close(true); //make sure all pending informations are flushed
    assert(false && "Application terminates due to fatal error which occured just by!\nPlease take a look at the log for more informations.");
    exit(-1);
}
