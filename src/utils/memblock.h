#pragma once

#include <functional>
#include <stdexcept>
#include <new>
#include <cassert>
#include <memory>
#include <algorithm>
#include <cstring>

#include "meta.h"
#include "memlen.h"

namespace details {
    enum mem_access {
        readonly,
        readwrite
    };
    template <class Impl> class mem_impl;
    class mem_ro;
    class mem_rw;
}

using memblock = ::details::mem_ro;
using storage = ::details::mem_rw;

namespace details {
    template <class Impl>
    struct mem_traits;

    template <> struct mem_traits<memblock> { using VoidT = const void; using AccessT = const char; };
    template <> struct mem_traits<storage> { using VoidT = void; using AccessT = char; };

    template <class Impl>
    class mem_impl
    {
    protected:
        friend class mem_rw;

        using VoidT = typename mem_traits<Impl>::VoidT;
        using AccessT = typename mem_traits<Impl>::AccessT;
        using DtorT = std::function<void(std::add_const_t<VoidT>*)>;

        mem_impl(VoidT* mem, memlen_t memlen, DtorT dealloc) : ptr(mem), len(memlen), dtor(dealloc)
        {}

        mem_impl(VoidT* mem, memlen_t memlen, DtorT dealloc, memlen_t) : ptr(mem), len(memlen), dtor(dealloc)
        {}

        void abandon()
        {
            dtor = nullptr;
            ptr = nullptr;
            len = 0;
        }

    public:
        struct empty_t {};
        static constexpr empty_t empty=empty_t();

    public:
        mem_impl(empty_t) : mem_impl(nullptr, 0, nullptr) {}

        mem_impl(mem_impl&& block) : ptr(block.ptr), len(block.len), dtor(std::move(block.dtor))
        {
            block.abandon();
        }

        mem_impl() = delete;
        mem_impl(mem_impl const&) = delete;

        ~mem_impl()
        {
            if (dtor) {
                dtor(ptr);
                abandon();
            }
        }

        auto begin() const { return static_cast<AccessT*>(ptr); }
        auto end() const { return static_cast<AccessT*>(ptr) + reinterpret_cast<const Impl*>(this)->size(); }
        auto data() const { return begin(); }

        auto capacity() const { return len; }
        auto available() const { return capacity() - reinterpret_cast<const Impl*>(this)->size(); }
        auto ownsmemory() const { return dtor != nullptr; }

        auto subblock(memlen_t off) const { return Impl{ (AccessT*)ptr+off, len-off, nullptr, len-off }; }
        auto subblock(memlen_t off, memlen_t elems) const { return Impl{ (AccessT*)ptr + off, std::min(len - off, elems), nullptr, std::min(len - off, elems) }; }

        operator bool() const { return ptr != nullptr; }

    public:
        template <class T>
        static std::conditional_t<std::is_const<T>::value || std::is_same<Impl, memblock>::value, memblock, storage> manage(T* mem, memlen_t count = 1, DtorT dealloc = nullptr);

        template <class T>
        static auto readwrite(T mem, memlen_t count = 1)
            -> std::enable_if_t<
                std::is_pointer<T>::value &&
                !std::is_const<std::remove_pointer_t<T>>::value &&
                !std::is_void<std::remove_pointer_t<T>>::value, storage>;

        template <class T, size_t N>
        static std::enable_if_t<!std::is_const<T>::value, storage> readwrite(T (&mem)[N])
        {
            return readwrite(&mem[0], N);
        }

        template <class T>
        static auto readonly(T mem, memlen_t count = 1)
            -> std::enable_if_t<
                std::is_pointer<T>::value &&
                !std::is_void<std::remove_pointer_t<T>>::value, memblock>;

        template <class T, size_t N>
        static memblock readonly(T (&mem)[N]);

        static storage manage(void* mem, memlen_t count, DtorT dealloc);
        static memblock manage(const void* mem, memlen_t count, DtorT dealloc);
        static storage readwrite(void* mem, memlen_t bytes);
        static memblock readonly(const void* mem, memlen_t bytes);

    protected:
        VoidT* ptr;
        memlen_t len;
        DtorT dtor;
    };

    class mem_ro : public mem_impl<mem_ro>
    {
        using parent = mem_impl<mem_ro>;
        friend class mem_impl<mem_ro>;
        friend class mem_rw;

    public:
        using parent::parent;

        mem_ro(mem_ro&&) = default;
        mem_ro(storage&& s);
        mem_ro(storage const& s);

        mem_ro& operator =(mem_ro&& mem);
        mem_ro& operator =(storage&& s);

        auto size() const { return parent::len; }
    };

    class mem_rw : public mem_impl<mem_rw>
    {
        using parent = mem_impl<mem_rw>;
        friend class mem_impl<mem_rw>;
        friend class mem_ro;

        mem_rw(VoidT* ptr, memlen_t len, DtorT dtor, memlen_t off) : parent(ptr, len, dtor), offset(off) {}

    public:
        mem_rw() : parent(empty) {}
        mem_rw(mem_rw&& rw);

        using parent::parent;

        auto size() const { return offset; }

        bool append(memblock const& mem, bool resize = false);
        bool override(memblock const& mem);

        mem_rw& operator =(mem_rw&& rw);

        /*
            Changes capacity of storage to 'count' elements of given type.
            This function always performs reallocation - even if required size is smaller than actual capacity.
            If copy is set to true, copies as many bytes from old storage as possible before deallocation.
            Size of the new storage is 0 if data hasn't been copied or std::min(newsize, oldsize) otherwise.
        */
        template <class T, class Allocator = std::allocator<T>>
        std::enable_if_t<!std::is_void<T>::value, void> reallocate(memlen_t count, bool copy, Allocator& a)
        {
            auto newptr = std::allocator_traits<Allocator>::allocate(a, count);
            if (copy && ptr)
                memcpy(newptr, ptr, std::min(count * sizeof(T), size()));
            else
                offset = 0;

            if (dtor)
                dtor(ptr);

            new (this) storage{ newptr, count * sizeof(T),[&a, count](std::add_const_t<VoidT>* ptr) {
                std::allocator_traits<Allocator>::deallocate(a, static_cast<typename Allocator::pointer>(const_cast<void*>(ptr)), count);
            }, std::min(count * sizeof(T), size()) };
        }

        template <class T, class Allocator = std::allocator<T>>
        std::enable_if_t<!std::is_void<T>::value, void> reallocate(memlen_t count, bool copy, Allocator&& a)
        {
            auto newptr = std::allocator_traits<Allocator>::allocate(a, count);
            if (copy && ptr)
                memcpy(newptr, ptr, std::min(count * sizeof(T), size()));
            else
                offset = 0;

            if (dtor)
                dtor(ptr);

            new (this) storage{ newptr, count * sizeof(T), [a=std::move(a), count](std::add_const_t<VoidT>* ptr) mutable {
                std::allocator_traits<Allocator>::deallocate(a, static_cast<typename Allocator::pointer>(const_cast<void*>(ptr)), count);
            }, std::min(count * sizeof(T), size()) };
        }

        template <class T, class Allocator = std::allocator<T>>
        std::enable_if_t<!std::is_void<T>::value, void> reallocate(memlen_t count = 1, bool copy = true) {
            Allocator a;
            return reallocate<T>(count, copy, std::move(a));
        }
        /*
            Guarantess that storage will have enought place to store 'count' more elements of given type.
            Reallocates with copy if neccessary.
            Does not modify current size of storage.
        */
        template <class T, class Allocator = std::allocator<T>>
        std::enable_if_t<!std::is_void<T>::value, void> enlarge(memlen_t count)
        {
            if (size() + count > capacity())
                reallocate<T>(size() + count, true);
        }

        void clear() { offset = 0; }

        void update_size(memlen_t newbytes)
        {
            if (size() + newbytes > capacity())
                throw std::runtime_error("Updates storage size outside of storage boundary!");

            offset += newbytes;
        }

        template <class T, class Allocator = std::allocator<T>>
        static std::enable_if_t<!std::is_void<T>::value, storage> allocate(memlen_t count, Allocator& a)
        {
            return storage{ std::allocator_traits<Allocator>::allocate(a, count), count * sizeof(T), [&a, count](std::add_const_t<VoidT>* ptr) {
                std::allocator_traits<Allocator>::deallocate(a, static_cast<typename Allocator::pointer>(const_cast<void*>(ptr)), count);
            } };
        }
        template <class T, class Allocator = std::allocator<T>>
        static std::enable_if_t<!std::is_void<T>::value, storage> allocate(memlen_t count, Allocator&& a)
        {
            return storage{ std::allocator_traits<Allocator>::allocate(a, count), count * sizeof(T), [a=std::move(a), count](std::add_const_t<VoidT>* ptr) mutable {
                std::allocator_traits<Allocator>::deallocate(a, static_cast<typename Allocator::pointer>(const_cast<void*>(ptr)), count);
            } };
        }

        template <class T, class Allocator = std::allocator<T>>
        static std::enable_if_t<!std::is_void<T>::value, storage> allocate(memlen_t count = 1)
        {
            Allocator a;
            return allocate<T>(count, std::move(a));
        }

        template <class Container>
        static std::enable_if_t<std::is_convertible<typename Container::reference, memblock const&>::value, storage> join(Container const& c)
        {
            memlen_t total_size = 0;
            for (auto const& block : c)
                total_size += static_cast<memblock const&>(block).size();

            auto store = storage::allocate<char>(total_size);
            for (auto const& block : c)
                store.append(static_cast<memblock const&>(block));

            return store;
        }

    private:
        memlen_t offset = 0;
    };

    template <class Impl> template <class T>
    std::conditional_t<std::is_const<T>::value || std::is_same<Impl, memblock>::value, memblock, storage> mem_impl<Impl>::manage(T* mem, memlen_t count, typename mem_impl<Impl>::DtorT dealloc)
    {
        if (!dealloc)
            dealloc = [count](VoidT* ptr) {
                if (count == 1)
                    delete static_cast<std::add_const_t<T>*>(ptr);
                else
                    delete[] static_cast<std::add_const_t<T>*>(ptr);
            };

        return std::conditional_t<std::is_const<T>::value || std::is_same<Impl, memblock>::value, memblock, storage>{ mem, count * sizeof(T), dealloc, count * sizeof(T) };
    }

    template <class Impl> template <class T>
    auto mem_impl<Impl>::readwrite(T mem, memlen_t count)
        -> std::enable_if_t<std::is_pointer<T>::value && !std::is_const<std::remove_pointer_t<T>>::value && !std::is_void<std::remove_pointer_t<T>>::value, storage>
    {
        return storage{ mem, count * sizeof(std::remove_pointer_t<T>), nullptr, count * sizeof(std::remove_pointer_t<T>) };
    }

    template <class Impl> template <class T>
    auto mem_impl<Impl>::readonly(T mem, memlen_t count)
        -> std::enable_if_t<std::is_pointer<T>::value && !std::is_void<std::remove_pointer_t<T>>::value, memblock>
    {
        return memblock{ mem, count * sizeof(std::remove_pointer_t<T>), nullptr };
    }

    template <class Impl>
    storage mem_impl<Impl>::manage(void* mem, memlen_t count, typename mem_impl<Impl>::DtorT dealloc)
    {
        if (!dealloc)
            throw std::logic_error("Managed void pointer without dealloc function!");

        return storage{ mem, count, dealloc, count };
    }

    template <class Impl>
    memblock mem_impl<Impl>::manage(const void* mem, memlen_t count, typename mem_impl<Impl>::DtorT dealloc)
    {
        if (!dealloc)
            throw std::logic_error("Managed void pointer without dealloc function!");

        return memblock{ mem, count, dealloc };
    }

    template <class Impl>
    storage mem_impl<Impl>::readwrite(void* mem, memlen_t bytes)
    {
        return storage{ mem, bytes, nullptr, bytes };
    }

    template <class Impl>
    memblock mem_impl<Impl>::readonly(const void* mem, memlen_t bytes)
    {
        return memblock{ mem, bytes, nullptr };
    }

    template<class Impl> template <class T, size_t N>
    memblock mem_impl<Impl>::readonly(T (&mem)[N])
    {
        return readonly(&mem[0], N * sizeof(T));
    }
}
