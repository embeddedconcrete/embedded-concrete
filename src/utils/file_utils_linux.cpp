#ifdef LINUX
#include "file_utils.h"
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

bool file_utils::exists(std::string const& path)
{
    return access(path.c_str(), F_OK) != -1;
}

file_utils::file_type file_utils::type(std::string const& path)
{
    struct stat buffer;
    if (stat(path.c_str(), &buffer) == -1) {
        return invalid;
    } else if (S_ISDIR(buffer.st_mode)) {
        return directory;
    } else if (S_ISSOCK(buffer.st_mode)) {
        return socket;
    } else if (buffer.st_mode & S_IXUSR) {
        return executable;
    } else {
        return regular;
    }
}

bool file_utils::can_access(std::string const& path, access_mode mode)
{
    int flags = 0;
    if (mode & read) {
        flags |= R_OK;
    }
    if (mode & write) {
        flags |= W_OK;
    }
    if (mode & execute) {
        flags |= X_OK;
    }
    return access(path.c_str(), flags) != -1;
}
#endif