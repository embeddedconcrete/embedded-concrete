#include "memblock.h"
#include "optional.h"
using namespace details;
using namespace std;

mem_ro::mem_ro(storage&& s) : mem_ro::parent{ mem_ro::parent::empty }
{
    if (s.size()) {
        parent::ptr = s.ptr;
        parent::len = s.size();
        parent::dtor = move(s.dtor);
        s.clear();
        s.abandon();
    }
}

mem_ro::mem_ro(storage const& s) : mem_ro::parent{ mem_ro::parent::empty }
{
    if (s.size()) {
        parent::ptr = s.ptr;
        parent::len = s.size();
    }
}

mem_ro & details::mem_ro::operator=(mem_ro && mem)
{
//    parent::~mem_impl();
//	parent::~mem_impl();
	details::destroy(*this);
    new (this) mem_ro(move(mem));
    return *this;
}

mem_ro & details::mem_ro::operator=(storage && s)
{
	details::destroy(*this);
//    parent::~mem_impl();
    new (this) mem_ro(move(s));
    return *this;
}

details::mem_rw::mem_rw(mem_rw && rw) : mem_rw::parent{ mem_rw::parent::empty }
{
    parent::ptr = rw.ptr;
    parent::len = rw.len;
    parent::dtor = move(rw.dtor);
    offset = rw.offset;

    rw.clear();
    rw.abandon();
}

bool mem_rw::append(memblock const& mem, bool resize)
{
    if (!mem.data())
        return true;

    if (available() < mem.size())
    {
        if (!resize)
            return false;
        else
            enlarge<char>(mem.size());
    }

    memcpy(end(), mem.data(), mem.size());
    offset += mem.size();
    return true;
}

bool mem_rw::override(memblock const& mem)
{
    if (capacity() < mem.size())
        return false;

    memcpy(begin(), mem.data(), mem.size());
    offset = mem.size();
    return true;
}

mem_rw & details::mem_rw::operator=(mem_rw && rw)
{
//    parent::~mem_impl();
	details::destroy(*this);
    new (this) mem_rw(move(rw));
    return *this;
}
