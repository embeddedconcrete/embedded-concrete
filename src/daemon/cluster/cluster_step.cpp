#include "cluster_step.h"
#include "cluster.h"
#include "request.h"
#include "file_utils.h"
#include "config/config.h"
#include "process_wrapper.h"
#include "network/network_step.h"

#include "root_job.h"
#include "worker_job.h"

#include <thread>
#include <chrono>
#include <iostream>
#include <fstream>
#include <string>

using namespace ec;
using namespace std;

ClusterStep* ClusterStep::instance = nullptr;

namespace {
    enum decline_reason : uint8
    {
        out_of_resources,
        executable_not_found,
        invalid_executable,
        cannot_run,
        output_refused,
        cluster_id_conflict,
        internal_error
    };
}

ClusterStep::ClusterStep()
{}

ClusterStep::ClusterStep(ClusterStep &&)
{}

ClusterStep::~ClusterStep()
{}

bool ClusterStep::run()
{
    if (instance != nullptr)
        throw runtime_error("Multiple ClusterStep instances!");
    instance = this;

    running = true;
    start_point = chrono::system_clock::now();
    sLog.log("Starting cluster daemon at ", start_point);

    thread* dbgThread = nullptr;
    atomic_bool dbgThreadRunning = {false};
    if (globalConfig.debugEnabled.get())
        dbgThread = new thread([this, &dbgThreadRunning]() {
            sLog.log("Starting debug output thread...");
            DebugOutput out;

            dbgThreadRunning = true;
            while (dbgThreadRunning)
            {
                if (!debtosend.empty())
                {
                    auto debs = debtosend.fetchAll();
                    for (auto const& deb : debs)
                    {
                        auto itr = debsubs.find(deb.getHeader().cluster);
                        if (itr == debsubs.end())
                            getWorkerJob(deb.getHeader().getSrcUid())->stopDebug();
                        else
                            for (auto const& sub : itr->second)
                                out.send(sub, deb);
                    }
                }
                else
                    this_thread::sleep_for(chrono::milliseconds{ 100 });
            }
        });

    single_run = globalConfig.run_cluster.has_value();
    if (single_run)
        sLog.log("Enabling single-run mode with provided cluster description file: ", globalConfig.run_cluster.get());

    atomic_bool iothreadrun = {false};
    thread* iothread = nullptr;
    if (!single_run)
        iothread = new thread([this, &iothreadrun]() {
            sLog.log("[DEBUG] Enabling keyboard input");
            iothreadrun = true;
            while (iothreadrun)
            {
                try {
                    string str;

                    sLog.cout(string{ "> " }, false);
                    getline(cin, str);

                    if (str == "exit")
                    {
                        running = false;
                        iothreadrun = false;
                    }
                    else if (!str.empty())
                    {
                        packets::StartCluster start{ str };
                        ControlClient cli;
                        cli.connect(NetworkStep::getInstance().getLocalHost());
                        cli.send(start);
                        cli.disconnect(NetworkStep::getInstance().getLocalHost());
                    }
                }
                catch (...) {
                    sLog.log("[DEBUG] Exception occured while processing keyboard input");
                }
            }
        });

    auto* mainSocket = NetworkStep::getInstance().getControllSocket();
    if (single_run)
    {
        mainSocket->setTimeout(5);
        ControlClient{}.signal(NetworkStep::getInstance().getLocalHost(), packets::StartCluster{ globalConfig.run_cluster.get() });
    }

    while (running)
    {
        auto result = mainSocket->recv<memblock>();
        if (!result)
        {
            if (single_run && result.has_error() && result.get_error() == EAGAIN)
                continue;

            if (result.has_error())
            {
                sLog.log("Error occured while waiting for control packet, error code: ", result.get_error(), "( ", result.descr(), ")");
                break;
            } else {
                sLog.log("No error occured, but no data has been recieved WTF?");
            }
        }
        else
        {
            auto& req = result.get();
            auto header = req.deserializePart<PacketHeader>();
            if (header.dst == 0)
            {
                auto OC = req.deserializePart<OpCode>();
                handleReq(OC, req);
            }
            else
                routeRequest(header, req);
        }
    }

    auto diff = chrono::system_clock::now() - start_point;
    sLog.log("Cluster daemon released after ", chrono::duration_cast<chrono::seconds>(diff).count(), " seconds");

    if (iothread)
    {
        if (iothreadrun)
            sLog.log("[DEBUG] Press enter to exit");
        iothreadrun = false;
        iothread->join();
        delete iothread;
        iothread = nullptr;
    }

    sLog.log("Shutting down ec-deamon");
    if (dbgThread)
    {
        dbgThreadRunning = false;
        dbgThread->join();
        delete dbgThread;
        dbgThread = nullptr;
    }
    return true;
}

void ClusterStep::clean()
{
    sLog.log("Closing all connections");
    //todo?

    instance = nullptr;
}

void ClusterStep::enqueueDebugData(packets::DebugInfo && dbg)
{
    if (globalConfig.debugEnabled.get())
        debtosend.push(std::move(dbg));
}

void ClusterStep::removeJobHnd(Job * hnd)
{
    lock_guard<mutex> _(jobs_lock);
    jobs.erase(hnd->getUid());
    if (hnd->getWorkerId() != 0)
        decWorkersCountForCluster(hnd->getClusterId());

    delete hnd;

    if (single_run && jobs.empty())
        running = false;
}

void ClusterStep::addJobHnd(Job * hnd)
{
    lock_guard<mutex> _(jobs_lock);
    jobs.insert(make_pair(hnd->getUid(), hnd));
    if (hnd->getWorkerId() != 0)
    {
        auto cluster = hnd->getClusterId();
        if (getWorkersCountForCluster(cluster))
            incWorkersCountForCluster(cluster);
        else
            registerClusterId(cluster, reinterpret_cast<WorkerJob*>(hnd)->getRootAddr());
    }
}

WorkerJob * ClusterStep::getWorkerJob(ec::uid_t entity) const
{
    if (EC_GET_WORKER(entity) == 0)
        return nullptr;

    auto itr = jobs.find(entity);
    return (itr == jobs.end() ? nullptr : reinterpret_cast<WorkerJob*>(itr->second));
}

RootJob * ClusterStep::getRootJob(ec::cluster_id_t cluster) const
{
    auto itr = jobs.find(EC_GET_UID(cluster, 0));
    if (itr == jobs.end())
        return nullptr;

    return reinterpret_cast<RootJob*>(itr->second);
}

std::string ClusterStep::getRootForCluster(ec::cluster_id_t cluster)
{
    auto itr = clusters.find(cluster);
    if (itr == clusters.end())
        return "";

    return itr->second.first;
}

void ClusterStep::registerClusterId(ec::cluster_id_t cluster, std::string const & rootaddr)
{
    clusters[cluster] = make_pair(rootaddr, 1);
}

uint32 ClusterStep::getWorkersCountForCluster(ec::cluster_id_t cluster)
{
    auto itr = clusters.find(cluster);
    if (itr == clusters.end())
        return 0;

    return itr->second.second;
}

void ClusterStep::incWorkersCountForCluster(ec::cluster_id_t cluster)
{
    auto itr = clusters.find(cluster);
    if (itr == clusters.end())
        return;

    ++itr->second.second;
}

void ClusterStep::decWorkersCountForCluster(ec::cluster_id_t cluster)
{
    auto itr = clusters.find(cluster);
    if (itr == clusters.end())
        return;

    --itr->second.second;
    if (!itr->second.second)
        clusters.erase(itr);
}

list<WorkerJob*> ClusterStep::getWorkersForCluster(ec::cluster_id_t cluster) const
{
    list<WorkerJob*> ret;
    auto itr = jobs.lower_bound(EC_GET_UID(cluster, 0));
    while (itr != jobs.end() && EC_GET_CLUSTER(itr->first) == cluster && EC_GET_WORKER(itr->first) != 0)
        ret.push_back(reinterpret_cast<WorkerJob*>(itr->second));

    return ret;
}

void ClusterStep::handleReq(OpCode OC, Request<memblock>& req)
{
    sLog.log("Handling new request");
    switch (OC)
    {
    case OpCode::HelpRequest: return handleHelpRequest(req.deserialize<packets::HelpRequest>());
    case OpCode::StartCluster: 
        if (!handleStartCluster(req.deserialize<packets::StartCluster>()) && single_run)
            running = false;
        break;

    // Debug info
    case OpCode::DebugListRootsReq: return handleDbgListRoots(req.deserialize<packets::DebugListRootsReq>());

    default:
        routeRequest(req.deserializePart<PacketHeader>(0), req);
        break;
    }
}

void ClusterStep::handleHelpRequest(Request<packets::HelpRequest>&& req)
{
    auto rootaddr = makestr("tcp://", req.getPeerAddress(), ":", req.rootport());

    sLog.log("Received help request from ", rootaddr);
    auto root = getRootForCluster(req.getHeader().cluster);
    if (!root.empty() && root != rootaddr)
    {
        sLog.log("New cluster id ", req.getHeader().cluster, " already associated with root node at address ", root);
        return (void)req.reply(packets::Decline{ decline_reason::cluster_id_conflict });
    }

    if (!globalConfig.maxWorkers.has_value() || jobs.size() >= globalConfig.maxWorkers.get())
    {
        sLog.log("Already running maximum number of ", globalConfig.maxWorkers.get(), " workers");
        return (void)req.reply(packets::Decline{ decline_reason::out_of_resources });
    }

    auto filename = req.programName();

#ifdef WINDOWS
    if (!endswith(filename, ".exe") && !file_utils::exists(filename))
    {
        sLog.log("Could not find application file: ", filename, ", trying to add .exe");
        filename += ".exe";
    }
#endif
    if (!file_utils::exists(filename))
    {
        sLog.log("Requested app name ", filename, " does not exists");
        return (void)req.reply(packets::Decline{ decline_reason::executable_not_found });
    }
    if (file_utils::type(filename) != file_utils::executable)
    {
        sLog.log("Requested file ", filename, " is not binary executable");
        return (void)req.reply(packets::Decline{ decline_reason::invalid_executable });
    }
    if (!file_utils::can_access(filename, file_utils::execute))
    {
        sLog.log("Does not have permission to run requested file ", filename);
        return (void)req.reply(packets::Decline{ decline_reason::cannot_run });
    }

    DataInputTcp * in = nullptr;
    string inputaddr = "";
    if (!req.inputaddr().empty())
    {
        try {
            in = new DataInputTcp(req.inputaddr());
        }
        catch (runtime_error& error)
        {
            sLog.log("Could not bind output socket for new worker, requsted address ", req.inputaddr(), ", error: ", error.what());
            return (void)req.reply(packets::Decline{ decline_reason::output_refused });
        }

        assert(in->getBoundAddrs().size() == 1 && "Could not receive bounded address");
        if (in->getBoundAddrs().size() == 1)
            inputaddr = in->getBoundAddrs().front();
        else
        {
            delete in;
            sLog.log("Unexpected count of bounded addresses while creating worker output socket ", in->getBoundAddrs().size());
            return (void)req.reply(packets::Decline{ decline_reason::internal_error });
        }
    }

    sLog.log("Accepting help request:\n    root addr: ", rootaddr, "\n    cluster id: ", req.getHeader().cluster, "\n    worker: ", req.newWorkerId(), "\n    application name: ", filename, "\n    input socket: ", (in ? join(in->getBoundAddrs(), ' ') : "(none)"));
    WorkerJob* wj = new WorkerJob{ rootaddr, req.getHeader().cluster, req.newWorkerId(), process_wrapper(filename)};
    addJobHnd(wj);
    wj->setInputSocket(in);
	wj->run();
    req.reply(packets::Acknowledge{ inputaddr });
}

bool ClusterStep::handleStartCluster(Request<packets::StartCluster>&& req)
{
    sLog.log("Trying to start new cluster...");
    if (!file_utils::exists(req.clusterDescFile()))
        return (void)sLog.log("Could not find requested cluster description: ", req.clusterDescFile()), false;

    if (!file_utils::can_access(req.clusterDescFile(), file_utils::read))
        return (void)sLog.log("Could not read cluster description from file: ", req.clusterDescFile()), false;

    ifstream in{ req.clusterDescFile(), ios::in | ios::binary };
    if (!in.is_open())
        return (void)sLog.log("Could not open ifstream for file: ", req.clusterDescFile()), false;

    Cluster::Description desc;
    if (!Cluster::Description::parse(in, desc, req.clusterDescFile()))
        return (void)sLog.log("Parsing provided file failed, cluster will not be created"), false;

    Cluster* cluster = new Cluster{ NetworkStep::getInstance().getInputPort() };
    RootJob* rjob = new RootJob{ cluster };
    addJobHnd(rjob);
    rjob->run(move(desc));
    return true;
}

void ClusterStep::handleDbgListRoots(ec::Request<ec::packets::DebugListRootsReq>&& req)
{
    packets::DebugListRootsRep rep;
    for (auto const& job : jobs)
        if (EC_GET_WORKER(job.first) == 0)
            rep.clusters().push_back(EC_GET_CLUSTER(job.first));

    //for debug only, since creating new clusters may not work currently
    rep.clusters().push_back(cluster_id_t(12));
    rep.clusters().push_back(cluster_id_t(122));

    req.reply(move(rep));
}

void ClusterStep::routeRequest(ec::PacketHeader const& header, ec::Request<memblock>& req)
{
    auto OC = req.deserializePart<OpCode>();

    auto job = jobs.find(header.getDstUid());
    if (job == jobs.end())
        return (void)sLog.log("Unknown entity addressed in packet's header: ", header.getDstUid(), " - packet ", nameForOpCode(OC), " (", static_cast<int>(OC),  ") will be dropped");

    job->second->enqueueRequest(OC, move(req));
}
