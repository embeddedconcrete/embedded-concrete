#pragma once

#include "applicationcontroller.h"
#include "packets.h"
#include "request.h"

#include <map>
#include <mutex>
#include <istream>

class Job;
class RootJob;
class WorkerJob;

class ClusterStep : public ApplicationStep
{
public:
    ClusterStep();
    ClusterStep(ClusterStep&&);
    ~ClusterStep();

    bool run() override;
    void clean() override;

    void enqueueDebugData(ec::packets::DebugInfo&& dbg);
    void removeJobHnd(Job* hnd);
    void addJobHnd(Job* hnd);

    static ClusterStep& getInstance() { return *instance; }

private:
    static ClusterStep* instance;
    std::atomic_bool running;

    std::map<ec::uid_t, Job*> jobs;
    std::map<ec::cluster_id_t, std::pair<std::string, uint32>> clusters;
    std::mutex jobs_lock;

    NlQueue<ec::packets::DebugInfo> debtosend;
    std::map<ec::cluster_id_t, std::list<std::string>> debsubs;

    bool single_run = false;
    std::chrono::system_clock::time_point start_point;

    WorkerJob* getWorkerJob(ec::uid_t entity) const;
    RootJob* getRootJob(ec::cluster_id_t cluster) const;
    std::list<WorkerJob*> getWorkersForCluster(ec::cluster_id_t cluster) const;

    std::string getRootForCluster(ec::cluster_id_t cluster);
    void registerClusterId(ec::cluster_id_t cluster, std::string const& rootaddr);
    uint32 getWorkersCountForCluster(ec::cluster_id_t cluster);
    void incWorkersCountForCluster(ec::cluster_id_t cluster);
    void decWorkersCountForCluster(ec::cluster_id_t cluster);

private:
    void handleReq(ec::OpCode OC, ec::Request<memblock>& req);
    void handleHelpRequest(ec::Request<ec::packets::HelpRequest>&& req);
    bool handleStartCluster(ec::Request<ec::packets::StartCluster>&& req);

    void handleDbgListRoots(ec::Request<ec::packets::DebugListRootsReq>&& req);

    void routeRequest(ec::PacketHeader const& header, ec::Request<memblock>& req);
};
