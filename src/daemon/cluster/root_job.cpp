#include "root_job.h"
#include "mtlog.h"
#include "network/network_step.h"
using namespace ec;
using namespace std;

RootJob::RootJob(Cluster* mycluster) : parent(EC_GET_UID(mycluster->getId(), 0)), cluster{ mycluster }
{
}

RootJob::~RootJob()
{
    end(true);
    delete cluster;
    cluster = nullptr;
}

bool RootJob::init(ec::Cluster::Description&& desc)
{
    if (!cluster->acquire(desc, NetworkStep::getInstance().performNetworkDiscovery()))
        return (void)sLog.log("Cluster creation failed"), false;

    sLog.log("Cluster acquire succeeded - sending initial configuration to workers");
    cluster->configureWorkers();
    return true;
}

void RootJob::handleRequest(ec::OpCode OC, ec::Request<memblock>& req)
{
    switch (OC)
    {
    case OpCode::WorkIsDone: return handleWorkIsDone(req.deserialize<packets::WorkIsDone>());
    case OpCode::DynamicWorker: return handleDynamicWorker(req.deserialize<packets::DynamicWorker>());
    case OpCode::WorkerDetach: return handleWorkerDetach(req.deserialize<packets::WorkerDetach>());
    case OpCode::DebugListWorkersReq: return handleDbgListWorkers(req.deserialize<packets::DebugListWorkersReq>());
    default:
        sLog.log("Unexpected packet opcode passed to root ", getIdentifier(), ": ", nameForOpCode(OC));
        break;
    }
}

void RootJob::handleWorkIsDone(ec::Request<ec::packets::WorkIsDone>&& req)
{
    auto worker = getCluster()->getWorker(req.getHeader().src);
    if (!worker)
        return sLog.log("Root ", getIdentifier(), ": unknown worker addressed as WorkIsDone source: ", req.getHeader().src);

    if (worker->hasDone())
        return sLog.log("Root ", getIdentifier(), ": received WorkIsDone packet from worker ", worker->getId(), " but worked is already marked as finished");

    sLog.log("Root ", getIdentifier(), ": received WorkIsDone packet from worker ", worker->getId(), " - sending dismantle message");
    worker->markAsDone();
    getCluster()->sendDismantle(worker);

    if (!worker->getOutputs().empty())
    {
        sLog.log("Root ", getIdentifier(), ": validating inputs after worker ", worker->getId(), " dismantling");
        //process all output nodes and check if they still have valid input nodes, if no send them EndOfInput
        for (auto next : worker->getOutputs())
        {
            bool has_input = false;
            for (auto prev : next->getInputs())
            {
                if (prev->hasDone())
                    continue;

                has_input = true;
                break;
            }

            if (has_input)
                continue;

            sLog.log("Root ", getIdentifier(), ": worker ", next->getId(), " doesn't have any more running input nodes, signalling EndOfInput packet");
            getCluster()->getContext().send(packets::EndOfInput{}, *next);
        }
    }
    else
    {
        sLog.log("Root ", getIdentifier(), ": dismantled worker ", worker->getId(), " is a terminator node, checking if cluster is still working...");
        if (getCluster()->allDone())
        {
            sLog.log("Root ", getIdentifier(), ": all workers has been dismantled, cleaning cluster and exiting job");
            getCluster()->cleanup();
            end_req = true;
        }
    }

}

void RootJob::handleDynamicWorker(ec::Request<ec::packets::DynamicWorker>&& req)
{
    if (getCluster()->getState() != Cluster::acquired)
    {
        sLog.log("Root ", getIdentifier(), " requested to add dynamic worker to not-running cluster");
        //todo: reply status?
        return;
    }

    sLog.log("Root ", getIdentifier(), " handling new dynamic worker");
    sLog.log("Root ", getIdentifier(), " validating dynamic worker configuration");

    bool ok = true;

    list<Worker*> outputs;
    list<Worker*> inputs;

    const auto checkWorkersId = [this](list<worker_id_t> const& ids, list<Worker*>& workers, const char* type) -> bool {
        bool ret = true;
        for (auto id : ids)
        {
            auto node = getCluster()->getWorker(id);
            if (!node)
            {
                sLog.log("Root ", getIdentifier(), " dynamic worker has non-existing worker specified as ", type, " id: ", id);
                ret = false;
                continue;
            }

            workers.push_back(node);
        }

        return ret;
    };

    ok = ok && checkWorkersId(req.inputWorkers(), inputs, "input");
    ok = ok && checkWorkersId(req.outputWorkers(), outputs, "output");
    if (!ok)
    {
        //todo: send status?
        return;
    }

    auto wid = getCluster()->getNextWorkerId();
    sLog.log("Root ", getIdentifier(), " configuration valid, trying to allocate new worker with id: ", wid);
    
    ec::Worker::Description desc;
    desc.app = req.progName();
    desc.args = join(req.executableArgs(), ' ');
    desc.mode = static_cast<process_wrapper::output_mode>(req.transferMode());
    desc.packetSize = req.packetSize();
    desc.node = nullptr;
    desc.isDataSource = req.inputWorkers().empty();

    string inputaddr;
    if (!getCluster()->requestHelp(Node{ req.nodeAddress() }, &desc, wid, inputaddr))
    {
        sLog.log("Root ", getIdentifier(), " remote peer has rejected help request, allocating dynamic worker has failed");
        //todo: reply some status?
        return;
    }

    auto worker = getCluster()->getWorker(wid);
    assert(worker != nullptr && "Help request has been accepted but couldn't find worker?");

    for (auto output : outputs)
    {
        getCluster()->getContext().send(packets::TryOpenInputSocket{}, *output);
    }
    
    for (auto input : inputs)
    {
        getCluster()->getContext().send(packets::NewOutput{ inputaddr }, *input);
        input->addInput(worker);
    }

    
}

void RootJob::handleWorkerDetach(ec::Request<ec::packets::WorkerDetach>&& req)
{
}

void RootJob::handleDbgListWorkers(Request<packets::DebugListWorkersReq>&& req)
{
    packets::DebugListWorkersRep rep;

    for (auto const& worker : getCluster()->getWorkers())
    {
		// TODO send extra information about worker data port
        rep.workers().push_back(dbg_worker_info(dbg_worker_address(worker.second->getControlNode().getTcpAddress(), worker.first), list<dbg_worker_address>{}));
        for (auto const& out : worker.second->getOutputs())
            get<1>(rep.workers().back()).push_back(dbg_worker_address(out->getControlNode().getTcpAddress(), out->getId()));
    }

    req.reply(move(rep));
}
