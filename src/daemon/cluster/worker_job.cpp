#include "worker_job.h"
#include "cluster_step.h"
#include "mtlog.h"
using namespace ec;
using namespace std;

WorkerJob::WorkerJob(std::string const & rootaddr, cluster_id_t cid, worker_id_t wid, process_wrapper&& my_pw) : parent(EC_GET_UID(cid, wid)), pw(move(my_pw)), to_write(memblock::empty), root(rootaddr)
{
}

WorkerJob::~WorkerJob()
{
    end(true);
    stopChild();
    delete out;
    out = nullptr;
    delete in;
    in = nullptr;
}

void WorkerJob::setInputSocket(DataInputTcp* input)
{
	if(input)
    {
		if (input->getBoundAddrs().empty())
			sLog.log("Worker ", getIdentifier(), ": warning - requested to associate input port which is not bound to any endpoint");
		else
			sLog.log("Worker ", getIdentifier(), ": associating new input socket bound at address(es): ", join(input->getBoundAddrs(), ' '));
	}

    if (in)
    {
		if(!input)
			sLog.log("Worker ",getIdentifier(),": Setting new input socket to NULL");

        sLog.log("Worker ", getIdentifier(), " releasing previously assigned input socket");
        delete in;
        in = nullptr;
    }

    in = input;
}

void WorkerJob::handleRequest(OpCode OC, Request<memblock>& req)
{
    switch (OC)
    {
    case OpCode::Configuration: return handleConfigurationRequest(req.deserialize<packets::Configuration>());
    case OpCode::EndOfInput: return handleEndOfInput(req.deserialize<packets::EndOfInput>());
    case OpCode::ClusterDismantle: return handleClusterDismantle(req.deserialize<packets::ClusterDismantle>());
    default:
        sLog.log("Unexpected packet opcode at passed to worker ", getIdentifier(), ": ", nameForOpCode(OC));
        break;
    }
}

void WorkerJob::stopChild()
{
    if (!init)
        return;

    sLog.log("Worker ", getIdentifier(), ": terminating child process ", pw.getProgName());
    pw.terminate();
    init = false;
}

bool WorkerJob::job()
{
	if(!init)
		return false;

    bool done_anything = false;

    for (auto output : pw)
    {
        for (auto& data : output)
        {
            done_anything = true;
            if (out)
            {
                auto result = out->send(data);
                if (!result && result.has_error())
                    sLog.log("Worker ", getIdentifier(), ": error while sending network data: ", result.get_error(), " (", result.descr(), ")");
                else if (!result) {
                    sLog.log("Worker ", getIdentifier(), ": No data to send :E");
                }
                else if (debug)
                {
                    packets::DebugInfo info{ static_cast<uint32>(data.size()), static_cast<uint64>(time(NULL)) };
                    info.getHeader().cluster = getClusterId();
                    info.getHeader().src = getWorkerId();
                    info.getHeader().dst = 0;
                    ClusterStep::getInstance().enqueueDebugData(move(info));
                }
            }
        }
    }

    if (to_write)
    {
        done_anything = true;
        auto done = pw.write(to_write.subblock(writen));
        writen += done;
        if (writen == to_write.size())
        {
            to_write = memblock::empty;
            writen = 0;
        }
    }
    else if (in && poll_network)
    {
        auto recv = in->recv(false);
        if (!recv && recv.has_error())
            sLog.log("Worker ", getIdentifier(), ": error while receiving network data: ", recv.get_error(), " (", recv.descr(), ")");
        else if (recv)
        {
            done_anything = true;
            to_write = move(recv.get());
        }
        else if (input_end)
        {
            sLog.log("Worker ", getIdentifier(), " input is marked as exhausted and no more data remained in network buffer - resigning from network input and closing child process input");
            poll_network = false;
            pw.close_input();
        }
    }
    else if (!pw.is_running())
    {
        sLog.log("Worker ", getIdentifier(), " both child process and all input nodes have finished it work and no more data remained in outgoing buffer - notifying root about finished work");
        packets::WorkIsDone done{};
        done.getHeader().cluster = getClusterId();
        done.getHeader().src = getWorkerId();
        done.getHeader().dst = 0;
        ControlClient{}.signal(root, done);
        init = false;
    }

    return done_anything;
}

void WorkerJob::tryConnectOutput(ec::Node const& node)
{
    if (!out)
        return (void)sLog.log("Worker ", getIdentifier(), ": requested to connect to an output node but no output socket has been allocated");

    sLog.log("Worker ", getIdentifier(), " trying to connect to the output node: ", node.getTcpAddress());
    auto itr = output_nodes.lower_bound(node);
    if (itr != output_nodes.end() && *itr == node)
        return (void)sLog.log("Worker ", getIdentifier(), " already connected");

    auto result = out->connect(node.getTcpAddress());
    if (!result)
        sLog.log("Worker ", getIdentifier(), " could not connect");
    else
    {
        sLog.log("Worker ", getIdentifier(), " connection established");
        output_nodes.insert(node);
    }
}

void WorkerJob::handleConfigurationRequest(ec::Request<ec::packets::Configuration>&& req)
{
    sLog.log("Worker ", getIdentifier(), " new configuration received");
    if (!req.outputs().empty())
    {
        sLog.log("Worker ", getIdentifier(), " requested output to ", req.outputs().size(), " nodes");
        if (!out)
        {
            sLog.log("Worker ", getIdentifier(), " allocating new output socket");
            out = new DataOutputTcp();
        }
        for (auto const& output : req.outputs())
            tryConnectOutput(output);
    }
    else
    {
        sLog.log("Worker ", getIdentifier(), " no output nodes specified, assuming it's terminator node");
        if (out)
            sLog.log("Worker ", getIdentifier(), " deleting previously allocated output socket");
        delete out;
        out = nullptr;
    }
    
    sLog.log("Worker ", getIdentifier(), " configuring child process IO");
    pw.setFetchMode(static_cast<process_wrapper::output_mode>(req.transferMode()));
    pw.setPacketSize(req.packetSize());

	sLog.log("Worker ", getIdentifier(), ": starting child process ", pw.getProgName(), " ", join(req.executableArgs(), ' '));
	pw.start(req.executableArgs());
    poll_network = (in != nullptr);
	init = true;
}

void WorkerJob::handleEndOfInput(ec::Request<ec::packets::EndOfInput>&& req)
{
    if (!input_end)
    {
        sLog.log("Worker ", getIdentifier(), " received EndOfInput packet, marking input as exhausted");
        input_end = true;
    }
    else
        sLog.log("Worker ", getIdentifier(), " received EndOfInput packet but input is already marked as exhausted - packet ignored");
}

void WorkerJob::handleClusterDismantle(ec::Request<ec::packets::ClusterDismantle>&& req)
{
    sLog.log("Worker ", getIdentifier(), " handling dismantle message");
    end_req = true;
    stopChild();
    if (out)
    {
        sLog.log("Worker ", getIdentifier(), " closing output socket");
        delete out;
        out = nullptr;
    }
}
