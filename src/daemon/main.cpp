#include "mtlog.h"
#include "applicationcontroller.h"

#include "config/config_step.h"
#include "network/network_step.h"
#include "cluster/cluster_step.h"

#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
    try {
#ifdef EC_DEBUG
        void OverrideArgumentsInDebugging(int& argc, char**& argv);
        OverrideArgumentsInDebugging(argc, argv);
#endif

        sLog.suppress(); //since we don't know if we have any output, suppress all messages from being printed (they will be queued and could be printed if output will be added and supress(false) called)
        auto exitcode = makeAppController(ConfigStep(argc, argv), NetworkStep(), ClusterStep()).run() ? 0 : 1;
        sLog.close();
        return exitcode;
    }
    catch (std::exception const& e) {
        cout << "Unhandled std::exception: " << e.what() << endl;
        return -1;
    }
    catch (...) {
        cout << "Unhandled exception!" << endl;
        return -1;
    }
}
