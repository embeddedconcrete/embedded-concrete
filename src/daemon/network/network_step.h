#pragma once

#include "applicationcontroller.h"
#include "cluster.h"
#include "socket.h"

#include <list>

class NetworkStep : public ApplicationStep
{
public:
    bool run() override;
    void clean() override;

    auto getControllSocket() const { return control; }

    static auto& getInstance() { return *myself; }

    ec::Cluster::PeersList performNetworkDiscovery();
    std::string getLocalHost();
    std::string getBindAddress() const;

    void loadNetworkDesciption(std::wstring const& file);

    auto getInputPort() const { return inputPort; }

private:
    static NetworkStep* myself;
    ec::ControlServer* control = nullptr;

    ec::Cluster::PeersList availablePeers;

    ec::port_t inputPort = 0;
};
