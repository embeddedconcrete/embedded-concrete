#include "network_step.h"
#include "config/config.h"
#include "strutils.h"
#include "file_utils.h"
#include "packets.h"

#include <stdexcept>
#include <fstream>

using namespace ec;
using namespace std;

NetworkStep* NetworkStep::myself = nullptr;

bool NetworkStep::run()
{
    if (myself)
        throw std::logic_error("Multiple NetworkStep::run");

    myself = this;
    control = new ec::ControlServer();
    inputPort = globalConfig.port.get();

    if (!control->bind(getBindAddress())) {
        throw std::runtime_error("Cannot bind " + getLocalHost());
    }

    if (globalConfig.selfAware.has_value() && globalConfig.selfAware.get())
        availablePeers.insert(ec::Node{ getLocalHost() });

    if (!globalConfig.networkdesc->empty() && file_utils::exists(toutf8(globalConfig.networkdesc.get())))
        loadNetworkDesciption(globalConfig.networkdesc.get());

    return true;
}

void NetworkStep::clean()
{
    availablePeers.clear();

    delete control;
    control = nullptr;

    myself = nullptr;
}

Cluster::PeersList NetworkStep::performNetworkDiscovery()
{
    return availablePeers;
}

string NetworkStep::getLocalHost()
{
    return makestr("tcp://127.0.0.1:", globalConfig.port);
}

std::string NetworkStep::getBindAddress() const
{
    return makestr("tcp://", globalConfig.address, ':', globalConfig.port);
}

void NetworkStep::loadNetworkDesciption(std::wstring const & file)
{
    string node;
    ifstream in(toutf8(file));
    while (in >> node)
        availablePeers.insert(Node{ node });
}
