#pragma once

#include <ostream>
#include <string>

#include "mtlog.h"
#include "optional.h"
#include "singleton.h"

struct Config : public Singleton<Config>
{
#define CMDOPT(name, type, switches, desc, required, def) optional<type> name;
#include "cmdopts.inc"
#undef CMDOPT
    static const std::wstring DefaultRoutinesDirectory;

    void validate();
    void print(std::ostream& out) const;
    void log(Log& l) const;
};

#define globalConfig Config::singleton()
