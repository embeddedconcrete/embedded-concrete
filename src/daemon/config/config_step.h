#pragma once

#include "applicationcontroller.h"
#include "optional.h"

#include <fstream>

class ConfigStep : public ApplicationStep
{
public:
    ConfigStep(int argc, char** argv);

    bool run() override;
    void clean() override;

private:
    optional<std::ofstream> log;
};
