#include "config.h"

#include "cmdline.h"
#include "strutils.h"
#include "stringlist.h"

#include <iostream>
using namespace std;

namespace {

#define CMDOPT(name, type, switches, desc, required, def) CmdOpt __##name{ switches, desc, 1, 1, CMDLINE_ASSIGMENT_HANDLER(globalConfig.name) };
#include "cmdopts.inc"
#undef CMDOPT

}

const wstring Config::DefaultRoutinesDirectory = L"Routines/";

void Config::validate()
{
    StringList conf_errors;

#define CMDOPT(name, type, switches, desc, required, def) \
    if (!name.has_value()) { \
        if (required) \
            conf_errors.push_back(makestr("\n    '"#name "' avaliable switches: ", StringList switches)); \
        else \
            name = def; \
    }

#include "cmdopts.inc"
#undef CMDOPT

    if (conf_errors.empty())
        return;

    string error = "Following required parameters are missing, please set them while running app:";
    for (auto& s : conf_errors)
        error += s;

    std::cout << "E" << std::endl;
    throw runtime_error(error);
}

void Config::print(ostream& out) const
{
    makestr("placki", 12);
#define CMDOPT(name, type, switches, desc, required, def) out << makestr(*begin(switches), "=", name);
#include "cmdopts.inc"
#undef CMDOPT
}

void Config::log(Log & l) const
{
#define CMDOPT(name, type, switches, desc, required, def) l.log(*begin(switches), "=", name);
#include "cmdopts.inc"
#undef CMDOPT
}
