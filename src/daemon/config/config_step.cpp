#include "config_step.h"
#include "cmdline.h"
#include "config.h"
#include "mtlog.h"
#include "strutils.h"

#include <iostream>
using namespace std;

ConfigStep::ConfigStep(int argc, char ** argv)
{
    CmdLine::singleton().parse(argv, argc);
}

bool ConfigStep::run()
{
    globalConfig.validate();

    BEGIN_LOCKED_REGION(sLog)

        if (globalConfig.logfile.has_value())
        {
#ifdef WINDOWS 
            log = ofstream(globalConfig.logfile.get(), ios::out | ios::trunc);
#elif defined(LINUX)
            log = ofstream(toutf8(globalConfig.logfile.get()), ios::out | ios::trunc);
#endif
        
            sLog.addOutput(log.ptr());
        }

        if (!globalConfig.quiet.get())
            sLog.addOutput(&cout);

    END_LOCKED_REGION

    if (!globalConfig.logfile.has_value() && globalConfig.quiet.get())
        sLog.close();
    else
        sLog.suppress(false);

    globalConfig.log(sLog);
    return true;
}

void ConfigStep::clean()
{
    sLog.flush();

    BEGIN_LOCKED_REGION(sLog)
    sLog.removeOutput(log.ptr());
    END_LOCKED_REGION

    log.abandon();
}
