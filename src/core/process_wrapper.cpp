#include "process_wrapper.h"
#include "mtlog.h"

using namespace ec;
using namespace std;

namespace {
    void addArg(string const& str, vector<char*>& vec)
    {
        auto arg = new char[str.size() + 1];
        memcpy(arg, str.data(), str.size());
        arg[str.size()] = 0;
        vec.push_back(arg);
    }
}

process_wrapper::process_wrapper(std::string const& path, io_id_t outputs, io_id_t inputs)
{
    auto list = split(path, ' ');
	auto itr = list.begin();
	fixed_args.reserve(list.size());
    name = move(*itr++);
    replace(name.begin(), name.end(), '\\', '/');

    addArg(name, fixed_args);
    
    while (itr != list.end())
		addArg(*itr++, fixed_args);

	//copy fixed_args to args now to prevent memory leak if process is never called (and args are not copied)
	args.reserve(fixed_args.size());
	for (auto const& arg : fixed_args)
		args.push_back(arg);

    if (!setup(inputs, outputs))
        throw runtime_error("Could not setup child process I/O");
}

process_wrapper::process_wrapper(process_wrapper&& xvalue) : name(move(xvalue.name)), fixed_args(move(xvalue.fixed_args)), args(move(xvalue.args)), started{ xvalue.started.load() }, finished{ xvalue.finished.load() }, inputs(move(xvalue.inputs)), outputs(move(xvalue.outputs)), childPid(xvalue.childPid)
{
#ifdef WINDOWS
    memcpy(&processInfo, &xvalue.processInfo, sizeof(processInfo));
    xvalue.processInfo.hProcess = INVALID_HANDLE_VALUE;
#endif

    xvalue.moved = true;
}

process_wrapper::~process_wrapper()
{
    if (!moved)
    {
        terminate();
        close_all_io();

        for (auto& arg : args)
            delete[] arg;
    }
}

pid_t process_wrapper::start(StringList const& runargs)
{
    setArgs(runargs);
    assert(inputs.size() == 1);
    assert(outputs.size() == 1);
    childPid = start_process_impl(&inputs[0], &outputs[0]);
    started = true;
    return childPid;
}

pid_t process_wrapper::rerun(StringList const& appargs)
{
    if (is_running()) {
        terminate();
    }
    return start(appargs);
}

bool process_wrapper::setup(io_id_t inputs_count, io_id_t outputs_count)
{
    if (!inputs_count)
        inputs_count = 1; //todo remember to create only anonymouse pipe as input and attach it to stdin
    if (!outputs_count)
        outputs_count = 1; //as above

    assert(inputs_count || outputs_count && "Process should have at least one IO handler opened");
    close_all_io();
    if (!setup_io_handlers(inputs_count, inputs))
        return (close_all_io(), false);

    if (!setup_io_handlers(outputs_count, outputs))
        return (close_all_io(), false);

    return true;
}

void process_wrapper::close_all_io()
{
    if (is_running()) {
        sLog.log("Warning, requested to close all IO handlers for process which is still running, terminating...");
        terminate();
    }

    for (auto& input : inputs)
        close_io_impl(input);
    for (auto& output : outputs)
        close_io_impl(output);
}

bool process_wrapper::is_running()
{
    if (!started)
        return false;
    if (finished)
        return false;

    finished = !is_running_impl();
    return !finished;
}

memblock process_wrapper::read(io_id_t output)
{
    if (output >= getOutputsCount())
        return memblock::empty;

    switch (outputs[output].mode) {
        case one_shot: return read_oneshot(output);
        case fixed_packets: return read_fixed(output);
        case prefixed_packets: return read_prefixed(output);
        case synchronized: return read_synchronized(output);
        default:
            return memblock::empty;
    }
}

bool process_wrapper::is_packet_ready(io_id_t output)
{
    read_part(output);

    switch (outputs[output].mode) {
    case one_shot:
        return !is_running();

    case fixed_packets:
        return (aval_bytes(output) >= outputs[output].packet_size);

    case prefixed_packets:
        if (!outputs[output].prefix_read)
            if (!read_prefix(output))
                return false;

        return (aval_bytes(output) >= outputs[output].packet_size);

    case synchronized:
        return false;

    default:
        assert(false && "Unknown fetch_mode");
        return false;
    }
}

void process_wrapper::close_input(io_id_t input)
{
    if (input >= inputs.size())
        return;
    if (inputs[input].closed)
        return;

    close_io_impl(inputs[input]);
}

memlen_t process_wrapper::write(memblock const & block, io_id_t input)
{
    if (input >= getInputsCount())
        return 0;
    if (inputs[input].closed)
        return 0;

    return write_impl(inputs[input], block.data(), block.size());
}

memblock process_wrapper::read_oneshot(io_id_t output)
{
    static constexpr memlen_t read_chunk_size = 1024;
    memlen_t rem = read_chunk_size;

    auto& read_storage = outputs[output].read_storage;
    read_storage.enlarge<char>(read_chunk_size);
    while (true) {
        auto res = read_impl(outputs[output], read_storage.end(), rem);
        if (res <= 0)
            break;

        read_storage.update_size(res);
        if (!rem) {
            read_storage.enlarge<char>(read_chunk_size);
            rem = read_chunk_size;
        }
        else
            rem -= res;
    }

    return move(read_storage);
}

memblock process_wrapper::read_fixed(io_id_t output)
{
    auto& read_storage = outputs[output].read_storage;
    if (read_storage.capacity() != outputs[output].packet_size)
        read_storage.reallocate<char>(outputs[output].packet_size);

    auto res = read_impl(outputs[output], read_storage.end(), outputs[output].packet_size - read_storage.size());
    if (res != (outputs[output].packet_size - read_storage.size()))
        if (!is_running())
            throw process_output_mismatch(outputs[output].packet_size, res);

    read_storage.update_size(res);
    auto ret = move(read_storage);
    read_storage = storage::allocate<char>(outputs[output].packet_size);
    return ret;
}

memblock process_wrapper::read_prefixed(io_id_t output)
{
    auto& read_storage = outputs[output].read_storage;
    if (!outputs[output].prefix_read)
        if (!read_prefix(output))
            return memblock::empty;

    auto res = read_impl(outputs[output], read_storage.end(), outputs[output].packet_size - read_storage.size());
    if (res != (outputs[output].packet_size - read_storage.size()))
        if (!is_running())
            throw process_output_mismatch(outputs[output].packet_size, res);

    read_storage.update_size(res);
    outputs[output].prefix_read = false;
    return move(read_storage);
}

memblock process_wrapper::read_synchronized(io_id_t output)
{
    return memblock::empty;
}

bool process_wrapper::read_prefix(io_id_t output)
{
    if (aval_bytes(output) < 4)
        return false;

    uint32 len = outputs[output].packet_size;
    memcpy(&len, outputs[output].read_storage.begin(), sizeof(len));

    outputs[output].packet_size = len;
    outputs[output].prefix_read = true;
    outputs[output].read_storage.clear();
    outputs[output].read_storage.enlarge<char>(len);
    return true;
}

void process_wrapper::read_part(io_id_t output)
{
    auto bytes = aval_bytes_impl(outputs[output]);
    if (!bytes)
        return;

    if (outputs[output].mode != one_shot)
        bytes = min(bytes, get_remaining_bytes(output));

    if (!bytes)
        return;

    outputs[output].read_storage.enlarge<char>(bytes);
    auto readb = read_impl(outputs[output], outputs[output].read_storage.end(), bytes);
    outputs[output].read_storage.update_size(readb);
}

memlen_t process_wrapper::aval_bytes(io_id_t output)
{
    return outputs[output].read_storage.size();
}

memlen_t process_wrapper::get_remaining_bytes(io_id_t output)
{
    auto curr = outputs[output].read_storage.size();
    auto goal = curr;

    switch (outputs[output].mode)
    {
    case fixed_packets:     goal = outputs[output].packet_size; break;
    case prefixed_packets:  goal = (outputs[output].prefix_read ? outputs[output].packet_size.load() : 4); break;
    default:
        break;
    }

    return (goal > curr ? goal - curr : 0);
}

void process_wrapper::_setArgs(StringList const& newArgs)
{
    //make sure not to delete fixed args
    for (auto i = fixed_args.size(); i < args.size(); ++i)
        delete[] args[i];

    args.reserve(fixed_args.size() + newArgs.size() + 1);
    args.resize(fixed_args.size());
    for (auto const& str : newArgs)
        addArg(str, args);

    args.push_back(0);
}
