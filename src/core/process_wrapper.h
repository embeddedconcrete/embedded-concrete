/*
 * File:   process_wrapper.h
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 10 czerwca 2016, 16:37
 */

#pragma once

#include "ecconfig.h"
#include "memblock.h"
#include "stringlist.h"
#include "strutils.h"
#include "memlen.h"

#include <cstdio>
#include <string>
#include <vector>
#include <atomic>

#ifdef LINUX
#include <signal.h>
#elif defined WINDOWS 
#include <Windows.h>
typedef DWORD pid_t;
#endif

EC_BEGIN

struct process_output_mismatch : public std::exception
{
public:

    process_output_mismatch(memlen_t expected, memlen_t got) throw () : std::exception(), msg(makestr("Expected ", expected, " bytes to read from child process but got only ", got).c_str()) { }

    const char* what() const throw () override
    {
        return msg.c_str();
    }

    std::string msg;
};

/**
 * Creates child process
 */
class process_wrapper
{
public:
    typedef uint8 io_id_t;

    enum output_mode
    {
        one_shot, //ready everything until EOF as one block
        fixed_packets, //split data stream into packets of fixed size
        prefixed_packets, //each packet's data should be prefixed with 4-bytes packet's length
        synchronized, //
        dataSink //placeholder for data sink nodes
    };

    struct process_output
    {
        friend struct output_iterator;
        friend struct process_outputs_iterator;

    public:

        process_output(process_wrapper& pw, io_id_t output) : proc(pw), out(output)
        {
            if (out >= proc.getOutputsCount())
                throw std::runtime_error("Process output index out of range");
        }

        auto begin() const
        {
            return output_iterator{ proc, out, proc.try_read(out)};
        }

        auto end() const
        {
            return output_iterator{ proc, out, memblock::empty};
        }

        process_wrapper& get_proc()
        {
            return proc;
        }

        process_wrapper const& get_proc() const
        {
            return proc;
        }

        auto get_output() const
        {
            return out;
        }

        auto is_packet_ready() const
        {
            return proc.is_packet_ready(out);
        }

    private:
        process_wrapper& proc;
        const io_id_t out;
    };

    struct output_iterator
    {
        friend class process_wrapper;

        decltype(auto) get_block() &
        {
            return reinterpret_cast<memblock&> (myblock);
        }

        decltype(auto) get_block() const&
        {
            return reinterpret_cast<memblock const&> (myblock);
        }

        output_iterator(process_wrapper& proc, io_id_t output, memblock&& mem) : myproc(proc), out(output)
        {
            ::details::create<memblock>(std::addressof(myblock), std::move(mem));
        }

    public:
        output_iterator(process_output& outitr) : myproc(outitr.get_proc()), out(outitr.get_output())
        {
            ::details::create<memblock>(std::addressof(myblock), memblock::empty);
        }

        output_iterator(output_iterator&& itr) : myproc(itr.myproc), out(itr.out)
        {
            ::details::create<memblock>(std::addressof(myblock), std::move(itr.get_block()));
        }

        ~output_iterator()
        {
            ::details::destroy(get_block());
        }

        output_iterator& operator++()
        {
            ::details::destroy(get_block());
            ::details::create<memblock>(std::addressof(myblock), myproc.try_read(out));
            return *this;
        }

        memblock& operator*()
        {
            return get_block();
        }

        bool operator!=(output_iterator const& itr) const
        {
            return get_block().data() != itr.get_block().data();
        }

        output_iterator& operator=(output_iterator&& itr)
        {
            new (this) output_iterator{std::move(itr)};
            return *this;
        }

    private:
        process_wrapper& myproc;
        io_id_t out;

        std::aligned_storage_t<sizeof (memblock), alignof(memblock) > myblock;
    };

    struct process_outputs_iterator
    {
        friend class process_wrapper;

        process_outputs_iterator(process_wrapper& pw, io_id_t output) : proc(pw), out(output) { }

    public:

        process_outputs_iterator(process_wrapper& pw) : proc(pw), out(0) { }

        auto operator*()
        {
            return process_output{ proc, out};
        }

        auto& operator++()
        {
            if (out != proc.getOutputsCount())
                ++out;
            return *this;
        }

        bool operator!=(process_outputs_iterator const& itr) const
        {
            if (proc.getPid() != itr.proc.getPid())
                return true;
            return out != itr.out;
        }

    private:
        process_wrapper& proc;
        io_id_t out;
    };

public:
    process_wrapper(std::string const& path="", io_id_t outputs = 0, io_id_t inputs = 0);
    process_wrapper(process_wrapper&& xvalue);
    process_wrapper(const process_wrapper& orig) = delete;

    ~process_wrapper();

    auto begin() { return process_outputs_iterator{ *this, 0}; }
    auto end()   { return process_outputs_iterator{ *this, static_cast<io_id_t> (outputs.size())}; }

    void setPacketSize(uint32 packet_size, io_id_t output = 0) { if (output < outputs.size() && outputs[output].mode == output_mode::fixed_packets) outputs[output].packet_size = packet_size; }
    void setFetchMode(output_mode mode, io_id_t output = 0)    { if (output < outputs.size()) outputs[output].mode = mode;  }

    pid_t getPid() const                    { return childPid; }

    auto const& getProgName() const         { return name; }
    auto getBinName() const          		{ return name.substr(name.rfind('/') + 1); }
    auto getProgDir() const                 { return name.substr(0, name.rfind('/')); }

    void setArgs(StringList const& newargs) { _setArgs(newargs); }
    auto const& getArgs() const             { return args; }

    pid_t start(StringList const& appargs);
    pid_t rerun(StringList const& appargs);

    int terminate();
    int fkill();

    bool is_running();
    bool has_started() const                { return started; }

    memblock read(io_id_t output = 0);

    memblock try_read(io_id_t output = 0)
    {
        if (!is_packet_ready(output)) 
            return memblock::empty;
        return read(output);
    }
    memlen_t write(memblock const& block, io_id_t input = 0);

    bool is_packet_ready(io_id_t output = 0);
    void close_input(io_id_t input = 0);

    io_id_t getInputsCount() const          { return static_cast<io_id_t> (inputs.size()); }
    io_id_t getOutputsCount() const         { return static_cast<io_id_t> (outputs.size()); }

private:

    struct IOHandler
    {
#ifdef LINUX
        typedef int PIPE_DESCR_HANDLE;
#elif defined WINDOWS
        typedef HANDLE PIPE_DESCR_HANDLE;
#endif

        PIPE_DESCR_HANDLE myend; // on daemon side
        PIPE_DESCR_HANDLE childend;
        bool closed = true;

    };

    struct OHandler : public IOHandler
    {
        OHandler() = default;

        OHandler(OHandler const& rval) : mode(rval.mode.load()), packet_size(rval.packet_size.load()), prefix_read(rval.prefix_read) { }

        std::atomic<output_mode> mode = { one_shot };
        std::atomic<uint32> packet_size = { 0 };
        bool prefix_read = false;
        storage read_storage;
    };

private:
    std::string name;
    std::vector<char*> fixed_args;
    std::vector<char*> args;
    std::atomic<bool> started = { false };
    std::atomic<bool> finished = { false };

    std::vector<IOHandler> inputs;
    std::vector<OHandler> outputs;

	pid_t childPid;

    bool moved = false;

#ifdef WINDOWS
    PROCESS_INFORMATION processInfo;
#endif

	void _setArgs(StringList const& newArgs);

private: //IO
    bool setup(io_id_t inputs_count, io_id_t outputs_count);

    template <class T>
    std::enable_if_t<std::is_base_of<IOHandler, T>::value, bool> setup_io_handlers(io_id_t count, std::vector<T>& handlers)
    {
        if (!count) {
            handlers.clear();
            return true;
        }

        handlers.resize(count);
        for (auto& hnd : handlers)
            if (!create_io_impl(hnd, std::is_same<OHandler,T>::value))
                return false;

        return true;
    }

    void close_all_io();

    memblock read_oneshot(io_id_t output);
    memblock read_fixed(io_id_t output);
    memblock read_prefixed(io_id_t output);
    memblock read_synchronized(io_id_t output);
    bool read_prefix(io_id_t output);

    memlen_t read_bytes(io_id_t output, char* ptr, memlen_t len);
    void read_part(io_id_t output);
    memlen_t aval_bytes(io_id_t output);
    memlen_t get_remaining_bytes(io_id_t output);

private: //OS specific
    memlen_t read_impl(IOHandler& io, char* ptr, memlen_t len);
    memlen_t write_impl(IOHandler& io, const char* ptr, memlen_t len);

    memlen_t aval_bytes_impl(IOHandler& io);

    // output is from context of child process
    bool create_io_impl(IOHandler& io, bool output);
    void close_io_impl(IOHandler& io);

    //process status
    pid_t start_process_impl(IOHandler* child_stdin, IOHandler* child_stdout);
    bool is_running_impl() const;
};

EC_END
