
#ifdef WINDOWS
#include "process_wrapper.h"
#include "mtlog.h"

#include <string>
#include <stdexcept>
#include <cstring>
#include <cstdio>
#include "config/config.h"

#define NOMINMAX
#include <Windows.h>

using namespace ec;
using namespace std;

constexpr memlen_t defaultPipeBufferSize = 512;

namespace {
    struct pipe_close_guard {
        pipe_close_guard(HANDLE& p1, HANDLE& p2) : _p1(p1), _p2(p2) {}
        ~pipe_close_guard() { if (guard) { CloseHandle(_p1); _p1 = INVALID_HANDLE_VALUE; CloseHandle(_p2); _p2 = INVALID_HANDLE_VALUE; } }

        bool guard = true;
        HANDLE& _p1;
        HANDLE& _p2;
    };

}

pid_t process_wrapper::start_process_impl(IOHandler* child_stdin, IOHandler* child_stdout)
{
    assert(!child_stdin || !child_stdin->closed && "Pipe passed as stdin has been closed");
    assert(!child_stdout || !child_stdout->closed && "Pipe passed as stdout has been closed");

    STARTUPINFOW processStartupInfo;

    ZeroMemory(&processInfo, sizeof (processInfo));
    ZeroMemory(&processStartupInfo, sizeof (processStartupInfo));
    processStartupInfo.cb = sizeof (processStartupInfo);

    if (child_stdin || child_stdout)
    {
        processStartupInfo.hStdInput = (child_stdin ? child_stdin->childend : INVALID_HANDLE_VALUE);
        processStartupInfo.hStdOutput = (child_stdout ? child_stdout->childend : INVALID_HANDLE_VALUE);
        processStartupInfo.hStdError = GetStdHandle(STD_ERROR_HANDLE);
        processStartupInfo.dwFlags |= STARTF_USESTDHANDLES;
    }

    // create child process
    memlen_t size = 0;
    for (auto const& arg : args)
        size += (arg ? strlen(arg) : 0);

    wstring cmdline;
    cmdline.reserve(size + args.size() * 2 + 1);
    for (auto const& arg : args)
    {
        if (!arg)
            continue;

        if (!cmdline.empty())
            cmdline += L" ";
        if (strchr(arg, ' ') != nullptr)
        {
            cmdline += L"\"";
            cmdline += fromutf8(arg);
            cmdline += L"\"";
        }
        else
            cmdline += fromutf8(arg);
    }

    auto wname = fromutf8(name);
    auto wdir = fromutf8(getProgDir());

    LPCWSTR wname_ptr = wname.c_str();
    LPCWSTR wdir_ptr = wdir.c_str();
    if (!CreateProcessW(wname_ptr, &cmdline[0], 0, 0, true, 0, 0, wdir_ptr, &processStartupInfo, &processInfo)) {
        //todo: check error?
        throw runtime_error("Cannot start child process");
    }

    if (child_stdin)
        CloseHandle(child_stdin->childend);
    if (child_stdout)
        CloseHandle(child_stdout->childend);

    return processInfo.dwProcessId;
}

memlen_t process_wrapper::read_impl(IOHandler& io, char* ptr, memlen_t len)
{
    DWORD readBytes = 0;
    if (FALSE == ReadFile(io.myend, ptr, static_cast<DWORD>(len), &readBytes, 0)) 
    {
        uint32 error = GetLastError();
        if (error != ERROR_BROKEN_PIPE && error != ERROR_NO_DATA)
            sLog.log("Reading from child process failed: ", name, " error: ", error);

        return 0; //todo: mark 'io' as closed from child's side? (note: may not mean that child has exited)
    }

    return readBytes;
}

memlen_t process_wrapper::write_impl(IOHandler& io, const char* ptr, memlen_t len)
{
    DWORD bytesWritten = 0;
    if (FALSE == WriteFile(io.myend, ptr, static_cast<DWORD>(len), &bytesWritten, 0)) 
    {
        uint32 error = GetLastError();
        if (error != ERROR_BROKEN_PIPE)
            sLog.log("Writing to child process failed: ", name, " error: ", error);

        return 0; //todo: mark 'io' as closed from child's side? (note: may not mean that child has exited)return 0;
    }

    return bytesWritten;
}

memlen_t process_wrapper::aval_bytes_impl(IOHandler& io)
{
    DWORD aval = 0;
    if (FALSE == PeekNamedPipe(io.myend, nullptr, 0, nullptr, &aval, nullptr))
    {
        uint32 error = GetLastError();
        if (error != ERROR_BROKEN_PIPE)
            sLog.log("Reading from child process failed: ", name, " error: ", error);

        return 0; //todo: mark 'io' as closed from child's side? (note: may not mean that child has exited)
    }

    return aval;
}

bool process_wrapper::create_io_impl(IOHandler& io, bool output)
{
    if (!io.closed)
        return false;

    SECURITY_ATTRIBUTES secAttrs;
    secAttrs.nLength = sizeof(SECURITY_ATTRIBUTES);
    secAttrs.bInheritHandle = TRUE;
    secAttrs.lpSecurityDescriptor = NULL;

    // create pipelines
    if (!CreatePipe(output ? &io.myend : &io.childend, output ? &io.childend : &io.myend, &secAttrs, 0))
        return false;
    
    if (!SetHandleInformation(io.myend, HANDLE_FLAG_INHERIT, 0))
        return false;

    DWORD mode = PIPE_READMODE_BYTE | PIPE_NOWAIT;
    if (FALSE == SetNamedPipeHandleState(io.myend, &mode, nullptr, nullptr))
        sLog.log("Warning: could not set pipe mode to PIPE_NOWAIT, deadlocks are possible");

    io.closed = false;
    return true;
}

void process_wrapper::close_io_impl(IOHandler& io)
{
    if (io.closed)
        return;

    CloseHandle(io.myend);
    io.closed = true;
}

int process_wrapper::terminate()
{
    return TerminateProcess(processInfo.hProcess, EXIT_FAILURE);
}

int process_wrapper::fkill()
{
    return TerminateProcess(processInfo.hProcess, EXIT_FAILURE);
}

bool process_wrapper::is_running_impl() const
{
    DWORD code;
    if (FALSE == GetExitCodeProcess(processInfo.hProcess, &code))
        throw runtime_error("Could not check status of child process!");

    return (code == STILL_ACTIVE);
}

#endif
