/*
 * File:   process_wprapper_linux.cpp
 * Author: Wojciech Paluch <wojciech.paluch1993 at gmail.com>
 *
 * Created on 15 czerwca 2016, 00:40
 */

#ifdef LINUX
#include "process_wrapper.h"
#include "mtlog.h"
#include "strutils.h"
#include "dir.h"

#include <cstring>
#include <cstdio>
#include <cassert>
#include <stdexcept>
#include <string>

#include <unistd.h>
#include <signal.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <thread>
#include <chrono>


using namespace ec;
using namespace std;

#define PIPE_WRITE_END 1
#define PIPE_READ_END 0


pid_t process_wrapper::start_process_impl(IOHandler* child_stdin, IOHandler* child_stdout)
{
    assert(child_stdin);
    assert(child_stdout);

    childPid = fork();
	
    if (childPid < 0) {
        throw runtime_error("Cannot create child process");
        return 0;
    } else if (childPid == 0) {
		// child process
		
        // swap the pipes roles 
        // to read from input and write to output
        if (child_stdin) {
            close(child_stdin->myend);
            dup2(child_stdin->childend, STDIN_FILENO);
            fcntl(child_stdin->childend, F_SETFD, FD_CLOEXEC);
        }
        if (child_stdout) {
            close(child_stdout->myend);
            dup2(child_stdout->childend, STDOUT_FILENO);
            fcntl(child_stdout->childend, F_SETFD, FD_CLOEXEC);
        }

        fprintf(stderr, "Running child process (%d) with %s\n", getpid(), name.c_str());
        if (chdir(getProgDir().c_str()) != 0)
            throw runtime_error("Cannot cd " + getProgDir());

        execv(getBinName().c_str(), args.size() > 0 ? args.data() : nullptr); // run the process
        perror(strerror(errno));
        throw runtime_error("Cannot run child process");
    }
    if (child_stdin) {
        close(child_stdin->childend);
    }
    if (child_stdout) {
        close(child_stdout->childend);
    }
    return childPid;
}

memlen_t process_wrapper::read_impl(IOHandler& io, char* ptr, memlen_t len)
{
    auto ret = ::read(io.myend, ptr, len);
    if (ret <= 0) {
        if (ret < 0) {
            sLog.log("Reading from child process failed: ", name, " error: ", strerror(errno));
        }
        return 0;
    }
    return ret;
}

memlen_t process_wrapper::write_impl(IOHandler& io, const char* ptr, memlen_t len)
{
    auto ret = ::write(io.myend, ptr, len);
    if (ret < 0) {
		if(errno != EAGAIN) {
			sLog.log("writing to child process failed: ", name, " error: ", strerror(errno));
		}
        return 0;
    }
    return ret;
}

int process_wrapper::terminate()
{
    return kill(getPid(), SIGTERM);
}

int process_wrapper::fkill()
{
    return kill(getPid(), SIGKILL);
}

memlen_t process_wrapper::aval_bytes_impl(IOHandler& io)
{
    int bytesAvailable;
    auto err = ioctl(io.myend, FIONREAD, &bytesAvailable);
    if (err < 0) {
        sLog.log("An error occured while trying to peek available data size in pipeline");
        return 0;
    }
    assert(bytesAvailable >= 0);

    return static_cast<memlen_t> (bytesAvailable);
}

bool process_wrapper::create_io_impl(IOHandler& io, bool output)
{
    if (!io.closed)
        return false;

    int handlers[2];
    if (pipe(handlers) != 0) {
        throw runtime_error(makestr("Cannot create ", output ? "output" : "input", " pipeline for ", name));
    }

	fcntl(handlers[PIPE_WRITE_END], F_SETPIPE_SZ, 4*1024*1024);

    fcntl(handlers[PIPE_WRITE_END], F_SETFD, FD_CLOEXEC);
    fcntl(handlers[PIPE_READ_END], F_SETFD, FD_CLOEXEC);

    io.myend = handlers[output ? PIPE_READ_END : PIPE_WRITE_END];
    io.childend = handlers[output ? PIPE_WRITE_END : PIPE_READ_END];

	// make myend non blocking
	int flags = fcntl(io.myend, F_GETFL);
	fcntl(io.myend, F_SETFL, flags | O_NONBLOCK);

    io.closed = false;
    return true;
}

void process_wrapper::close_io_impl(IOHandler& io)
{
    if (io.closed)
        return;
    auto ret = close(io.myend);
    io.closed = true;
}

bool process_wrapper::is_running_impl() const
{
    int status;
    pid_t result = waitpid(childPid, &status, WNOHANG);
    if (result == -1)
        throw runtime_error("Could not check status of child process: "+makestr(result));
    return (result == 0);
}

#endif
