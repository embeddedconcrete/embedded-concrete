# EC - Embedded Concrete - system pozwalający na autonomiczną budowę klastra urządzeń wbudowanych (i nie tylko) oraz rozproszenie obliczeń na wiele urządzeń

## Instalacja EC na płytkach Intel Galileo gen. 1:

Do działania EC na płytkach Intel Galileo konieczne jest uruchomienie przygotowanego systemu Gentoo. 
Na karcie microSD należy utworzyć następujące partycje (dokładnie w podanej kolejności):

* partycja boot, początkowa, wielkość ok. 200MB, system plików fat32
* partycja swap, 2048MB, system plików linux-swap
* partycja root, wielkość powyżej 500MB, system plików f2fs

Po utworzeniu partycji należy rozpakować archiwum *galileo-gentoo.tar* z poziomu systemu Linux przy pomocy polecenia:
```
# mkdir galileo-gentoo && tar -xpf galileo-gentoo.tar -C galileo-gentoo
```
(Konieczne jest działanie z konta użytkownika root oraz dodanie przełącznika -p, aby zachowane zostały prawa dostępu do plików)

Następnie należy zamontować partycje boot oraz root karty microSD. W tej instrukcji przyjęto, że partycje zostały zamontowane w katalogach:
* boot: /mnt/microSD-boot
* root: /mnt/microSD-root

Po rozpakowaniu w katalogu *galileo-gentoo* pojawią się katalogi z plikami, które - za wyjątkiem katalogu boot - należy przekopiować na partycję root:
```
# cd galileo-gentoo
# cp -r !(boot) /mnt/microSD-root/
```

Na partycji root utworzyć pusty katalog `boot`.
```
# mkdir /mnt/microSD-root/boot
```

Przekopiować zawartość katalogu boot z rozpakowanej paczki bezpośrednio na partycję boot karty microSD:
```
# cp -r boot/* /mnt/microSD-boot/
```

W pliku `/mnt/microSD-root/etc/conf.d/net` należy wpisać ustawienia karty sieciowej (adres IP, maska, brama domyślna);
Podobnie w pliku `/mnt/microSD-root/etc/conf.d/hostname` można ustalić nazwę urządzenia;


Po tych operacjach należy bezpiecznie wysunąć urządzenie, a gotową kartę microSD włożyć w czytnik kart na płytce Intel Galileo.
Płytkę w tym momencie można bezpiecznie uruchomić, system Gentoo powinien uruchomić się automatycznie (uruchamianie może potrwać do ok. minuty).

Po podpięciu płytki do sieci można nią zarządzać przy pomocy ssh - należy zalogować się na konto roota, które dla ułatwienia procesu wytwarzania oprogramowania zostało skonfigurowane tak, aby umożliwić logowanie bez hasła.

```
$ ssh root@192.168.0.5
```

Po pierwszym zalogowaniu się na urządzeniu należy wykonac polecenie `ldconfig`, w celu aktualizacji ścieżek linkowania bibliotek dynamicznych.



## Uruchomienie systemu Embedded Concrete

Na urządzeniach Intel Galileo system Embedded Concrete można uruchomić wchodząc w katalog `/root/ec/bin/` oraz uruchamiając plik `./ec-daemon`.
Program posiada parametry uruchomienia, które pozwalają na konfigurację uruchomienia:
```
$ ./ec-daemon \
	[-port <port>] \# pozwala ustalić port konfiguracyjny, domyślnie 8888
	[-log <logfile.log>] \# Pozwala ustalić plik loga, domyślnie "ec.log"
	[-run_cluster <ścieżka do pliku opisu>] \# Pozwala na jednorazowe uruchomienie klastra w oparciu o podany plik z zapisaną strukturą klastra
	[-maxWorkers <liczba węzłów roboczych>] \# Ustala maksymalną liczbę węzłów roboczych, jakie mogą zostać zaalokowane na tym węźle fizycznym, domyślnie 4
	[-netfile <plik z adresami ip>] \# Ustala plik z adresami ip hostów, jakie mogą współpracować z tą instancją klastra
	[-quiet] \# Ustawienie tej flagi wycisza drukowanie komunikatów - log lądował będzie tylko w pliku loga
```

Przykładowe uruchomienie:
```
$ ./ec-daemon -port 8888 -maxWorkers -netfile network.desc -log run.log
```

Po uruchomieniu procesu ec-daemon, możliwe jest uruchomienie klastra, podając w wewnętrznym wierszu poleceń ścieżkę do pliku z opisem klastra.
Sposób tworzenia pliku ze strukturą klastra został opisany w Dodatku A

## Testowe programy

W katalogu `/root/ec/tests/bin/workers` znajdują się przykładowe programy testowe.
Na temat sposobu uruchomienia każdego z nich można się dowiedzieć uruchamiając dany program z parametrem --help


## Rozwój i kompilacja programu

Do budowania projekt wykorzystuje program CMake. W tym projekcie budowanie zostało zautomatyzowane, tak aby umożliwić kompilację zarówno na architekturę i586 dla płytek Intel Galileo jak i dla architektury x86_64. 

Budowa całego projektu jest możliwa przy użyciu polecenia
```
$ ./ec deps: configure make ec: configure make_tests make :tools/compiler-i586
```

Sekwencja `ec:` oznacza wyznaczenie celu kompilacji, dla którego podawane są opcje.
Opcja `configure` powoduje uruchomienie programu cmake w celu wygenerowania nowej konfiguracji dla programu make.
Opcja `make_tests` odpowiada za budowę programów testowych
opcja `make` powoduje uruchomienie budowania projektu

Sekwencja `:<ściezka do pliku>` pozwala wczytać fragment konfiguracji i jest dobrym sposobem grupowania parametrów określających kompilator.
Przykładowa zawartość pliku `tools/compiler-i586`:
```
default: --c_compiler i586-pc-linux-gnu-gcc --cxx_compiler i586-pc-linux-gnu-g++
```

W przypadku problemów z linkowaniem biblioteki OpenCV należy wskazać ścieżkę do katalogu instalacji biblioteki w zmiennej środowiskowej `OpenCV_DIR`, przykładowo:
```
$ export OpenCV_DIR=/usr/i586-pc-linux-gnu/usr/
```

