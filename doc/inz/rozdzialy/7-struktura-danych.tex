\section[Projekt struktury danych (Łukasz Dudziak)]{Projekt struktury danych}

Jako aplikacja działająca w~charakterze usługi systemowej, opisywany system nie zawiera
żadnych zewnętrznych zależności związanych z~przetwarzanymi danymi - wszystkie
informacje niezbędne do jego poprawnego działania przechowywane są lokalnie i, poza
dodatkową usługą diagnozowania i~monitorowania pracy systemu, nie istnieje potrzeba
udostępniania ich poza opisywany system. Dodatkowo, biorąc pod uwagę zadanie stawiane
warstwom pośredniczącym, do których zalicza się omawiany system, należy zwrócić
uwagę na~fakt, że przetwarzane dane mają bezpośredni związek z~czasem użycia
obsługiwanego programu. Prowadzi to do wniosku, że opisywany system nie wymaga
specjalnego miejsca do trwałego przechowywania danych - a~zatem bazy danych.
Dokładniej rzecz biorąc, wynika to z~faktu, że opisywany system jest systemem
bezstanowym w~tym sensie, że jego wyłączenie powinno wiązać się ze zwolnieniem
wszystkich zajmowanych zasobów, a~przywrócenie poprzedniego stanu jest wręcz
niepożądane.

Oczywiście istnieje możliwość rozszerzenia systemu o~zabezpieczenia mające na~celu
zwiększenie niezawodności w~sytuacji \emph{chwilowej} awarii, w~której przypadku której
system byłby w~stanie racjonalnie szybko ponownie się uruchomić, odtworzyć swój stan i
podjąć przerwaną pracę bez narażania współpracujących jednostek na~potrzebę reagowania
na niespodziewane zaburzenie struktury sieciowej klastra. Wytworzenie takiej
funkcjonalności na~pewno wymagałoby użycia trwałego nośnika do zapisu danych, jest to
jednak zadanie niezwiązane bezpośrednio z~tematyką opisywanego systemu -- dużo bardziej
wyraźna jest tutaj potrzeba wykrycia takiej sytuacji i~reakcji na~nią po stronie
współpracujących jednostek. Oba te stwierdzenia wynikają odpowiednio z~faktów, że:
\begin{itemize}
\item odtworzenie stanu procesu możliwe jest przez zewnętrzne narzędzia udostępnione w~ramach
systemu operacyjnego, jest to więc alternatywa niewymagająca specjalnego wsparcia w~projektowanym systemie,
\item reakcja na~zaburzenie struktury klastra jest natomiast związana
bezpośrednio z~logiką warstwy pośredniczącej i~nie istnieje (przynajmniej łatwa)
możliwość wykorzystania do tego narzędzi ogólnego zastosowania -- musi ona brać pod
uwagę specyfikę konkretnego systemu.
\end{itemize}

Ponieważ jednak reakcja na~utratę połączenia odbywa się oczywiście po stronie działającej instancji systemu -- która, w~myśli informacji z~pierwszego akapitu, jest samowystarczalna i~nie wymaga pobierania i~udostępniania danych poza system -- w~konsekwencji nie wymaga przechowywania ani pobierania danych z~pamięci trwałej.

Podsumowując, opisywany system nie implementuje dostępu do żadnej pamięci trwałej
(poza małymi wyjątkami opisanymi poniżej, niezwiązanymi bezpośrednio z~funkcjonalnością systemu), w~szczególności nie wykorzystuje on w~tym celu bazy danych.
Aby przybliżyć jednak obraz systemu jako całości, w~niniejszym dokumencie
zwrócono uwagę na~takie aspekty jak: reprezentacja w~pamięci struktury klastra,
rozmieszczenie danych pomiędzy węzły (które jednostki do pracy potrzebują jakich
informacji) oraz operacje możliwe do przeprowadzenia na~tej strukturze, udostępnione
przez zaprojektowany w~tym celu protokół sterujący.

\subsection{Schemat przetwarzania danych dot. struktury klastra}
Struktura klastra jest podstawowym i, z~punktu widzenia interakcji z~użytkownikiem, jedynym elementem przetwarzanym przez wspomniany system (w rzeczywistości dochodzą do tego również komunikaty sieciowe wymieniane pomiędzy jednostkami i~pakiety danych przesyłane w~ramach tego klastra -- żaden z~tych elementów nie jest jednak uwidaczniany użytkownikowi i~od niego nie pochodzi). Wynika z~tego fakt, że usługi oferowane użytkownikowi przez opisywany system związane są bezpośrednio właśnie ze strukturą klastra -- od utworzenia klastra o~zadanej strukturze, poprzez jej modyfikację w~czasie realizacji obliczeń. Wiąże się z~tym kolejny fakt: struktura ta i~dane ją opisujące znajdować się mogą na~wielu etapach przetwarzania, na~których obecny jest inny zbiór wartości i~które mogą, bądź też nie, zmieniać się dynamicznie w~ramach żądań użytkownika.

Na rysunku \ref{imgs:cluster_struct} zaprezentowano dosyć ogólny, poglądowy schemat przetwarzania danych na
poszczególnych etapach: jakie elementy potrzebne są następnemu etapowi i~jakie operacje
są możliwe do wykonania na~obecnym.

\begin{figure}[H]
\includegraphics[scale=0.7]{imgs/7-1-cluster-struct.png}
\caption{Schemat przetwarzania struktury klastra na~poszczególnych etapach działania systemu} \label{imgs:cluster_struct}
\end{figure}

W dalszej części dokumentu, wytłumaczone zostaną szczegółowo struktury przetwarzane na każdym z~etapów.

\subsection{Plik z~opisem struktury klastra}
W celu ułatwienia pracy użytkownikowi klastra, system umożliwia zapisanie i~ponowne,
wielokrotne użycie tej samej struktury klastra. W~tym celu struktura taka opisana zostaje
przy użyciu specjalnej składni, w~formacie tekstowym.
Opis taki pozwala na~definiowanie 3 rodzajów elementów:
\begin{itemize}
\item węzła fizycznego,
\item konfiguracji węzła roboczego,
\item samego węzła roboczego.
\end{itemize}

Dwa pierwsze elementy wykorzystanie mogą być przy definicji trzeciego i~jako takie są
opcjonalne (nie ma potrzeby ich osobnego definiowania, mogą zostać opisane bezpośrednio
w definicji węzła roboczego). Ich wcześniejsze opisanie pozwala jednak na~wielokrotne
wykorzystanie ich w~opisie wielu węzłów roboczych, które mają przyjmować te same
wartości wybranych parametrów.

Szczegółowy opis gramatyki znajduje się w~dodatku \ref{app:gramma}.

\subsection{Opis struktury klastra}
Opis struktury klastra wykorzystywany jest przez koordynatora na~etapie pozyskiwania
węzłów roboczych. Zawiera on opis logicznej struktury klastra oraz opcjonalne ograniczenia
nałożone na~topologię fizyczną. Żaden z~węzłów roboczych nie otrzymuje szczegółowych
informacji na~temat struktury w~momencie prośby o~pomoc -- jest to etap, na~którym
określone ma być, czy węzeł w~ogóle jest w~stanie pomóc, bez szczegółowego określania, w
jakim trybie ma przebiegać jego praca.

\noindent
Koordynator przechowuje na~temat węzłów takie informacje jak:
\begin{itemize}
\item nazwa uruchamianego polecenia,
\item stałe argumenty dla polecenia,
\item listę węzłów, do których ten węzeł ma przesyłać dane produkowane przez polecenie,
\item tryb podziału wysyłanych danych,
\item rozmiar pakietu (wykorzystywany przy trybie o~stałej wielkości),
\item flagę, czy dany węzeł jest źródłem danych (nie oczekuje niczego na~wejściu),
\item flagę, czy dany węzeł ma być alokowany zachłannie,
\item opcjonalny adres węzła fizycznego, na~którym uruchomiony ma być opisywany
węzeł roboczy.
\end{itemize}

\subsection{Klaster}
Klaster jest już strukturą przechowywaną przez koordynatora w~czasie działania klastra. W~związku z~tym, poza informacjami pochodzącymi bezpośrednio z~opisu klastra, przechowuje on również te specyficzne dla elementów dynamicznych, m.in. obecny stan, w~którym znajdują się poszczególne węzły.

Dodatkowo, koordynator przechowuje informacje na~temat adresu urządzenia, na~którym
zaalokowany został konkretny węzeł roboczy, jak również adres, na~którym uruchomiony
węzeł oczekuje na~dane wejściowe, które mają zostać przekazane do uruchomionego
procesu (chyba że węzeł został oznaczony jako źródło danych i~w związku z~tym nie
otworzył takiego połączenia).

W procesie alokacji węzłów roboczych na~węzłach fizycznych, Koordynator rozszerza przetwarzane
informacje o~następujące elementy:
\begin{itemize}
\item identyfikator węzła w~ramach klastra,
\item adres węzła fizycznego (jeśli wcześniej nie był określony),
\item adres dla danych przychodzących (może być pusty, o~ile węzeł jest źródłem i~nie
otworzył połączenia),
\item listę węzłów, które swoje wyjście przekazują do tego węzła,
\item flagę zakończenia pracy, mówiąca czy węzeł zgłosił fakt zakończenia pracy,
\item flagę rozwiązania, mówiąca, czy węzeł został już zdealokowany (nie istnieje już
żadna informacja na~jego temat na~współpracującym urządzeniu).
\end{itemize}

\subsection{Operacje dostępne w~czasie działania klastra}
Poniżej znajduje się spis operacji dostępnych na~strukturze klastra, jakich użytkownik może
żądać od Koordynatora w~trakcie pracy klastra. Szczegółowa specyfikacja każdej z~nich
przedstawiona została w~projekcie protokołu sterującego, opisanego w~rozdziale \ref{chap:protocole}, którego zadaniem jest właśnie
zdefiniowanie ram komunikacyjnych do realizacji wymienionych tutaj działań.

\noindent
Możliwe operacje to:
\begin{itemize}
\item dynamiczne dodanie nowego węzła, na co składa się:
\begin{itemize}
\item otworzenie połączenia dla danych przychodzących na~wybranym węźle,
\item nawiązanie dodatkowego połączenia wychodzącego z~wybranego węzła -
pomiędzy nim a~nowo utworzonym;
\end{itemize}
\item odłączenie węzła ze struktury klastra w~trakcie jego działania,
\item sygnalizacja zamknięcia wszystkich węzłów wejściowych dla danego węzła
(przełączenie w~tryb pracy węzła źródłowego),
\item zmiana trybu podziału danych,
\item restart aplikacji obsługiwanej na~wybranym węźle,
\item wylistowanie Koordynatorów działających na~danym węźle fizycznym,
\item pobranie listy węzłów roboczych wchodzących w~skład klastra, wraz z~adresami
węzłów fizycznych, na~których się znajdują,
\item subskrypcja informacji o~stanie węzła roboczego,
\item usunięcie subskrypcji.
\end{itemize}

\subsection{Informacje przechowywane na~węzłach roboczych}
W przyjętym modelu pracy systemu, to koordynator obciążony jest koniecznością
tworzenia i~zarządzania strukturą klastra, i z~założenia nie angażuje w~ten proces węzłów
roboczych. Powoduje to, że instancje systemu zajmujące się realizacją funkcjonalności
węzła roboczego mają zdecydowanie mniejszy narzut związany z~przechowywanymi po
swojej stronie informacjami.

Powoduje to też, że węzły robocze muszą przede wszystkim znać adres koordynatora, dla
którego wykonują pracę -- to z~nim będą się komunikować. Dodatkowo przekazywane im są
wszystkie informacje niezbędne do uruchomienia i~obsługi aplikacji, nawiązania połączenia
z węzłami, do których przekazywane mają być dane. Węzły robocze same też przechowują informacje
na temat stanu, w~jakim się znajdują.
Dokładny spis przechowywanych danych obejmuje:
\begin{itemize}
\item nazwę i~argumenty dla uruchamianego procesu,
\item tryb podziału wyjścia,
\item adres koordynatora,
\item listę adresów, na~które przekazywane mają być kolejne pakiety danych (same adresy,
w szczególnym przypadku koordynator może skonfigurować węzeł roboczy, żeby
przekazywał dane do jednostki, która nie wchodzi w~skład klastra),
\item flagę mówiąca, czy węzeł jest w~trakcie pracy,
\item flagę mówiąca, czy należy oczekiwać więcej danych przychodzących z~sieci,
\item opcjonalnie, otwarte połączenie dla danych przychodzących,
\item opcjonalnie, otwarte połączenie dla danych wychodzących.
\end{itemize}
