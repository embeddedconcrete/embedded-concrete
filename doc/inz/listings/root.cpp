class RootJob : public Job<ec::Cluster::Description>
{
    using parent = Job<ec::Cluster::Description>;

public:
    RootJob(ec::Cluster* mycluster);
    ~RootJob();

    auto getCluster() const { return cluster; }

    std::string getIdentifier() const { return makestr(cluster->getId()); }

private:
    ec::Cluster* cluster;

    bool init(ec::Cluster::Description&& desc) override;
    void handleRequest(ec::OpCode OC, ec::Request<memblock>& req) override;
    void handleWorkIsDone(ec::Request<ec::packets::WorkIsDone>&& req);
    void handleDynamicWorker(ec::Request<ec::packets::DynamicWorker>&& req);
    void handleWorkerDetach(ec::Request<ec::packets::WorkerDetach>&& req);
    void handleDbgListWorkers(ec::Request<ec::packets::DebugListWorkersReq>&& req);
};
