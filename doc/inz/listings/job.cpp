template <class... T>
class Job
{
public:
    Job() = default;

    ~Job()
    {
        end();
    }
    
    void run(T&&... t)
    {
        if (running)
            return;

        running = true;
        myjob = std::thread([this](T&&... t) {
            if (!init(std::forward<T>(t)...))
            {
                running = false;
                myjob.detach();
                ClusterStep::getInstance().removeJobHnd(this);
                return;
            }

            while (running && !end_req)
            {
                handleRequests();
                if (!job())
                    std::this_thread::sleep_for(std::chrono::milliseconds{ 10 });
            }

            running = false;
            myjob.detach();
            ClusterStep::getInstance().removeJobHnd(this);
        }, std::forward<T>(t)...);
    }
    
    void end(bool dowait = true)
    {
        if (!running)
            return;

        end_req = true;
        if (dowait)
            wait();
    }

    void wait()
    {
        if (!running)
            return;

        if (myjob.joinable())
            myjob.join();
    }

private:
    std::thread myjob;

    virtual bool init(T&&... t) { return true; }

    //derived class can return true if next call should be done immediately without waiting
    virtual bool job() { return false; }
    
    ec::uid_t uid;
    std::atomic_bool running = { false };
    std::atomic_bool end_req = { false };
    NlQueue<std::pair<ec::OpCode, ec::Request<memblock>>> reqs;

    void handleRequests()
    {
        for (auto& req : reqs.fetchAll())
            handleRequest(req.first, req.second);
    }

    virtual void handleRequest(ec::OpCode OC, ec::Request<memblock>& req) = 0;
};
