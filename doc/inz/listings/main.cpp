bool ClusterStep::run()
{
    if (instance != nullptr)
        throw runtime_error("Multiple ClusterStep instances!");
    instance = this;

    running = true;

    auto* mainSocket = NetworkStep::getInstance().getControllSocket();

    while (running)
    {
        auto result = mainSocket->recv<memblock>();
        if (!result)
        {
            if (result.has_error())
            {
                sLog.log("Error occured while waiting for control packet, error code: ", result.get_error(), "( ", result.descr(), ")");
                break;
            } else {
                sLog.log("No error occured, but no data has been recieved");
            }
        }
        else
        {
            auto& req = result.get();
            auto header = req.deserializePart<PacketHeader>();
            if (header.dst == 0)
            {
                auto OC = req.deserializePart<OpCode>();
                handleReq(OC, req);
            }
            else
                routeRequest(header, req);
        }
    }

    return true;
}

void ClusterStep::handleReq(OpCode OC, Request<memblock>& req)
{
    sLog.log("Handling new request");
    switch (OC)
    {
    case OpCode::HelpRequest: return handleHelpRequest(req.deserialize<packets::HelpRequest>());
    case OpCode::StartCluster: 
        if (!handleStartCluster(req.deserialize<packets::StartCluster>()) && single_run)
            running = false;
        break;

    // Debug info
    case OpCode::DebugListRootsReq: return handleDbgListRoots(req.deserialize<packets::DebugListRootsReq>());

    default:
        routeRequest(req.deserializePart<PacketHeader>(0), req);
        break;
    }
}

void ClusterStep::routeRequest(ec::PacketHeader const& header, ec::Request<memblock>& req)
{
    auto OC = req.deserializePart<OpCode>();

    auto job = jobs.find(header.getDstUid());
    if (job == jobs.end())
        return (void)sLog.log("Unknown entity addressed in packet's header: ", header.getDstUid(), " - packet ", nameForOpCode(OC), " (", static_cast<int>(OC),  ") will be dropped");

    job->second->enqueueRequest(OC, move(req));
}
