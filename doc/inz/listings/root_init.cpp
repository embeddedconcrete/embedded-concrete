bool RootJob::init(ec::Cluster::Description&& desc)
{
    if (!cluster->acquire(desc, NetworkStep::getInstance().performNetworkDiscovery()))
        return (void)sLog.log("Cluster creation failed"), false;

    sLog.log("Cluster acquire succeeded - sending initial configuration to workers");
    cluster->configureWorkers();
    return true;
}