bool WorkerJob::job()
{
	if(!init)
		return false;

    bool done_anything = false;

    for (auto output : pw)
    {
        for (auto& data : output)
        {
            done_anything = true;
            if (out)
            {
                auto result = out->send(data);
                if (!result && result.has_error())
                    sLog.log("Worker ", getIdentifier(), ": error while sending network data: ", result.get_error(), " (", result.descr(), ")");
                else if (!result) {
                    sLog.log("Worker ", getIdentifier(), ": No data to send :E");
                }
                else if (debug)
                {
                    packets::DebugInfo info{ static_cast<uint32>(data.size()), static_cast<uint64>(time(NULL)) };
                    info.getHeader().cluster = getClusterId();
                    info.getHeader().src = getWorkerId();
                    info.getHeader().dst = 0;
                    ClusterStep::getInstance().enqueueDebugData(move(info));
                }
            }
        }
    }

    if (to_write)
    {
        done_anything = true;
        auto done = pw.write(to_write.subblock(writen));
        writen += done;
        if (writen == to_write.size())
        {
            to_write = memblock::empty;
            writen = 0;
        }
    }
    else if (in && poll_network)
    {
        auto recv = in->recv(false);
        if (!recv && recv.has_error())
            sLog.log("Worker ", getIdentifier(), ": error while receiving network data: ", recv.get_error(), " (", recv.descr(), ")");
        else if (recv)
        {
            done_anything = true;
            to_write = move(recv.get());
        }
        else if (input_end)
        {
            sLog.log("Worker ", getIdentifier(), " input is marked as exhausted and no more data remained in network buffer - resigning from network input and closing child process input");
            poll_network = false;
            pw.close_input();
        }
    }
    else if (!pw.is_running())
    {
        sLog.log("Worker ", getIdentifier(), " both child process and all input nodes have finished it work and no more data remained in outgoing buffer - notifying root about finished work");
        packets::WorkIsDone done{};
        done.getHeader().cluster = getClusterId();
        done.getHeader().src = getWorkerId();
        done.getHeader().dst = 0;
        ControlClient{}.signal(root, done);
        init = false;
    }

    return done_anything;
}
