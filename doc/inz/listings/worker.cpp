class WorkerJob : public Job<>
{
    using parent = Job<>;

public:
    WorkerJob(std::string const& rootaddr, ec::cluster_id_t cid, ec::worker_id_t wid, ec::process_wrapper&& my_pw);
    ~WorkerJob();

    std::string getIdentifier() const { return makestr(root, ":", getClusterId(), ":", getWorkerId()); }

    void stopDebug() { debug = false; }
    void startDebug() { debug = true; }

    void setInputSocket(ec::DataInput* input);

    auto const& getRootAddr() const { return root; }

private:
    ec::process_wrapper pw;

    memblock to_write;
    memlen_t writen = 0;

    ec::DataInput* in = nullptr;
    ec::DataOutput* out = nullptr;

    std::atomic<bool> debug = { false };
    std::atomic<bool> init = { false };

    std::string root;

    ec::Cluster::PeersList output_nodes;

    std::atomic<bool> input_end = { false };
    bool poll_network = false;

private:
    void stopChild();

    bool job() override;
    void tryConnectOutput(ec::Node const& node);

    void handleRequest(ec::OpCode OC, ec::Request<memblock>& req) override;
    void handleConfigurationRequest(ec::Request<ec::packets::Configuration>&& req);
    void handleEndOfInput(ec::Request<ec::packets::EndOfInput>&& req);
    void handleClusterDismantle(ec::Request<ec::packets::ClusterDismantle>&& req);
};
